# File Fetcher

This is a file server that was primarily intended for images, but it can also be used for other files. The basic idea was to be able to retrieve and provide a client file from a different location on the file system using a URL.

The application does not have a graphical interface, but a REST interface. This interface is used for complete server
 management. A local file database for data storage is also integrated.

**This is a server only!** For normal functionality, it is good to create your own client. The client can also be
 javascript. Queries to the server are REST (either classic JSON or just URL). The response is always JSON. Except for
  getting the file (binary response).\
It is possible to use my version of the client written in PHP, which I have among the projects.

## Requirements
Java 13+

## Download
1) Go to the **Tags** section.
2) Find the latest version of the app.
3) Click the Download button -> Download artifacts -> package-release (it is a zip file, then unzip it).

## Properties

- [x] Category and nested category support
- [x] Support for all file types (although the primary target is for images)
- [x] Support resizing the image directly in real time
- [x] Image rotation support (also in real time)
- [x] Mp4 video support
- [x] Database for storing data
- [x] Each category and file can have a unique identifier (hash)
- [x] Ability to change both category and file hashes
- [x] Password protected categories and files
- [x] High level of password security (SHA3-512)
- [x] Logic for import and export of the whole database
- [x] Detailed information about a file or category
- [x] Optimized for many REST queries

## Configuration

The following parameter can be used for custom configuration. If this parameter is not used, the default
 configuration will be used:\
`--spring.config.location=application.yaml`

The complete command can then look like this:\
`java -jar .\file-fetcher.war --spring.config.location=application.yaml`

## REST Interface

Insomnia was used for testing and administration. Here is the workspace [HERE](insomnia_workspace.yaml).

#### Tools

Contains server utilities. these tools are always called manually. They should never be called from a client.

- **/api/tool/clean-everything** [POST]
    - Deletes all records in all tables. Behavior as a clean installation.
- **/api/tool/dir-to-json** [POST]
    - A tool for generating JSON from a directory of files that can be used for import.
- **/api/tool/regenerate-all-guids** [POST]
    - Regenerates all GUID's for categories and files.
    - All original GUID's will be invalid!
    
#### Loader

Import and export data. Calls manually.

- **/api/loader/import** [POST]
    - Import data into the application.
- **/api/loader/export** [POST]
    - Export data from the application to JSON.
    - This output is re-importable.
    
#### User

User operations.

- **/api/user/change-categories** [POST]
    - Changes user permissions to categories.
- **/api/user/change-password** [POST]
    - Changes the user's password. Manual password change only.
- **/api/user/change-password-token** [GET]
    - Changes the user's password. This call is used from the client.
- **/api/user/clean** [POST]
    - Deletes all users.
- **/api/user/create** [POST]
    - Creates a new user.
- **/api/user/delete/{login}** [POST]
    - Deletes users by login.
    
#### Category

Category operations.

- **/api/cat** [GET]
    - Gets a list of categories and their information to display the page.
- **/api/cat/assign-to-category** [POST]
    - Assigns files to a category. It is also possible to remove a category.
- **/api/cat/clean** [POST]
    - Deletes all categories.
- **/api/cat/create** [POST]
    - Creates a new category.
- **/api/cat/delete/{guid}** [POST]
    - Deletes an existing category by GUID.
- **/api/cat/update** [POST]
    - Updates an existing category.
    
#### File

File operations.

- **/api/file/clean** [POST]
    - Deletes all files.
- **/api/file/create** [POST]
    - Creates a new file.
- **/api/file/delete/{guid}** [POST]
    - Deletes an existing file by GUID.
- **/api/file/fetch/{guid}/access** [GET]
    - Gets basic information about the file and whether the user has permission to it.
- **/api/file/fetch/{guid}/download** [GET]
    - Downloads the file by GUID.
- **/api/file/fetch/{guid}/image** [GET]
    - Gets a preview of the file.
- **/api/file/fetch/{guid}/info** [GET]
    - Gets file information.
- **/api/file/fetch/{guid}/open** [GET]
    - Gets a direct link (stream) to the file.
- **/api/file/random/image** [GET]
    - Gets a preview of the random file.
- **/api/file/random/info** [GET]
    - Gets information about a random file.
- **/api/file/update** [POST]
    - Updates an existing file.

#### Other

- **/api** [GET]
    - Get basic information about the service (eg. service version).
- **/api/stats** [GET]
    - Gets content statistics.

## Examples

Creates a new user with one category and one file in the category.
Password must be in SHA3-256.
```
[POST] /api/user/create
[
  {
    "login": "user",
    "password": "f5a5207a8729b1f709cb710311751eb2fc8acad5a1fb8ac991b736e69b6529a3"
  }
]
[POST] /api/cat/create
[
  {
    "guid": "my-category",
    "name": "My category"
  }
]
[POST] /api/file/create
[
  {
    "source": "c:/image.png",
    "category": "my-category"
  }
]
```
Or using a loader (more convenient and correct variant).
```
[POST] /api/loader/import
{
  "config": {
    "regen-guids": false,
    "regen-orders": true
  },
  "vars": {},
  "users": [
    {
      "login": "user",
      "password": "f5a5207a8729b1f709cb710311751eb2fc8acad5a1fb8ac991b736e69b6529a3"
    }
  ],
  "data": [
    {
      "name": "My category",
      "files": [
        {
          "source": "c:/image.png"
        }
      ]
    }
  ]
}
```
