package oc.mimic.filefetcher.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Aspekt pro controllery.
 *
 * @author mimic
 */
@Aspect
@Component
public class ControllerAspect extends AbstractAspect {

  /**
   * Anotace - {@link GetMapping}, {@link PostMapping}, {@link RequestMapping}.
   */
  @Around("@annotation(org.springframework.web.bind.annotation.GetMapping) || @annotation(org.springframework.web.bind.annotation.PostMapping) || @annotation(org.springframework.web.bind.annotation.RequestMapping)")
  public Object annotationMapping(ProceedingJoinPoint joinPoint) throws Throwable {
    return joinPoint.proceed();
  }
}
