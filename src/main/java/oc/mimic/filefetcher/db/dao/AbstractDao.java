package oc.mimic.filefetcher.db.dao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Základní DAO pro správu databáze a tabulek.
 *
 * @author mimic
 */
abstract class AbstractDao {

  private static final String SDP_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS";

  /**
   * Získá datum a čas z timestamp.
   */
  protected static Date parseTimestamp(String timestamp) throws ParseException {
    if (timestamp == null) {
      return null;
    }
    return new SimpleDateFormat(SDP_FORMAT).parse(timestamp);
  }
}
