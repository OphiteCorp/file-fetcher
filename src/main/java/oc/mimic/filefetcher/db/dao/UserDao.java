package oc.mimic.filefetcher.db.dao;

import oc.mimic.filefetcher.db.entity.User;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * DAO pro správu uživatelů.
 *
 * @author mimic
 */
@Repository
public class UserDao extends AbstractDao {

  private static final String REGEX_PASSWORD_OUT = "@hash[$1]";

  /**
   * Získá všechny uživatele.
   */
  public List<User> getAll(Connection conn) throws SQLException {
    //@formatter:off
    String sql = "select * from user ";
    sql       += "order by login asc";
    //@formatter:on

    List<User> list = new ArrayList<>();

    try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
      try (ResultSet result = pstmt.executeQuery()) {
        while (result.next()) {
          User user = mapToUser(conn, result);
          list.add(user);
        }
      }
    }
    return list;
  }

  /**
   * Získá uživatele podle ID nebo loginu.
   */
  public User read(Connection conn, Object idOrLogin) throws SQLException {
    User user = null;

    if (idOrLogin != null) {
      String sql;

      if (idOrLogin instanceof String) {
        sql = "select * from user where login = ?"; // 1
      } else {
        sql = "select * from user where id = ?"; // 1
      }
      try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
        pstmt.setObject(1, idOrLogin);

        try (ResultSet result = pstmt.executeQuery()) {
          result.next();

          if (result.getRow() == 1) {
            user = mapToUser(conn, result);
          }
        }
      }
    }
    return user;
  }

  /**
   * Vytvoří nového uživatele.
   */
  public User create(Connection conn, String login, String password) throws SQLException {
    //@formatter:off
    String sql = "insert into user ";
    sql       += "(login, password) ";
    sql       += "values (?,?)"; // 1,2
    //@formatter:on

    try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
      pstmt.setString(1, login);
      pstmt.setString(2, password);
      pstmt.executeUpdate();

      return read(conn, login);
    }
  }

  /**
   * Změní heslo uživatele.
   */
  public boolean changePassword(Connection conn, String login, String newPassword) throws SQLException {
    //@formatter:off
    String sql = "update user set ";
    sql       += "password = ? ";   // 1
    sql       += "where login = ?"; // 2
    //@formatter:on

    try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
      pstmt.setString(1, newPassword);
      pstmt.setString(2, login); // where

      return (pstmt.executeUpdate() > 0);
    }
  }

  /**
   * Smaže uživatele.
   */
  public boolean delete(Connection conn, String login) throws SQLException {
    String sql = "delete from user where login = ?"; // 1

    try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
      pstmt.setString(1, login);

      return (pstmt.executeUpdate() > 0);
    }
  }

  /**
   * Smaže všechny uživatele.
   */
  public int deleteAll(Connection conn) throws SQLException {
    String sql = "delete from user";

    try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
      return pstmt.executeUpdate();
    }
  }

  /**
   * Smaže oprávnění uživatele na kategorie.
   */
  public int deleteUserCategories(Connection conn, String login) throws SQLException {
    String sql = "delete from user_category where user_id = (select id from user where login = ?)"; // 1

    try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
      pstmt.setString(1, login);
      return pstmt.executeUpdate();
    }
  }

  /**
   * Přiřadí uživateli přístup do kategorie.
   */
  public boolean assignUserCategory(Connection conn, Long userId, Long categoryId) throws SQLException {
    String sql = "insert into user_category (user_id, category_id) values (?,?)"; // 1,2

    try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
      pstmt.setLong(1, userId);
      pstmt.setLong(2, categoryId);
      return (pstmt.executeUpdate() > 0);
    }
  }

  /**
   * Získá všechny kategorie, ke kterým má uživatel přístup.
   */
  private Set<String> getUserCategories(Connection conn, String login) throws SQLException {
    //@formatter:off
    String sql = "select c.guid category_guid from user_category uc ";
    sql       += "join user u on u.id = uc.user_id ";
    sql       += "join category c on c.id = uc.category_id ";
    sql       += "where u.login = ?";
    //@formatter:on

    Set<String> categories = new LinkedHashSet<>();

    try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
      pstmt.setString(1, login);

      try (ResultSet result = pstmt.executeQuery()) {
        while (result.next()) {
          categories.add(result.getString("category_guid"));
        }
      }
    }
    return categories;
  }

  private User mapToUser(Connection conn, ResultSet result) throws SQLException {
    User user = new User();
    user.setId(result.getLong("id"));
    user.setLogin(result.getString("login"));
    user.setPassword(result.getString("password").replaceAll("(.+)", REGEX_PASSWORD_OUT));

    Set<String> categories = getUserCategories(conn, user.getLogin());
    user.setCategories(categories);

    return user;
  }
}
