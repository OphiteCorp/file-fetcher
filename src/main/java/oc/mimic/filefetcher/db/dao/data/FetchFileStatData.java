package oc.mimic.filefetcher.db.dao.data;

import oc.mimic.filefetcher.db.entity.enums.FileExtraType;

/**
 * Informace o souboru pro statistiku.
 *
 * @author mimic
 */
public final class FetchFileStatData {

  private String source;
  private boolean hidden;
  private FileExtraType extraType;

  public String getSource() {
    return source;
  }

  public void setSource(String source) {
    this.source = source;
  }

  public boolean isHidden() {
    return hidden;
  }

  public void setHidden(boolean hidden) {
    this.hidden = hidden;
  }

  public FileExtraType getExtraType() {
    return extraType;
  }

  public void setExtraType(FileExtraType extraType) {
    this.extraType = extraType;
  }
}
