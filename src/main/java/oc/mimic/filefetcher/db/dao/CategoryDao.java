package oc.mimic.filefetcher.db.dao;

import com.fasterxml.jackson.core.JsonProcessingException;
import oc.mimic.filefetcher.db.dao.data.CategoryExtra;
import oc.mimic.filefetcher.db.dao.data.CategoryPath;
import oc.mimic.filefetcher.db.entity.Category;
import oc.mimic.filefetcher.db.entity.FetchFile;
import oc.mimic.filefetcher.db.entity.enums.FileExtraType;
import oc.mimic.filefetcher.db.entity.enums.SortColumnType;
import oc.mimic.filefetcher.db.service.data.IFileExtraData;
import oc.mimic.filefetcher.exception.FileFetcherException;
import oc.mimic.filefetcher.utils.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.*;

/**
 * DAO pro správu kategorie.
 *
 * @author mimic
 */
@Repository
public class CategoryDao extends AbstractDao {

  @Autowired
  private FetchFileDao fetchFileDao;

  /**
   * Získá všechny kategorie.
   */
  public List<Category> getAll(Connection conn) throws SQLException, ParseException, JsonProcessingException {
    //@formatter:off
    String sql = "select * from category ";
    sql       += "order by order_code asc, created desc, name asc";
    //@formatter:on

    List<Category> categories = new ArrayList<>();

    try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
      try (ResultSet result = pstmt.executeQuery()) {
        while (result.next()) {
          Category category = mapToCategory(conn, result);
          categories.add(category);
        }
      }
    }
    return categories;
  }

  /**
   * Získá kategorii podle ID nebo GUID.
   */
  public Category read(Connection conn, Object idOrGuid) throws SQLException, ParseException, JsonProcessingException {
    Category category = null;

    if (idOrGuid != null) {
      String sql;

      if (idOrGuid instanceof String) {
        sql = "select * from category where guid = ?"; // 1
      } else {
        sql = "select * from category where id = ?"; // 1
      }
      try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
        pstmt.setObject(1, idOrGuid);

        try (ResultSet result = pstmt.executeQuery()) {
          result.next();

          if (result.getRow() == 1) {
            category = mapToCategory(conn, result);
          }
        }
      }
    }
    return category;
  }

  /**
   * Vytvoří novou kategorii.
   */
  public Category create(Connection conn, String guid, String name, String description, Integer order,
          String sortColumn, Long parentId, Long previewFileId)
          throws SQLException, ParseException, JsonProcessingException {

    //@formatter:off
    String sql = "insert into category ";
    sql       += "(guid, name, description, order_code, sort_column, parent_id, preview_file_id) ";
    sql       += "values (?,?,?,?,?,?,?)"; // 1,2,3,4,5,6,7
    //@formatter:on

    try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
      SortColumnType sortColumnType = SortColumnType.getByColumnName(sortColumn);
      int i = 1;
      pstmt.setString(i++, guid);
      pstmt.setString(i++, name);
      pstmt.setObject(i++, description);
      pstmt.setInt(i++, (order != null) ? order : Category.DEFAULT_ORDER);
      pstmt.setObject(i++, (sortColumnType != null) ? sortColumnType.getColumnName() : null);
      pstmt.setObject(i++, parentId);
      pstmt.setObject(i, previewFileId);
      pstmt.executeUpdate();

      return read(conn, guid);
    }
  }

  /**
   * Aktualizuje kategorii.
   */
  public Category update(Connection conn, String guid, String newGuid, String name, String description, Integer order,
          String sortColumn, Long parentId, Long previewFileId)
          throws SQLException, ParseException, JsonProcessingException {

    //@formatter:off
    String sql = "update category set ";
    sql       += "  name = ?, "; // 1
    sql       += "  description = ?, "; // 2
    sql       += "  order_code = ?, "; // 3
    sql       += "  sort_column = ?, "; // 4
    sql       += "  parent_id = ?, "; // 5
    sql       += "  updated = STRFTIME('%Y-%m-%d %H:%M:%f', 'now', 'localtime'), ";
    sql       += "  guid = ?, "; // 6
    sql       += "  preview_file_id = ? "; // 7
    sql       += "where guid = ?"; // 8
    //@formatter:on

    try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
      if (newGuid == null) {
        newGuid = guid;
      }
      SortColumnType sortColumnType = SortColumnType.getByColumnName(sortColumn);
      int i = 1;
      pstmt.setString(i++, name);
      pstmt.setObject(i++, description);
      pstmt.setInt(i++, (order != null) ? order : Category.DEFAULT_ORDER);
      pstmt.setObject(i++, (sortColumnType != null) ? sortColumnType.getColumnName() : null);
      pstmt.setObject(i++, parentId);
      pstmt.setObject(i++, newGuid);
      pstmt.setObject(i++, previewFileId);
      pstmt.setString(i, guid); // where
      pstmt.executeUpdate();

      return read(conn, newGuid);
    }
  }

  /**
   * Smaže kategorii.
   */
  public boolean delete(Connection conn, String guid) throws SQLException {
    String sql = "delete from category where guid = ?"; // 1

    try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
      pstmt.setString(1, guid);

      return (pstmt.executeUpdate() > 0);
    }
  }

  /**
   * Smaže všechny kategorie.
   */
  public int deleteAll(Connection conn) throws SQLException {
    String sql = "delete from category";

    try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
      return pstmt.executeUpdate();
    }
  }

  /**
   * Získá cestu k aktuální kategorii.
   */
  public List<CategoryPath> getCategoryPaths(Connection conn, String guid) throws SQLException {
    //@formatter:off
    String sql = "with tmp as ( ";
    sql       += "  select * from category where guid = ? "; // 1
    sql       += "  union all ";
    sql       += "  select c.* from category c, tmp ";
    sql       += "  where tmp.parent_id = c.id ";
    sql       += ") ";
    sql       += "select tmp.guid, tmp.name, tmp.description from tmp";
    //@formatter:on

    try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
      pstmt.setObject(1, guid);

      try (ResultSet result = pstmt.executeQuery()) {
        List<CategoryPath> paths = new ArrayList<>();

        while (result.next()) {
          CategoryPath categoryPath = new CategoryPath();
          categoryPath.setGuid(result.getString("guid"));
          categoryPath.setName(result.getString("name"));
          categoryPath.setDescription(result.getString("description"));

          paths.add(categoryPath);
        }
        Collections.reverse(paths); // je nutné otočit, jinak to půjde od kategorie po root
        return paths;
      }
    }
  }

  /**
   * Získá podkategorie podle GUID.
   */
  public List<Category> getSubcategories(Connection conn, String guid)
          throws SQLException, ParseException, JsonProcessingException {

    //@formatter:off
    String sql = "";

    if (guid == null) {
      sql += "select * from category where parent_id is null ";
      sql += "order by order_code asc, created desc, name asc";
    } else {
      sql += "with tmp as ( ";
      sql += "  select * from category where guid = ? "; // 1
      sql += "  union all ";
      sql += "  select c.* from category c, tmp ";
      sql += "  where tmp.id = c.parent_id and c.parent_id = (select id from category where guid = ?) "; // 2
      sql += ") ";
      sql += "select * from tmp t ";
      sql += "order by t.id asc, t.created desc, t.name asc ";
      sql += "limit -1 offset 1";
    }
    //@formatter:on

    try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
      if (guid != null) {
        pstmt.setString(1, guid);
        pstmt.setString(2, guid);
      }
      try (ResultSet result = pstmt.executeQuery()) {
        List<Category> categories = new ArrayList<>();

        while (result.next()) {
          Category category = mapToCategory(conn, result);
          categories.add(category);
        }
        return categories;
      }
    }
  }

  /**
   * Získá maximální hodnotu řazení pro kategorie.
   */
  public int getMaxOrder(Connection conn) throws SQLException {
    String sql = "select max(order_code) max_order from category";

    try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
      try (ResultSet result = pstmt.executeQuery()) {
        result.next();

        if (result.getRow() == 1) {
          return result.getInt("max_order");
        }
      }
    }
    return 0;
  }

  /**
   * Získá všechny podkategorie podle GUID.
   */
  public List<Category> getAllSubcategories(Connection conn, String guid)
          throws SQLException, ParseException, JsonProcessingException {

    //@formatter:off
    String sql = "";

    if (guid == null) {
      sql += "select * from category where parent_id is null ";
      sql += "order by order_code asc, created desc, name asc";
    } else {
      sql += "with tmp as ( ";
      sql += "  select * from category where guid = ? "; // 1
      sql += "  union all ";
      sql += "  select c.* from category c, tmp ";
      sql += "  where tmp.id = c.parent_id ";
      sql += ") ";
      sql += "select * from tmp t ";
      sql += "order by t.order_code asc, t.created desc, t.name asc ";
      sql += "limit -1 offset 1";
    }
    //@formatter:on

    try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
      if (guid != null) {
        pstmt.setString(1, guid);
      }
      try (ResultSet result = pstmt.executeQuery()) {
        List<Category> categories = new ArrayList<>();

        while (result.next()) {
          Category category = mapToCategory(conn, result);
          categories.add(category);
        }
        return categories;
      }
    }
  }

  /**
   * Získá extra informace o kategorii podle GUID.
   */
  public CategoryExtra getCategoryExtra(Connection conn, String guid) throws SQLException {
    //@formatter:off
    // získá počet všech souborů v kategorii
    String sql1 = "select count(*) cnt from file f ";
    sql1       += "join category c on c.id = f.category_id ";
    sql1       += "where f.hidden = 0 and c.guid = ?"; // 1
    // získá počet všech souborů v kategorii a podkategoriich
    String sql2 = "with tmp as ( ";
    sql2       += "  select * from category where guid = ? "; // 2
    sql2       += "  union all ";
    sql2       += "  select c.* from category c, tmp ";
    sql2       += "  where tmp.id = c.parent_id ";
    sql2       += ") ";
    sql2       += "select count(f.id) from tmp t ";
    sql2       += "left join file f on f.category_id = t.id ";
    sql2       += "where f.hidden = 0 and f.id is not null";
    // počet podkategorii v dané kategorii (bez podkategorii)
    String sql3 = "select count(*) from ( ";
    sql3       += "  with tmp as ( ";
    sql3       += "    select * from category where guid = ? "; // 3
    sql3       += "    union all ";
    sql3       += "    select c.* from category c, tmp ";
    sql3       += "    where tmp.id = c.parent_id and c.parent_id = (select id from category where guid = ?) "; // 4
    sql3       += "  ) ";
    sql3       += "  select * from tmp limit -1 offset 1 ";
    sql3       += ")";
    // náhodný soubor (náhodný výběr pouze ze souborů v kategorii)
    String sql4 = "select f.guid || '|' || f.checksum || '|' || f.extra_type || '|' || f.extra from file f ";
    sql4       += "join category c on c.id = f.category_id ";
    sql4       += "where f.hidden = 0 and c.guid = ? "; // 5
    sql4       += "order by random() limit 1";
    // získá všechny data k souborům v kategorii a ve všech podkategoriich
    String sql5 = "with tmp as ( ";
    sql5       += "  select * from category where guid = ? "; // 6
    sql5       += "  union all ";
    sql5       += "  select c.* from category c, tmp ";
    sql5       += "  where tmp.id = c.parent_id ";
    sql5       += ") ";
    sql5       += "select group_concat(f.source, '|') from tmp t ";
    sql5       += "left join file f on f.category_id = t.id ";
    sql5       += "where f.hidden = 0 and f.id is not null";
    // finální SQL
    String sql  = "select ";
    sql        += "(" + sql1 + ") category_files, ";
    sql        += "(" + sql2 + ") total_category_files, ";
    sql        += "(" + sql3 + ") subcategory_count, ";
    sql        += "(" + sql4 + ") preview_file, ";
    sql        += "(" + sql5 + ") files_data";
    //@formatter:on

    CategoryExtra extra = new CategoryExtra();

    try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
      int i = 1;
      pstmt.setString(i++, guid); // sql1
      pstmt.setString(i++, guid); // sql2
      pstmt.setString(i++, guid); // sql3
      pstmt.setString(i++, guid); // sql3
      pstmt.setString(i++, guid); // sql4
      pstmt.setString(i, guid); // sql5

      try (ResultSet result = pstmt.executeQuery()) {
        result.next();

        if (result.getRow() == 1) {
          extra.setCategoryFiles(result.getLong("category_files"));
          extra.setTotalCategoryFiles(result.getLong("total_category_files"));
          extra.setSubcategoryCount(result.getLong("subcategory_count"));

          // sql4
          String previewFile = result.getString("preview_file");
          if (previewFile != null) {
            String[] parts = previewFile.split("\\|");
            extra.setPreviewGuid(parts[0]);
            extra.setPreviewChecksum(Long.valueOf(parts[1]));
            FileExtraType extraType = FileExtraType.getByCode(parts[2]);
            try {
              IFileExtraData extraData = (IFileExtraData) IFileExtraData.toObject(parts[3], extraType);
              extra.setPreviewWidth(extraData.getWidth());
              extra.setPreviewHeight(extraData.getHeight());
            } catch (Exception e) {
              throw new FileFetcherException("Could not get extra data to preview file: " + extra.getPreviewGuid());
            }
          }
          // sql5
          String filesData = result.getString("files_data");
          if (filesData != null) {
            String[] data = filesData.split("\\|");
            long totalSize = 0;

            for (String source : data) {
              if (Validator.isFileExists(source)) {
                File file = new File(source);
                totalSize += file.length();
              }
            }
            extra.setTotalCategorySize(totalSize);
          }
        }
      }
    }
    return extra;
  }

  /**
   * Získá GUID kategorie podle ID.
   */
  private String getGuid(Connection conn, Long id) throws SQLException {
    //@formatter:off
    String sql = "select guid from category ";
    sql       += "where id = ?";
    //@formatter:on

    try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
      pstmt.setLong(1, id);

      try (ResultSet result = pstmt.executeQuery()) {
        result.next();

        if (result.getRow() == 1) {
          return result.getString("guid");
        }
      }
    }
    return null;
  }

  /**
   * Získá všechny uživatele kategorie.
   */
  private Set<String> getCategoryForUsers(Connection conn, String guid) throws SQLException {
    //@formatter:off
    String sql = "select u.login user_login from user u ";
    sql       += "join user_category uc on uc.user_id = u.id ";
    sql       += "join category c on c.id = uc.category_id ";
    sql       += "where c.guid = ?";
    //@formatter:on

    Set<String> users = new LinkedHashSet<>();

    try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
      pstmt.setString(1, guid);

      try (ResultSet result = pstmt.executeQuery()) {
        while (result.next()) {
          users.add(result.getString("user_login"));
        }
      }
    }
    return users;
  }

  /**
   * Získá všechny soubory kategorie.
   */
  private Set<String> getCategoryForFiles(Connection conn, String guid) throws SQLException {
    //@formatter:off
    String sql = "select f.guid file_guid from file f ";
    sql       += "join category c on c.id = f.category_id ";
    sql       += "where c.guid = ?";
    //@formatter:on

    Set<String> files = new LinkedHashSet<>();

    try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
      pstmt.setString(1, guid);

      try (ResultSet result = pstmt.executeQuery()) {
        while (result.next()) {
          files.add(result.getString("file_guid"));
        }
      }
    }
    return files;
  }

  private Category mapToCategory(Connection conn, ResultSet result)
          throws SQLException, ParseException, JsonProcessingException {

    Category category = new Category();
    category.setId(result.getLong("id"));
    category.setGuid(result.getString("guid"));
    category.setCreated(parseTimestamp(result.getString("created")));
    category.setUpdated(parseTimestamp(result.getString("updated")));
    category.setName(result.getString("name"));
    category.setOrder(result.getInt("order_code"));
    category.setDescription(result.getString("description"));
    category.setSortColumn(SortColumnType.getByColumnName(result.getString("sort_column")));

    long parentId = result.getLong("parent_id");
    if (parentId > 0) {
      category.setParentGuid(getGuid(conn, parentId));
    }
    long previewFileId = result.getLong("preview_file_id");
    if (previewFileId > 0) {
      FetchFile ff = fetchFileDao.read(conn, previewFileId);
      category.setPreviewFile(ff);
    }
    Set<String> users = getCategoryForUsers(conn, category.getGuid());
    category.setUsers(users);

    Set<String> files = getCategoryForFiles(conn, category.getGuid());
    category.setFiles(files);

    return category;
  }
}
