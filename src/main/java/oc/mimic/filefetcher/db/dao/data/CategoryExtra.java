package oc.mimic.filefetcher.db.dao.data;

/**
 * Extra informace o kategorii.
 *
 * @author mimic
 */
public final class CategoryExtra {

  // počet souborů v kategorii
  private long categoryFiles;

  // počet souborů v kategorii a všech podkategoriich
  private long totalCategoryFiles;

  // počet podkategorii v kategorii
  private long subcategoryCount;

  // náhodný obrázek kategorie vybraný z obrázků v kategorii dle guid
  private String previewGuid;
  private long previewChecksum;
  private int previewWidth;
  private int previewHeight;

  // celková velikost kategorie (obsahuje i všechny podkategorie)
  private long totalCategorySize;

  public boolean isPreviewFile() {
    return (previewGuid != null);
  }

  public long getCategoryFiles() {
    return categoryFiles;
  }

  public void setCategoryFiles(long categoryFiles) {
    this.categoryFiles = categoryFiles;
  }

  public long getTotalCategoryFiles() {
    return totalCategoryFiles;
  }

  public void setTotalCategoryFiles(long totalCategoryFiles) {
    this.totalCategoryFiles = totalCategoryFiles;
  }

  public long getSubcategoryCount() {
    return subcategoryCount;
  }

  public void setSubcategoryCount(long subcategoryCount) {
    this.subcategoryCount = subcategoryCount;
  }

  public long getPreviewChecksum() {
    return previewChecksum;
  }

  public void setPreviewChecksum(long previewChecksum) {
    this.previewChecksum = previewChecksum;
  }

  public String getPreviewGuid() {
    return previewGuid;
  }

  public void setPreviewGuid(String previewGuid) {
    this.previewGuid = previewGuid;
  }

  public long getTotalCategorySize() {
    return totalCategorySize;
  }

  public void setTotalCategorySize(long totalCategorySize) {
    this.totalCategorySize = totalCategorySize;
  }

  public int getPreviewWidth() {
    return previewWidth;
  }

  public void setPreviewWidth(int previewWidth) {
    this.previewWidth = previewWidth;
  }

  public int getPreviewHeight() {
    return previewHeight;
  }

  public void setPreviewHeight(int previewHeight) {
    this.previewHeight = previewHeight;
  }
}
