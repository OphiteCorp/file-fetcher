package oc.mimic.filefetcher.db.dao.data;

/**
 * Informace o cestě.
 *
 * @author mimic
 */
public final class CategoryPath {

  private String guid = "";
  private String name = "";
  private String description;

  public CategoryPath() {
  }

  public CategoryPath(String guid, String name, String description) {
    this.guid = (guid == null) ? "" : guid;
    this.name = (name == null) ? "" : name;
    this.description = description;
  }

  public String getGuid() {
    return guid;
  }

  public void setGuid(String guid) {
    this.guid = guid;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }
}
