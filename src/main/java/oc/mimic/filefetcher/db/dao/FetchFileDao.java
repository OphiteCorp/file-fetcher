package oc.mimic.filefetcher.db.dao;

import com.fasterxml.jackson.core.JsonProcessingException;
import oc.mimic.filefetcher.db.dao.data.FetchFileStatData;
import oc.mimic.filefetcher.db.entity.FetchFile;
import oc.mimic.filefetcher.db.entity.enums.FileExtraType;
import oc.mimic.filefetcher.db.entity.enums.Grade;
import oc.mimic.filefetcher.db.service.data.IFileExtraData;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * DAO pro správu souborů.
 *
 * @author mimic
 */
@Repository
public class FetchFileDao extends AbstractDao {

  /**
   * Získá všechny soubory.
   */
  public List<FetchFile> getAll(Connection conn) throws SQLException, JsonProcessingException, ParseException {
    //@formatter:off
    String sql = "select f.*, c.guid category_guid from file f ";
    sql       += "left join category c on c.id = f.category_id ";
    sql       += "order by f.order_code asc, f.created asc, f.name asc";
    //@formatter:on

    List<FetchFile> files = new ArrayList<>();

    try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
      try (ResultSet result = pstmt.executeQuery()) {
        while (result.next()) {
          FetchFile ff = mapToFetchFile(result);
          files.add(ff);
        }
      }
    }
    return files;
  }

  /**
   * Získá všechny soubory pro statistiku.
   */
  public List<FetchFileStatData> getAllForStats(Connection conn) throws SQLException {
    //@formatter:off
    String sql = "select f.source, f.hidden, f.extra_type from file f ";
    sql       += "order by f.order_code asc, f.created asc, f.name asc";
    //@formatter:on

    List<FetchFileStatData> files = new ArrayList<>();

    try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
      try (ResultSet result = pstmt.executeQuery()) {
        while (result.next()) {
          FetchFileStatData ff = mapToFetchFileStatData(result);
          files.add(ff);
        }
      }
    }
    return files;
  }

  /**
   * Získá všechny soubory bez kategorie.
   */
  public List<FetchFile> getFilesWithoutCategory(Connection conn)
          throws SQLException, JsonProcessingException, ParseException {

    //@formatter:off
    String sql = "select f.*, null category_guid from file f ";
    sql       += "left join category c on c.id = f.category_id ";
    sql       += "where f.category_id is null ";
    sql       += "order by f.order_code asc, f.created asc, f.name asc";
    //@formatter:on

    List<FetchFile> files = new ArrayList<>();

    try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
      try (ResultSet result = pstmt.executeQuery()) {
        while (result.next()) {
          FetchFile ff = mapToFetchFile(result);
          files.add(ff);
        }
      }
    }
    return files;
  }

  /**
   * Získá soubor podle ID nebo GUID.
   */
  public FetchFile read(Connection conn, Object idOrGuid) throws SQLException, JsonProcessingException, ParseException {
    FetchFile ff = null;

    if (idOrGuid != null) {
      //@formatter:off
      String sql = "select f.*, c.guid category_guid from file f ";
      sql       += "left join category c on c.id = f.category_id ";

      if (idOrGuid instanceof String) {
        sql     += "where f.guid = ?"; // 1
      } else {
        sql     += "where f.id = ?"; // 1
      }
      //@formatter:on

      try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
        pstmt.setObject(1, idOrGuid);

        try (ResultSet result = pstmt.executeQuery()) {
          result.next();

          if (result.getRow() == 1) {
            ff = mapToFetchFile(result);
          }
        }
      }
    }
    return ff;
  }

  /**
   * Získá všechny soubory pro danou kategorii.
   */
  public List<FetchFile> getByCategory(Connection conn, String categoryGuid)
          throws SQLException, JsonProcessingException, ParseException {

    List<FetchFile> list = new ArrayList<>();
    String sql;

    //@formatter:off
    if (categoryGuid == null) {
      sql  = "select f.*, null category_guid from file f ";
      sql += "where f.category_id is null ";
    } else {
      sql  = "select f.*, c.guid category_guid from file f ";
      sql += "left join category c on c.id = f.category_id ";
      sql += "where f.hidden = 0 and c.guid = ? "; // 1
    }
    sql   += "order by f.order_code asc, f.created asc, f.name asc";
    //@formatter:on

    try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
      if (categoryGuid != null) {
        pstmt.setString(1, categoryGuid);
      }
      try (ResultSet result = pstmt.executeQuery()) {
        while (result.next()) {
          FetchFile ff = mapToFetchFile(result);
          list.add(ff);
        }
      }
    }
    return list;
  }

  /**
   * Vytvoří nový soubor.
   */
  public FetchFile create(Connection conn, String guid, String name, String author, String description, Integer order,
          String source, Long categoryId, long checksum, String grade, boolean hidden, IFileExtraData extraData)
          throws SQLException, JsonProcessingException, ParseException {

    //@formatter:off
    String sql = "insert into file ";
    sql       += "(guid, name, author, description, order_code, source, category_id, checksum, grade, hidden, extra_type, extra) ";
    sql       += "values (?,?,?,?,?,?,?,?,?,?,?,?)"; // 1,2,3,4,5,6,7,8,9,10,11
    //@formatter:on

    try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
      int i = 1;
      pstmt.setString(i++, guid);
      pstmt.setObject(i++, name);
      pstmt.setObject(i++, author);
      pstmt.setObject(i++, description);
      pstmt.setInt(i++, (order != null) ? order : FetchFile.DEFAULT_ORDER);
      pstmt.setString(i++, source);
      pstmt.setObject(i++, categoryId);
      pstmt.setObject(i++, checksum);
      pstmt.setObject(i++, grade);
      pstmt.setInt(i++, hidden ? 1 : 0);
      pstmt.setObject(i++, (extraData != null) ? extraData.getExtraType().getCode() : null);
      pstmt.setObject(i, (extraData != null) ? extraData.toJson() : null);
      pstmt.executeUpdate();

      return read(conn, guid);
    }
  }

  /**
   * Aktualizuje soubor.
   */
  public FetchFile update(Connection conn, String guid, String newGuid, String name, String author, String description,
          Integer order, String source, Long categoryId, long checksum, String grade, boolean hidden,
          IFileExtraData extraData) throws SQLException, JsonProcessingException, ParseException {

    //@formatter:off
    String sql = "update file set ";
    sql       += "name = ?, "; // 1
    sql       += "author = ?, "; // 2
    sql       += "description = ?, "; // 3
    sql       += "order_code = ?, "; // 4
    sql       += "source = ?, "; // 5
    sql       += "category_id = ?, "; // 6
    sql       += "updated = STRFTIME('%Y-%m-%d %H:%M:%f', 'now', 'localtime'), ";
    sql       += "guid = ?, "; // 7
    sql       += "checksum = ?, "; // 8
    sql       += "grade = ?, "; // 9
    sql       += "hidden = ?, "; // 10
    sql       += "extra_type = ?, "; // 11
    sql       += "extra = ? "; // 12
    sql       += "where guid = ?"; // 11
    //@formatter:on

    try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
      if (newGuid == null) {
        newGuid = guid;
      }
      int i = 1;
      pstmt.setObject(i++, name);
      pstmt.setObject(i++, author);
      pstmt.setObject(i++, description);
      pstmt.setInt(i++, (order != null) ? order : FetchFile.DEFAULT_ORDER);
      pstmt.setString(i++, source);
      pstmt.setObject(i++, categoryId);
      pstmt.setObject(i++, newGuid);
      pstmt.setObject(i++, checksum);
      pstmt.setObject(i++, grade);
      pstmt.setInt(i++, hidden ? 1 : 0);
      pstmt.setObject(i++, (extraData != null) ? extraData.getExtraType().getCode() : null);
      pstmt.setObject(i++, (extraData != null) ? extraData.toJson() : null);
      pstmt.setString(i, guid); // where
      pstmt.executeUpdate();

      return read(conn, newGuid);
    }
  }

  /**
   * Smaže soubor.
   */
  public boolean delete(Connection conn, String guid) throws SQLException {
    String sql = "delete from file where guid = ?"; // 1

    try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
      pstmt.setString(1, guid);

      return (pstmt.executeUpdate() > 0);
    }
  }

  /**
   * Smaže všechny soubory.
   */
  public int deleteAll(Connection conn) throws SQLException {
    String sql = "delete from file";

    try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
      return pstmt.executeUpdate();
    }
  }

  /**
   * Přiřadí soubor do kategorie.
   */
  public boolean assignFileToCategory(Connection conn, String fileGuid, Long categoryId) throws SQLException {
    //@formatter:off
    String sql = "update file set ";
    sql       += "category_id = ? "; // 1
    sql       += "where guid = ?"; // 2
    //@formatter:on

    try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
      pstmt.setObject(1, categoryId);
      pstmt.setString(2, fileGuid);
      return (pstmt.executeUpdate() > 0);
    }
  }

  /**
   * Přiřadí soubory bez kategorie do kategorie.
   */
  public void assignFileNotCategoryToCategory(Connection conn, Long categoryId) throws SQLException {
    //@formatter:off
    String sql = "update file set ";
    sql       += "category_id = ? "; // 1
    sql       += "where category_id is null";
    //@formatter:on

    try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
      pstmt.setLong(1, categoryId);
      pstmt.executeUpdate();
    }
  }

  /**
   * Získá maximální hodnotu řazení pro soubory.
   */
  public int getMaxOrder(Connection conn) throws SQLException {
    String sql = "select max(order_code) max_order from file";

    try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
      try (ResultSet result = pstmt.executeQuery()) {
        result.next();

        if (result.getRow() == 1) {
          return result.getInt("max_order");
        }
      }
    }
    return 0;
  }

  /**
   * Získá veřejné náhodné soubory.
   */
  public List<FetchFile> getRandomPublicFiles(Connection conn, String categoryGuid, int count)
          throws SQLException, JsonProcessingException, ParseException {

    List<FetchFile> list = new ArrayList<>();
    String sql;

    if (categoryGuid != null) {
      //@formatter:off
      sql  = "with tmp as ( ";
      sql += "  select * from category where guid = ? "; // 1
      sql += "  union all ";
      sql += "  select c.* from category c, tmp ";
      sql += "  where tmp.id = c.parent_id ";
      sql += ") ";
      sql += "select f.*, t.guid category_guid from tmp t ";
      sql += "join file f on f.category_id = t.id ";
      //@formatter:on
    } else {
      sql = "select f.*, null category_guid from file f ";
    }
    // soubor musí být veřejný a nebýt pod heslem/uživatelem
    sql += "where (f.category_id is null or f.category_id not in (select distinct category_id from user_category)) ";
    sql += "order by random() limit " + count;

    try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
      if (categoryGuid != null) {
        pstmt.setString(1, categoryGuid);
      }
      try (ResultSet result = pstmt.executeQuery()) {
        while (result.next()) {
          FetchFile ff = mapToFetchFile(result);
          list.add(ff);
        }
      }
    }
    return list;
  }

  private static FetchFile mapToFetchFile(ResultSet result)
          throws SQLException, JsonProcessingException, ParseException {

    FetchFile ff = new FetchFile();
    ff.setId(result.getLong("id"));
    ff.setGuid(result.getString("guid"));
    ff.setCreated(parseTimestamp(result.getString("created")));
    ff.setUpdated(parseTimestamp(result.getString("updated")));
    ff.setName(result.getString("name"));
    ff.setAuthor(result.getString("author"));
    ff.setOrder(result.getInt("order_code"));
    ff.setDescription(result.getString("description"));
    ff.setSource(result.getString("source"));
    ff.setChecksum(result.getLong("checksum"));
    ff.setGrade(Grade.getByCode(result.getString("grade")));
    ff.setHidden(result.getInt("hidden") > 0);
    ff.setCategoryGuid(result.getString("category_guid"));
    ff.setExtraType(FileExtraType.getByCode(result.getString("extra_type")));

    if (ff.getExtraType() != null) {
      String extraJson = result.getString("extra");

      if (extraJson != null) {
        IFileExtraData extra = (IFileExtraData) IFileExtraData.toObject(extraJson, ff.getExtraType());
        ff.setExtra(extra);
      }
    }
    return ff;
  }

  private static FetchFileStatData mapToFetchFileStatData(ResultSet result) throws SQLException {
    FetchFileStatData data = new FetchFileStatData();
    data.setSource(result.getString("source"));
    data.setHidden(result.getInt("hidden") > 0);
    data.setExtraType(FileExtraType.getByCode(result.getString("extra_type")));
    return data;
  }
}
