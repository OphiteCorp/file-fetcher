package oc.mimic.filefetcher.db;

import oc.mimic.filefetcher.config.ApplicationConfig;
import oc.mimic.filefetcher.db.entity.Category;
import oc.mimic.filefetcher.db.entity.FetchFile;
import oc.mimic.filefetcher.exception.FileFetcherException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.sql.*;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Vytvoření databáze a tabulek.
 *
 * @author mimic
 */
@Component
public class DatabaseInitializer {

  private static final Logger LOG = LoggerFactory.getLogger(DatabaseInitializer.class);

  @Autowired
  private ApplicationConfig applicationConfig;

  /**
   * Vytvoří novou databázi včetně tabulek.
   */
  public void createDatabase() {
    String dbFileName = applicationConfig.getDatabaseFile();
    String url = "jdbc:sqlite:" + dbFileName;
    File dbFile = new File(applicationConfig.getDatabaseFile());

    if (!dbFile.exists()) {
      if (!dbFile.getParentFile().exists()) {
        if (dbFile.getParentFile().mkdirs()) {
          LOG.info("Directory created: {}", dbFile.getParentFile());
        }
      }
      try (Connection conn = DriverManager.getConnection(url)) {
        if (conn != null) {
          DatabaseMetaData meta = conn.getMetaData();
          LOG.info("The database driver is: {}", meta.getDriverName());
          LOG.info("The database was successfully created.");
          createTables(conn);
          LOG.info("All database tables have been created.");
        }
      } catch (SQLException e) {
        LOG.error("An error occurred while creating a new database.", e);
        throw new FileFetcherException(e);
      }
    }
  }

  private static void createTables(Connection conn) throws SQLException {
    Map<String, String> sqlMap = new LinkedHashMap<>();

    //@formatter:off
    sqlMap.put("category",
            "CREATE TABLE IF NOT EXISTS category (\n"
           + "	id              integer   PRIMARY KEY AUTOINCREMENT,\n"
           + "	guid            text      UNIQUE NOT NULL,\n"
           + "	parent_id       integer   NULL,\n"
           + "	created         timestamp NOT NULL DEFAULT(STRFTIME('%Y-%m-%d %H:%M:%f', 'now', 'localtime')),\n"
           + "	updated         timestamp NULL,\n"
           + "	name            text      NOT NULL,\n"
           + "	description     text      NULL,\n"
           + "	order_code      integer   NOT NULL DEFAULT " + Category.DEFAULT_ORDER + ",\n"
           + "	sort_column     text      NULL,\n"
           + "	preview_file_id integer   NULL,\n"
           + "  CONSTRAINT      fk_c_file FOREIGN KEY(preview_file_id) REFERENCES file(id) ON DELETE SET NULL\n"
           + ");");
    sqlMap.put("file",
            "CREATE TABLE IF NOT EXISTS file (\n"
            + "	id          integer   PRIMARY KEY AUTOINCREMENT,\n"
            + "	category_id integer   NULL,\n"
            + "	guid        text      UNIQUE NOT NULL,\n"
            + "	created     timestamp NOT NULL DEFAULT(STRFTIME('%Y-%m-%d %H:%M:%f', 'now', 'localtime')),\n"
            + "	updated     timestamp NULL,\n"
            + "	name        text      NULL,\n"
            + "	author      text      NULL,\n"
            + "	source      text      UNIQUE NOT NULL,\n"
            + "	description text      NULL,\n"
            + "	order_code  integer   NOT NULL DEFAULT " + FetchFile.DEFAULT_ORDER + ",\n"
            + "	checksum    integer   NOT NULL DEFAULT " + FetchFile.DEFAULT_CHECKSUM + ",\n"
            + "	grade       text      NULL,\n"
            + "	hidden      int       NOT NULL DEFAULT 0,\n"
            + "	extra_type  text      NULL,\n"
            + "	extra       text      NULL,\n"
            + "	CONSTRAINT  fk_f_category FOREIGN KEY(category_id) REFERENCES category(id) ON DELETE SET NULL\n"
            + ");");
    sqlMap.put("user",
            "CREATE TABLE IF NOT EXISTS user (\n"
            + "	id          integer   PRIMARY KEY AUTOINCREMENT,\n"
            + "	login       text      UNIQUE NOT NULL,\n"
            + "	password    text      NOT NULL\n"
            + ");");
    sqlMap.put("user_category",
            "CREATE TABLE IF NOT EXISTS user_category (\n"
            + "	user_id     integer   NOT NULL,\n"
            + "	category_id integer   NOT NULL,\n"
            + " CONSTRAINT  fk_uc_user          FOREIGN KEY(user_id)     REFERENCES user(id)     ON DELETE CASCADE,\n"
            + " CONSTRAINT  fk_uc_category      FOREIGN KEY(category_id) REFERENCES category(id) ON DELETE CASCADE,\n"
            + " CONSTRAINT  ak_uc_user_category UNIQUE(user_id, category_id)\n"
            + ");");
    //@formatter:on

    for (var entry : sqlMap.entrySet()) {
      LOG.info("Table '{}' created.", entry.getKey());
      execute(conn, entry.getValue());
    }
  }

  private static void execute(Connection conn, String sql) throws SQLException {
    try (Statement stmt = conn.createStatement()) {
      stmt.execute(sql);
    }
  }
}
