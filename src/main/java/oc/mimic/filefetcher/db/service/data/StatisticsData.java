package oc.mimic.filefetcher.db.service.data;

/**
 * Statistika.
 *
 * @author mimic
 */
public final class StatisticsData {

  private long totalCategories;
  private long totalFiles;
  private long totalImages;
  private long totalVideos;
  private long totalHiddenFiles;
  private long totalUsers;
  private long totalFilesSize;

  public long getTotalCategories() {
    return totalCategories;
  }

  public void setTotalCategories(long totalCategories) {
    this.totalCategories = totalCategories;
  }

  public long getTotalFiles() {
    return totalFiles;
  }

  public void setTotalFiles(long totalFiles) {
    this.totalFiles = totalFiles;
  }

  public long getTotalHiddenFiles() {
    return totalHiddenFiles;
  }

  public void setTotalHiddenFiles(long totalHiddenFiles) {
    this.totalHiddenFiles = totalHiddenFiles;
  }

  public long getTotalUsers() {
    return totalUsers;
  }

  public void setTotalUsers(long totalUsers) {
    this.totalUsers = totalUsers;
  }

  public long getTotalFilesSize() {
    return totalFilesSize;
  }

  public void setTotalFilesSize(long totalFilesSize) {
    this.totalFilesSize = totalFilesSize;
  }

  public long getTotalImages() {
    return totalImages;
  }

  public void setTotalImages(long totalImages) {
    this.totalImages = totalImages;
  }

  public long getTotalVideos() {
    return totalVideos;
  }

  public void setTotalVideos(long totalVideos) {
    this.totalVideos = totalVideos;
  }
}
