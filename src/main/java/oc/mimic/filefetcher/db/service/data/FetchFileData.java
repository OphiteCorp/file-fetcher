package oc.mimic.filefetcher.db.service.data;

import oc.mimic.filefetcher.db.entity.FetchFile;

/**
 * Kompletní informace o souboru.
 *
 * @author mimic
 */
public final class FetchFileData {

  private FetchFile file;
  private ImageExtraData extra;

  public FetchFile getFile() {
    return file;
  }

  public void setFile(FetchFile file) {
    this.file = file;
  }

  public ImageExtraData getExtra() {
    return extra;
  }

  public void setExtra(ImageExtraData extra) {
    this.extra = extra;
  }
}
