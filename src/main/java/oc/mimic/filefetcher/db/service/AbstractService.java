package oc.mimic.filefetcher.db.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import oc.mimic.filefetcher.config.ApplicationConfig;
import oc.mimic.filefetcher.db.dao.CategoryDao;
import oc.mimic.filefetcher.db.dao.FetchFileDao;
import oc.mimic.filefetcher.db.entity.Category;
import oc.mimic.filefetcher.db.entity.FetchFile;
import oc.mimic.filefetcher.exception.FileFetcherException;
import oc.mimic.filefetcher.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;

/**
 * Základní DAO pro správu databáze.
 *
 * @author mimic
 */
public abstract class AbstractService {

  private static final Logger LOG = LoggerFactory.getLogger(AbstractService.class);

  public static final String NEW_GUID_MODIFIER = "?";

  @Autowired
  private ApplicationConfig applicationConfig;

  /**
   * Odebere modifikátor lomítka z GUID.
   */
  public static String removeGuidSlash(String guid) {
    if (guid == null) {
      return null;
    }
    int slash = guid.indexOf('/');
    if (slash > 0) {
      return guid.substring(0, slash);
    }
    return guid;
  }

  /**
   * Získá konfiguraci aplikace.
   */
  protected final ApplicationConfig getConfig() {
    return applicationConfig;
  }

  /**
   * Získá připojení do databáze.
   * Připojení není zavřeno. Je potřeba ho poté uzavřít ručně!
   */
  protected Connection connect() {
    try {
      String dbFileName = applicationConfig.getDatabaseFile();
      String url = "jdbc:sqlite:" + dbFileName;
      Connection conn = DriverManager.getConnection(url);

      // přidá podporu cizích klíčů
      try (PreparedStatement pstmt = conn.prepareStatement("PRAGMA foreign_keys = ON")) {
        pstmt.executeUpdate();
      }
      return conn;

    } catch (SQLException e) {
      LOG.error("An error occurred while establishing a connection to the database.", e);
      throw new FileFetcherException(e);
    }
  }

  /**
   * Vyhledá a získá ID kategorie podle GUID.
   */
  protected static Long findCategoryId(CategoryDao categoryDao, Connection conn, String guid)
          throws SQLException, ParseException, JsonProcessingException {

    Category category = categoryDao.read(conn, guid);
    if (category != null) {
      return category.getId();
    }
    return null;
  }

  /**
   * Vyhledá a získá ID souboru podle GUID.
   */
  protected static Long findFileId(FetchFileDao fetchFileDao, Connection conn, String guid)
          throws SQLException, ParseException, JsonProcessingException {

    FetchFile ff = fetchFileDao.read(conn, guid);
    if (ff != null) {
      return ff.getId();
    }
    return null;
  }

  /**
   * Získá nový GUID z aktuálního.
   * GUID, které může obsahovat modifikátor jako /? nebo /nový_guid
   */
  protected static String readNewGuid(String guid) {
    if (guid == null) {
      return null;
    }
    int slash = guid.indexOf('/');
    if (slash > 0) {
      String newGuid = guid.substring(slash + 1);

      if (NEW_GUID_MODIFIER.equals(newGuid)) {
        newGuid = Utils.generateUuid();
      }
      return newGuid;
    }
    return null;
  }
}
