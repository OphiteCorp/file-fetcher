package oc.mimic.filefetcher.db.service.data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import oc.mimic.filefetcher.db.entity.enums.FileExtraType;

/**
 * Rozhraní pro extra informace o souboru.
 *
 * @author mimic
 */
public interface IFileExtraData {

  /**
   * Převede extra objekt do JSON.
   */
  @JsonIgnore
  String toJson() throws JsonProcessingException;

  /**
   * Typ extra informací.
   */
  @JsonIgnore
  FileExtraType getExtraType();

  /**
   * Šířka obrázku nebo náhledu.
   */
  int getWidth();

  /**
   * Výška obrázku nebo náhledu.
   *
   * @return
   */
  int getHeight();

  /**
   * Získá původní objekt z dat.
   */
  static Object toObject(String extraData, FileExtraType extraType) throws JsonProcessingException {
    ObjectMapper mapper = new JsonMapper();
    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    return mapper.readValue(extraData, extraType.getExtraClass());
  }
}
