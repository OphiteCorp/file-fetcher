package oc.mimic.filefetcher.db.service.data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import oc.mimic.filefetcher.config.ApplicationConfig;
import oc.mimic.filefetcher.db.entity.enums.FileExtraType;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.MessageFormat;

/**
 * Extra informace o videu.
 *
 * @author mimic
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public final class VideoExtraData implements IFileExtraData {

  @JsonProperty("codec")
  private String codec;

  @JsonProperty("width")
  private int width;

  @JsonProperty("height")
  private int height;

  @JsonProperty("duration")
  private double duration;

  @JsonProperty("total_frames")
  private int totalFrames;

  @JsonProperty("color_type")
  private String colorType;

  @JsonProperty("bits_per_pixel")
  private int bitsPerPixel;

  @Override
  public FileExtraType getExtraType() {
    return FileExtraType.VIDEO;
  }

  @Override
  public String toJson() throws JsonProcessingException {
    ObjectMapper mapper = new JsonMapper();
    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    return mapper.writeValueAsString(this);
  }

  /**
   * Ziska nazev nahladu videa.
   */
  public static String getPreviewFileName(String guid) {
    return MessageFormat.format("video-{0}", guid);
  }

  /**
   * Ziska absolutni cestu k nahladu videa.
   */
  public static Path getPreviewPath(ApplicationConfig config, String guid) {
    Path videoCachePath = Paths.get(config.getVideoCacheDirectory()).toAbsolutePath();
    File videoCacheDir = videoCachePath.toFile();

    if (!videoCacheDir.exists()) {
      videoCacheDir.mkdirs();
    }
    String cacheFileExt = config.getVideoPreviewFileExt();
    String cacheFileName = getPreviewFileName(guid);
    return Paths.get(videoCacheDir.getAbsolutePath(), cacheFileName + "." + cacheFileExt);
  }

  @Override
  public int getWidth() {
    return width;
  }

  public void setWidth(int width) {
    this.width = width;
  }

  @Override
  public int getHeight() {
    return height;
  }

  public void setHeight(int height) {
    this.height = height;
  }

  public double getDuration() {
    return duration;
  }

  public void setDuration(double duration) {
    this.duration = duration;
  }

  public int getTotalFrames() {
    return totalFrames;
  }

  public void setTotalFrames(int totalFrames) {
    this.totalFrames = totalFrames;
  }

  public String getColorType() {
    return colorType;
  }

  public void setColorType(String colorType) {
    this.colorType = colorType;
  }

  public int getBitsPerPixel() {
    return bitsPerPixel;
  }

  public void setBitsPerPixel(int bitsPerPixel) {
    this.bitsPerPixel = bitsPerPixel;
  }

  public String getCodec() {
    return codec;
  }

  public void setCodec(String codec) {
    this.codec = codec;
  }
}
