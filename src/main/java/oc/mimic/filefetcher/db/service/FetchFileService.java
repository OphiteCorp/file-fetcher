package oc.mimic.filefetcher.db.service;

import oc.mimic.filefetcher.controller.model.file.CreateFetchFileModelForm;
import oc.mimic.filefetcher.controller.model.file.UpdateFetchFileModelForm;
import oc.mimic.filefetcher.db.dao.CategoryDao;
import oc.mimic.filefetcher.db.dao.FetchFileDao;
import oc.mimic.filefetcher.db.entity.Category;
import oc.mimic.filefetcher.db.entity.FetchFile;
import oc.mimic.filefetcher.db.entity.enums.FileExtraType;
import oc.mimic.filefetcher.db.service.data.CredentialData;
import oc.mimic.filefetcher.db.service.data.IFileExtraData;
import oc.mimic.filefetcher.db.service.data.ImageExtraData;
import oc.mimic.filefetcher.db.service.data.VideoExtraData;
import oc.mimic.filefetcher.exception.FileFetcherException;
import oc.mimic.filefetcher.exception.UnsupportedSourceTypeException;
import oc.mimic.filefetcher.utils.Utils;
import oc.mimic.filefetcher.utils.Validator;
import org.apache.commons.imaging.ImageInfo;
import org.apache.commons.imaging.ImageReadException;
import org.apache.commons.imaging.Imaging;
import org.jcodec.api.FrameGrab;
import org.jcodec.api.UnsupportedFormatException;
import org.jcodec.common.DemuxerTrackMeta;
import org.jcodec.common.VideoCodecMeta;
import org.jcodec.common.io.FileChannelWrapper;
import org.jcodec.common.io.NIOUtils;
import org.jcodec.common.model.Picture;
import org.jcodec.scale.AWTUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.sqlite.SQLiteErrorCode;
import org.sqlite.SQLiteException;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.sql.Connection;
import java.util.List;
import java.util.Objects;

/**
 * Služba pro soubory.
 *
 * @author mimic
 */
@Service
public class FetchFileService extends AbstractService {

  private static final Logger LOG = LoggerFactory.getLogger(FetchFileService.class);

  @Autowired
  private CategoryDao categoryDao;

  @Autowired
  private FetchFileDao fetchFileDao;

  @Autowired
  private CategoryService categoryService;

  /**
   * Vyhodnotí, jestli je přístup k souboru povolen.
   */
  public boolean isAccessAllowed(String guid, CredentialData credential) {
    try (Connection conn = connect()) {
      FetchFile ff = fetchFileDao.read(conn, guid);

      if (ff != null && ff.getCategoryGuid() != null) {
        // soubor existuje a má nadřízenou kategorii
        Category category = categoryDao.read(conn, ff.getCategoryGuid());
        return categoryService.isAccessAllowed(category.getGuid(), credential);

      } else if (ff != null) {
        // soubor existuje, ale nemá žádnou kategorii
        return true;
      }
    } catch (Exception e) {
      LOG.error("Error evaluating permissions on file:" + guid, e);
      throw new FileFetcherException(e);
    }
    return false;
  }

  /**
   * Získá všechny soubory.
   */
  public List<FetchFile> getAll() {
    try (Connection conn = connect()) {
      return fetchFileDao.getAll(conn);

    } catch (Exception e) {
      LOG.error("Could not get files.", e);
      throw new FileFetcherException(e);
    }
  }

  /**
   * Získá soubor podle ID.
   */
  public FetchFile getById(Long id) {
    try (Connection conn = connect()) {
      return fetchFileDao.read(conn, id);

    } catch (Exception e) {
      LOG.error("Unable to get file by ID: " + id, e);
      throw new FileFetcherException(e);
    }
  }

  /**
   * Získá soubor podle GUID.
   */
  public FetchFile getByGuid(String guid) {
    try (Connection conn = connect()) {
      guid = removeGuidSlash(guid);
      return fetchFileDao.read(conn, guid);

    } catch (Exception e) {
      LOG.error("Unable to get file by GUID: " + guid, e);
      throw new FileFetcherException(e);
    }
  }

  /**
   * Získá veřejné náhodné soubory.
   */
  public List<FetchFile> getRandomPublicFiles(String categoryGuid, int count) {
    try (Connection conn = connect()) {
      count = Math.max(count, 1);
      return fetchFileDao.getRandomPublicFiles(conn, categoryGuid, count);

    } catch (Exception e) {
      LOG.error("Unable to get random file. The input GUID of the category is:" + categoryGuid, e);
      throw new FileFetcherException(e);
    }
  }

  /**
   * Získá soubory podle GUID kategorie.
   */
  public List<FetchFile> getByCategory(String categoryGuid) {
    try (Connection conn = connect()) {
      return fetchFileDao.getByCategory(conn, categoryGuid);

    } catch (Exception e) {
      LOG.error("Unable to get files by category GUID: " + categoryGuid, e);
      throw new FileFetcherException(e);
    }
  }

  /**
   * Vytvoří nový soubor.
   */
  public FetchFile create(CreateFetchFileModelForm createForm) {
    if (isSourceFileInvalid(createForm.getSource())) {
      throw new UnsupportedSourceTypeException("File not supported: " + createForm.getSource());
    }
    IFileExtraData extraData = null;
    String guid = null;

    try (Connection conn = connect()) {
      Long categoryId = findCategoryId(categoryDao, conn, createForm.getCategory());
      guid = (createForm.getGuid() == null) ? Utils.generateUuid() : createForm.getGuid();
      extraData = getExtraData(guid, createForm.getSource());
      long checksum = calculateChecksum(createForm.getSource(), Objects.requireNonNull(extraData).getExtraType());

      return fetchFileDao.create(conn, guid, createForm.getName(), createForm.getAuthor(), createForm.getDescription(),
              createForm.getOrder(), createForm.getSource(), categoryId, checksum, createForm.getGrade(),
              createForm.isHidden(), extraData);

    } catch (SQLiteException e) {
      throw catchFetchFileExistsException(e, createForm.getGuid(), null, createForm.getSource());

    } catch (Exception e) {
      if (extraData != null && extraData.getExtraType() == FileExtraType.VIDEO) {
        Path previewPath = VideoExtraData.getPreviewPath(getConfig(), guid);

        if (previewPath.toFile().delete()) {
          LOG.info("The file was deleted: {}", previewPath);
        }
      }
      if (e instanceof FileFetcherException) {
        throw (FileFetcherException) e;
      } else {
        LOG.error("Could not create new file.", e);
        throw new FileFetcherException(e);
      }
    }
  }

  /**
   * Aktualizuje soubor.
   */
  public FetchFile update(UpdateFetchFileModelForm updateForm) {
    if (isSourceFileInvalid(updateForm.getSource())) {
      throw new UnsupportedSourceTypeException("File not supported: " + updateForm.getSource());
    }
    String newGuid = readNewGuid(updateForm.getGuid());
    String formGuid = removeGuidSlash(updateForm.getGuid());
    IFileExtraData extraData = null;
    FetchFile ff = null;

    try (Connection conn = connect()) {
      ff = fetchFileDao.read(conn, formGuid);
      if (ff == null) {
        return null;
      }
      Long categoryId = findCategoryId(categoryDao, conn, updateForm.getCategory());
      String finalGuid = (newGuid == null) ? formGuid : newGuid;
      long checksum = calculateChecksum(updateForm.getSource(), ff.getExtraType());

      // nutne smazat puvodni nahled videa, protoze se vytvori novy
      if (ff.getExtraType() == FileExtraType.VIDEO) {
        Path previewPath = VideoExtraData.getPreviewPath(getConfig(), ff.getGuid());

        if (previewPath.toFile().delete()) {
          LOG.info("The file was deleted: {}", previewPath);
        }
      }
      extraData = getExtraData(finalGuid, updateForm.getSource());

      return fetchFileDao.update(conn, formGuid, newGuid, updateForm.getName(), updateForm.getAuthor(),
              updateForm.getDescription(), updateForm.getOrder(), updateForm.getSource(), categoryId, checksum,
              updateForm.getGrade(), updateForm.isHidden(), extraData);

    } catch (SQLiteException e) {
      throw catchFetchFileExistsException(e, formGuid, newGuid, updateForm.getSource());

    } catch (Exception e) {
      if (extraData != null && extraData.getExtraType() == FileExtraType.VIDEO) {
        Path previewPath = VideoExtraData.getPreviewPath(getConfig(), ff.getGuid());

        if (previewPath.toFile().delete()) {
          LOG.info("The file was deleted: {}", previewPath);
        }
      }
      if (e instanceof FileFetcherException) {
        throw (FileFetcherException) e;
      } else {
        LOG.error("Could not update file with GUID: " + updateForm.getGuid(), e);
        throw new FileFetcherException(e);
      }
    }
  }

  /**
   * Smaže soubor podle GUID.
   */
  public boolean delete(String guid) {
    try (Connection conn = connect()) {
      FetchFile ff = fetchFileDao.read(conn, guid);

      if (ff != null && fetchFileDao.delete(conn, guid)) {
        if (ff.getExtraType() == FileExtraType.VIDEO) {
          Path previewPath = VideoExtraData.getPreviewPath(getConfig(), ff.getGuid());

          if (previewPath.toFile().delete()) {
            LOG.info("The file was deleted: {}", previewPath);
          }
        }
        return true;
      }
    } catch (Exception e) {
      LOG.error("Unable to delete file: " + guid, e);
      throw new FileFetcherException(e);
    }
    return false;
  }

  /**
   * Smaže všechny soubory.
   */
  public List<FetchFile> deleteAll() {
    try (Connection conn = connect()) {
      List<FetchFile> files = fetchFileDao.getAll(conn);
      fetchFileDao.deleteAll(conn);
      return files;

    } catch (Exception e) {
      LOG.error("It was not possible to delete all files.", e);
      throw new FileFetcherException(e);
    }
  }

  /**
   * Získá maximální hodnotu řazení pro soubory.
   */
  public int getMaxOrder() {
    try (Connection conn = connect()) {
      return fetchFileDao.getMaxOrder(conn);

    } catch (Exception e) {
      LOG.error("Could not get maximum order value for files.", e);
      throw new FileFetcherException(e);
    }
  }

  private boolean isSourceFileInvalid(String source) {
    File file = new File(source);
    boolean supportedImage = Validator.isSupportedFile(file, getConfig().getSupportedImageTypes());
    boolean supportedVideo = Validator.isSupportedFile(file, getConfig().getSupportedVideoTypes());
    return !(supportedImage || supportedVideo);
  }

  private IFileExtraData getExtraData(String fileGuid, String source) {
    try {
      File sourceFile = new File(source);
      LOG.debug("Getting extra file information...");

      // získá informace o videu; aktualne popdoruje pouze mp4
      try {
        FileChannelWrapper channel = NIOUtils.readableChannel(sourceFile);
        FrameGrab grab = FrameGrab.createFrameGrab(channel);
        DemuxerTrackMeta meta = grab.getVideoTrack().getMeta();
        VideoCodecMeta videoCodecMeta = meta.getVideoCodecMeta();
        Picture framePicture = grab.getNativeFrame();
        BufferedImage frame = AWTUtil.toBufferedImage(framePicture);

        // uloží náhled videa do cache
        String cacheFileExt = getConfig().getVideoPreviewFileExt();
        Path cacheFilePath = VideoExtraData.getPreviewPath(getConfig(), fileGuid);
        ImageIO.write(frame, cacheFileExt, cacheFilePath.toFile());

        VideoExtraData extra = new VideoExtraData();
        extra.setCodec(meta.getCodec().toString());
        extra.setWidth(videoCodecMeta.getSize().getWidth());
        extra.setHeight(videoCodecMeta.getSize().getHeight());
        extra.setDuration(meta.getTotalDuration());
        extra.setTotalFrames(meta.getTotalFrames());
        extra.setColorType(videoCodecMeta.getColor().toString());
        extra.setBitsPerPixel(videoCodecMeta.getColor().bitsPerPixel);
        return extra;

      } catch (UnsupportedFormatException e) {
        LOG.debug("Could not get extra information about video: '" + source + "'. Returns null.");
      }
      // získá informace o obrázku
      try {
        ImageInfo info = Imaging.getImageInfo(sourceFile);
        ImageExtraData extra = new ImageExtraData();
        extra.setWidth(info.getWidth());
        extra.setHeight(info.getHeight());
        extra.setBitsPerPixel(info.getBitsPerPixel());
        extra.setFormat(info.getFormatDetails());
        extra.setColorType(info.getColorType().name());
        extra.setCompressionAlgorithm(info.getCompressionAlgorithm().name());
        extra.setTransparent(info.isTransparent());
        return extra;

      } catch (ImageReadException e) {
        LOG.debug("Could not get extra information about image: '" + source + "'. Returns null.");
      }
    } catch (Exception e) {
      LOG.error("Could not get extra information about file: '" + source + "'. Returns null.");
    }
    return null;
  }

  private long calculateChecksum(String source, FileExtraType extraType) throws IOException {
    long checksum = FetchFile.DEFAULT_CHECKSUM;

    if (source != null) {
      File file = new File(source);

      if (file.exists()) {
        boolean calcChecksum = (extraType == FileExtraType.VIDEO && getConfig().isVideoCalculateChecksum()) ||
                               (extraType == FileExtraType.IMAGE && getConfig().isImageCalculateChecksum());

        if (calcChecksum) {
          checksum = Utils.calculateFileChecksum(file);
        }
      }
    }
    return checksum;
  }

  private static FileFetcherException catchFetchFileExistsException(SQLiteException e, String formGuid, String newGuid,
          String source) {

    if (e.getResultCode() == SQLiteErrorCode.SQLITE_CONSTRAINT) {
      if (newGuid != null) {
        LOG.warn("Current GUID: {}. This file already exists: GUID={}, Source={}", formGuid, newGuid, source);
      } else {
        LOG.warn("This file already exists: GUID={}, Source={}", formGuid, source);
      }
    } else {
      LOG.error("Could not create or update fetch file.", e);
    }
    return new FileFetcherException(e);
  }
}
