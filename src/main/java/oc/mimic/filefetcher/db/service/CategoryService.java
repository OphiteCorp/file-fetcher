package oc.mimic.filefetcher.db.service;

import oc.mimic.filefetcher.config.ApplicationConfig;
import oc.mimic.filefetcher.controller.model.category.CreateCategoryModelForm;
import oc.mimic.filefetcher.controller.model.category.UpdateCategoryModelForm;
import oc.mimic.filefetcher.db.dao.CategoryDao;
import oc.mimic.filefetcher.db.dao.FetchFileDao;
import oc.mimic.filefetcher.db.dao.UserDao;
import oc.mimic.filefetcher.db.dao.data.CategoryExtra;
import oc.mimic.filefetcher.db.dao.data.CategoryPath;
import oc.mimic.filefetcher.db.entity.Category;
import oc.mimic.filefetcher.db.entity.FetchFile;
import oc.mimic.filefetcher.db.entity.User;
import oc.mimic.filefetcher.db.service.data.CategoryData;
import oc.mimic.filefetcher.db.service.data.CredentialData;
import oc.mimic.filefetcher.exception.FileFetcherException;
import oc.mimic.filefetcher.utils.Hash;
import oc.mimic.filefetcher.utils.Utils;
import oc.mimic.filefetcher.utils.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.sqlite.SQLiteErrorCode;
import org.sqlite.SQLiteException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Služba pro kategorie.
 *
 * @author mimic
 */
@Service
public class CategoryService extends AbstractService {

  private static final Logger LOG = LoggerFactory.getLogger(CategoryService.class);

  @Autowired
  private ApplicationConfig applicationConfig;

  @Autowired
  private CategoryDao categoryDao;

  @Autowired
  private UserDao userDao;

  @Autowired
  private FetchFileDao fetchFileDao;

  /**
   * Vyhodnotí, jestli je přístup do kategorie povolen.
   */
  public boolean isAccessAllowed(String guid, CredentialData credential) {
    try (Connection conn = connect()) {
      Category category = categoryDao.read(conn, guid);

      if (category != null && category.getUsers().isEmpty()) {
        // kategorie nemá žádné oprávnění na uživatele
        return true;

      } else if (category == null) {
        // kategorie neexistuje
        return false;
      }
      return isCredentialValid(conn, category, credential);

    } catch (Exception e) {
      LOG.error("Error evaluating permissions on category: " + guid, e);
      throw new FileFetcherException(e);
    }
  }

  /**
   * Získá podkategorie podle GUID.
   */
  public List<CategoryData> getSubcategories(String guid, CredentialData credential, boolean fetchExtraData) {
    List<CategoryData> subcategoriesData = new ArrayList<>();

    try (Connection conn = connect()) {
      // pokud se nebude jednat o root, tak je nutné zkontrolovat oprávnění na kategorii
      if (guid != null && credential != null) {
        Category category = categoryDao.read(conn, guid);
        if (category == null || !isCredentialValid(conn, category, credential)) {
          LOG.debug("Unauthorized access to a category with GUID: {}", guid);
          return subcategoriesData;
        }
      }
      List<Category> subcategories = categoryDao.getSubcategories(conn, guid);
      for (Category category : subcategories) {
        FetchFile previewFile = category.getPreviewFile();
        CategoryData data = new CategoryData();
        data.setCategory(category);

        if (fetchExtraData) {
          boolean accessAllowed = (credential == null) || isCredentialValid(conn, category, credential);
          data.setProtected(!category.getUsers().isEmpty());
          data.setAccessAllowed(accessAllowed);
          data.setNewCategory(Validator.isDateNewerThen(category.getCreated(), applicationConfig.getMarkAsNewInDays()));
          data.setNewCategoryLeft(Utils.getElapsedTimeFromNow(
                  Utils.addDaysToDate(category.getCreated(), applicationConfig.getMarkAsNewInDays())));

          // z bezpečtnostních důvodů skryjeme obsah, pokud není oprávnění
          if (accessAllowed) {
            CategoryExtra extra = categoryDao.getCategoryExtra(conn, category.getGuid());
            data.setExtra(extra);
          }
        }
        // pokud soubor nemá extra data, ale kategorie má náhled
        if (data.getExtra() == null && previewFile != null) {
          CategoryExtra extra = new CategoryExtra();
          extra.setPreviewGuid(previewFile.getGuid());
          extra.setPreviewChecksum(previewFile.getChecksum());
          extra.setPreviewWidth(previewFile.getExtra().getWidth());
          extra.setPreviewHeight(previewFile.getExtra().getHeight());
          data.setExtra(extra);

          // kategorie má extra data, ale má i náhled - v tomto případě přepíšeme extra náhled
        } else if (data.getExtra() != null && previewFile != null) {
          data.getExtra().setPreviewGuid(previewFile.getGuid());
          data.getExtra().setPreviewChecksum(previewFile.getChecksum());
          data.getExtra().setPreviewWidth(previewFile.getExtra().getWidth());
          data.getExtra().setPreviewHeight(previewFile.getExtra().getHeight());
        }
        subcategoriesData.add(data);
      }
      return subcategoriesData;

    } catch (Exception e) {
      LOG.error("Unable to get subcategories by GUID: " + guid, e);
      throw new FileFetcherException(e);
    }
  }

  /**
   * Získá cestu na kategorie od ROOT až po kategorii s GUID.
   */
  public List<CategoryPath> getCategoryPaths(String guid) {
    CategoryPath rootPath = new CategoryPath("", "/", null);

    if (guid != null) {
      try (Connection conn = connect()) {
        List<CategoryPath> paths = categoryDao.getCategoryPaths(conn, guid);
        paths.add(0, rootPath);
        return paths;

      } catch (SQLException e) {
        LOG.error("Unable to get category paths by GUID: " + guid, e);
        throw new FileFetcherException(e);
      }
    } else {
      List<CategoryPath> paths = new ArrayList<>(1);
      paths.add(rootPath);
      return paths;
    }
  }

  /**
   * Přiřadí soubory do kategorie.
   */
  public List<FetchFile> assignFilesToCategory(List<String> filesGuids, String categoryGuid, Boolean withoutCategory) {
    List<FetchFile> assignFiles = new ArrayList<>();
    boolean withoutCat = (withoutCategory != null && withoutCategory);

    try (Connection conn = connect()) {
      if (categoryGuid == null) {
        // odebere souborům kategorii
        for (String fileGuid : filesGuids) {
          FetchFile ff = fetchFileDao.read(conn, fileGuid);

          if (ff != null) {
            if (fetchFileDao.assignFileToCategory(conn, ff.getGuid(), null)) {
              assignFiles.add(ff);
              LOG.info("The '{}' file category has been removed.", ff.getGuid());
            }
          } else {
            LOG.warn("The file with GUID '{}' does not exist.", fileGuid);
          }
        }
      } else {
        // přiřadí soubory do kategorie
        Long categoryId = findCategoryId(categoryDao, conn, categoryGuid);

        if (categoryId != null) {
          if (withoutCat) {
            // přiřadí všechny soubory bez kategorie do kategorie
            assignFiles = fetchFileDao.getFilesWithoutCategory(conn);
            fetchFileDao.assignFileNotCategoryToCategory(conn, categoryId);

            for (FetchFile ff : assignFiles) {
              LOG.info("The '{}' file has been assigned to the category: {}", ff.getGuid(), categoryGuid);
            }
          } else {
            // přiřadí soubory podle GUID do kategorie
            for (String fileGuid : filesGuids) {
              FetchFile ff = fetchFileDao.read(conn, fileGuid);

              if (ff != null) {
                if (fetchFileDao.assignFileToCategory(conn, ff.getGuid(), categoryId)) {
                  assignFiles.add(ff);
                  LOG.info("The '{}' file has been assigned to the category: {}", ff.getGuid(), categoryGuid);
                }
              } else {
                LOG.warn("The file with GUID '{}' does not exist.", fileGuid);
              }
            }
          }
        } else {
          LOG.warn("Category with GUID '{}' does not exist.", categoryGuid);
        }
      }
    } catch (Exception e) {
      LOG.error("An error occurred while assigning files to category: " + categoryGuid, e);
      throw new FileFetcherException(e);
    }
    return assignFiles;
  }

  /**
   * Získá všechny kategorie.
   */
  public List<Category> getAll() {
    try (Connection conn = connect()) {
      return categoryDao.getAll(conn);

    } catch (Exception e) {
      LOG.error("Could not get categories.", e);
      throw new FileFetcherException(e);
    }
  }

  /**
   * Získá kategorii podle ID.
   */
  public Category getById(Long id) {
    try (Connection conn = connect()) {
      return categoryDao.read(conn, id);

    } catch (Exception e) {
      LOG.error("Unable to get category by ID: " + id, e);
      throw new FileFetcherException(e);
    }
  }

  /**
   * Získá kategorii podle GUID.
   */
  public Category getByGuid(String guid) {
    try (Connection conn = connect()) {
      guid = removeGuidSlash(guid);
      return categoryDao.read(conn, guid);

    } catch (Exception e) {
      LOG.error("Unable to get category by GUID: " + guid, e);
      throw new FileFetcherException(e);
    }
  }

  /**
   * Vytvoří novou kategorii.
   */
  public Category create(CreateCategoryModelForm createForm) {
    try (Connection conn = connect()) {
      Long parentId = findCategoryId(categoryDao, conn, createForm.getParentGuid());
      Long previewFileId = findFileId(fetchFileDao, conn, createForm.getPreviewFile());
      String guid = (createForm.getGuid() == null) ? Utils.generateUuid() : createForm.getGuid();

      return categoryDao.create(conn, guid, createForm.getName(), createForm.getDescription(), createForm.getOrder(),
              createForm.getSortColumn(), parentId, previewFileId);

    } catch (SQLiteException e) {
      throw catchCategoryExistsException(e, createForm.getGuid(), null, createForm.getName());

    } catch (Exception e) {
      LOG.error("Could not create new category.", e);
      throw new FileFetcherException(e);
    }
  }

  /**
   * Aktualizuje kategorii.
   */
  public Category update(UpdateCategoryModelForm updateForm) {
    String newGuid = readNewGuid(updateForm.getGuid());
    String formGuid = removeGuidSlash(updateForm.getGuid());

    try (Connection conn = connect()) {
      Category category = categoryDao.read(conn, formGuid);
      if (category == null) {
        return null;
      }
      Long parentId = findCategoryId(categoryDao, conn, updateForm.getParentGuid());
      Long previewFileId = findFileId(fetchFileDao, conn, updateForm.getPreviewFile());

      return categoryDao
              .update(conn, formGuid, newGuid, updateForm.getName(), updateForm.getDescription(), updateForm.getOrder(),
                      updateForm.getSortColumn(), parentId, previewFileId);

    } catch (SQLiteException e) {
      throw catchCategoryExistsException(e, formGuid, newGuid, updateForm.getName());

    } catch (Exception e) {
      LOG.error("Could not update category with GUID: " + formGuid, e);
      throw new FileFetcherException(e);
    }
  }

  /**
   * Smaže kategorii.
   */
  public boolean delete(String guid) {
    try (Connection conn = connect()) {
      Category category = categoryDao.read(conn, guid);

      if (category != null) {
        return categoryDao.delete(conn, guid);
      }
    } catch (Exception e) {
      LOG.error("Unable to delete category: " + guid, e);
      throw new FileFetcherException(e);
    }
    return false;
  }

  /**
   * Smaže všechny kategorie.
   */
  public List<Category> deleteAll() {
    try (Connection conn = connect()) {
      List<Category> categories = categoryDao.getAll(conn);
      categoryDao.deleteAll(conn);
      return categories;

    } catch (Exception e) {
      LOG.error("It was not possible to delete all categories.", e);
      throw new FileFetcherException(e);
    }
  }

  /**
   * Získá maximální hodnotu řazení pro kategorie.
   */
  public int getMaxOrder() {
    try (Connection conn = connect()) {
      return categoryDao.getMaxOrder(conn);

    } catch (Exception e) {
      LOG.error("Could not get maximum order value for categories.", e);
      throw new FileFetcherException(e);
    }
  }

  private boolean isCredentialValid(Connection conn, Category category, CredentialData credential) throws SQLException {
    if (category.getUsers().isEmpty()) {
      return true;
    }
    if (category.getUsers().contains(credential.getLogin())) {
      User user = userDao.read(conn, credential.getLogin());
      return Hash.isPasswordValid(credential.getPassword(), User.getRawPassword(user.getPassword()));
    }
    return false;
  }

  private static RuntimeException catchCategoryExistsException(SQLiteException e, String formGuid, String newGuid,
          String categoryName) {

    if (e.getResultCode() == SQLiteErrorCode.SQLITE_CONSTRAINT) {
      if (newGuid != null) {
        LOG.warn("Current GUID: {}. This category already exists: GUID={}, Name={}", formGuid, newGuid, categoryName);
      } else {
        LOG.warn("This category already exists: GUID={}, Name={}", formGuid, categoryName);
      }
    } else {
      LOG.error("Could not create or update category.", e);
    }
    return new FileFetcherException(e);
  }
}
