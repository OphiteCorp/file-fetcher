package oc.mimic.filefetcher.db.service.data;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import oc.mimic.filefetcher.db.entity.enums.FileExtraType;

/**
 * Extra informace o obrázku.
 *
 * @author mimic
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public final class ImageExtraData implements IFileExtraData {

  @JsonProperty("width")
  private int width;

  @JsonProperty("height")
  private int height;

  @JsonProperty("format")
  private String format;

  @JsonProperty("bits_per_pixel")
  private int bitsPerPixel;

  @JsonProperty("transparent")
  private boolean transparent;

  @JsonProperty("color_type")
  private String colorType;

  @JsonProperty("compression_alg")
  private String compressionAlgorithm;

  @Override
  public FileExtraType getExtraType() {
    return FileExtraType.IMAGE;
  }

  @Override
  public String toJson() throws JsonProcessingException {
    ObjectMapper mapper = new JsonMapper();
    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    return mapper.writeValueAsString(this);
  }

  @Override
  public int getWidth() {
    return width;
  }

  public void setWidth(int width) {
    this.width = width;
  }

  @Override
  public int getHeight() {
    return height;
  }

  public void setHeight(int height) {
    this.height = height;
  }

  public String getFormat() {
    return format;
  }

  public void setFormat(String format) {
    this.format = format;
  }

  public int getBitsPerPixel() {
    return bitsPerPixel;
  }

  public void setBitsPerPixel(int bitsPerPixel) {
    this.bitsPerPixel = bitsPerPixel;
  }

  public boolean isTransparent() {
    return transparent;
  }

  public void setTransparent(boolean transparent) {
    this.transparent = transparent;
  }

  public String getColorType() {
    return colorType;
  }

  public void setColorType(String colorType) {
    this.colorType = colorType;
  }

  public String getCompressionAlgorithm() {
    return compressionAlgorithm;
  }

  public void setCompressionAlgorithm(String compressionAlgorithm) {
    this.compressionAlgorithm = compressionAlgorithm;
  }
}
