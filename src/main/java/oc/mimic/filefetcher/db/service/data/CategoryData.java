package oc.mimic.filefetcher.db.service.data;

import oc.mimic.filefetcher.db.dao.data.CategoryExtra;
import oc.mimic.filefetcher.db.entity.Category;

/**
 * Kompletní informace o kategorii včetně extra dat.
 *
 * @author mimic
 */
public final class CategoryData implements Comparable<CategoryData> {

  private Category category;
  private CategoryExtra extra;
  private boolean isProtected;
  private boolean accessAllowed;
  private boolean newCategory;
  private long newCategoryLeft;

  public Category getCategory() {
    return category;
  }

  public void setCategory(Category category) {
    this.category = category;
  }

  public CategoryExtra getExtra() {
    return extra;
  }

  public void setExtra(CategoryExtra extra) {
    this.extra = extra;
  }

  public boolean isProtected() {
    return isProtected;
  }

  public void setProtected(boolean aProtected) {
    isProtected = aProtected;
  }

  public boolean isAccessAllowed() {
    return accessAllowed;
  }

  public void setAccessAllowed(boolean accessAllowed) {
    this.accessAllowed = accessAllowed;
  }

  public boolean isNewCategory() {
    return newCategory;
  }

  public void setNewCategory(boolean newCategory) {
    this.newCategory = newCategory;
  }

  public long getNewCategoryLeft() {
    return newCategoryLeft;
  }

  public void setNewCategoryLeft(long newCategoryLeft) {
    this.newCategoryLeft = newCategoryLeft;
  }

  @Override
  public int compareTo(CategoryData categoryData) {
    if (category.getSortColumn() != null) {
      Category c = categoryData.getCategory();

      switch (category.getSortColumn()) {
        case GUID:
          return category.getGuid().compareTo(c.getGuid());

        case NAME:
          return category.getName().compareTo(c.getName());

        case ORDER:
          return Integer.compare(category.getOrder(), c.getOrder());

        case CREATED:
          return category.getCreated().compareTo(c.getCreated());

        case UPDATED:
          if (category.getUpdated() != null) {
            return category.getUpdated().compareTo(c.getUpdated());
          }
      }
    }
    return 0;
  }

  @Override
  public String toString() {
    return category.getName();
  }
}
