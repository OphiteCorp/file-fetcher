package oc.mimic.filefetcher.db.service;

import oc.mimic.filefetcher.controller.model.user.ChangeCategoriesModelForm;
import oc.mimic.filefetcher.controller.model.user.CreateUserModelForm;
import oc.mimic.filefetcher.db.dao.CategoryDao;
import oc.mimic.filefetcher.db.dao.UserDao;
import oc.mimic.filefetcher.db.entity.Category;
import oc.mimic.filefetcher.db.entity.User;
import oc.mimic.filefetcher.db.service.data.CredentialData;
import oc.mimic.filefetcher.exception.FileFetcherException;
import oc.mimic.filefetcher.utils.Hash;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.sqlite.SQLiteException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.sqlite.SQLiteErrorCode.SQLITE_CONSTRAINT;

/**
 * Služba pro uživatele.
 *
 * @author mimic
 */
@Service
public class UserService extends AbstractService {

  private static final Logger LOG = LoggerFactory.getLogger(UserService.class);

  @Autowired
  private UserDao userDao;

  @Autowired
  private CategoryDao categoryDao;

  /**
   * Získá všechny uživatele.
   */
  public List<User> getAll() {
    try (Connection conn = connect()) {
      return userDao.getAll(conn);

    } catch (SQLException e) {
      LOG.error("Could not get users.", e);
      throw new FileFetcherException(e);
    }
  }

  /**
   * Získá uživatele podle loginu.
   */
  public User getByLogin(String login) {
    try (Connection conn = connect()) {
      return userDao.read(conn, login);

    } catch (SQLException e) {
      LOG.error("Unable to get user by login: " + login, e);
      throw new FileFetcherException(e);
    }
  }

  /**
   * Vytvoří nového uživatele.
   */
  public User create(CreateUserModelForm createForm) {
    try (Connection conn = connect()) {
      // test existence uživatele
      User user = userDao.read(conn, createForm.getLogin());
      if (user != null) {
        LOG.warn("User with login '{}' already exists.", createForm.getLogin());
        return null;
      }
      // pokud vstupní heslo bude z importu, tak může obsahovat i hash
      String password = User.getRawPassword(createForm.getPassword());
      if (password.equals(createForm.getPassword())) {
        password = Hash.hashPassword(createForm.getPassword());
      }
      // vytvoření uživatele
      user = userDao.create(conn, createForm.getLogin(), password);
      LOG.info("User '{}' has been created.", user.getLogin());
      return user;

    } catch (SQLiteException e) {
      throw catchUserExistsException(e, createForm.getLogin());

    } catch (Exception e) {
      LOG.error("Could not create new user.", e);
      throw new FileFetcherException(e);
    }
  }

  /**
   * Ověří přihlašovací údaje uživatele.
   */
  public boolean isCredentialValid(CredentialData credential) {
    try (Connection conn = connect()) {
      User user = userDao.read(conn, credential.getLogin());

      if (user != null) {
        return Hash.isPasswordValid(credential.getPassword(), User.getRawPassword(user.getPassword()));
      }
    } catch (Exception e) {
      LOG.error("The password for the '{}' user is not valid.", credential.getLogin());
      throw new FileFetcherException(e);
    }
    return false;
  }

  /**
   * Změní heslo uživatele.
   */
  public boolean changePassword(String login, String newPassword) {
    try (Connection conn = connect()) {
      String password = Hash.hashPassword(newPassword);
      return userDao.changePassword(conn, login, password);

    } catch (Exception e) {
      LOG.error("Could not change user password for: " + login, e);
      throw new FileFetcherException(e);
    }
  }

  /**
   * Smaže uživatele.
   */
  public boolean delete(String login) {
    try (Connection conn = connect()) {
      return userDao.delete(conn, login);

    } catch (SQLException e) {
      LOG.error("Unable to delete user: " + login, e);
      throw new FileFetcherException(e);
    }
  }

  /**
   * Smaže všechny uživatele.
   */
  public List<User> deleteAll() {
    try (Connection conn = connect()) {
      List<User> users = userDao.getAll(conn);
      userDao.deleteAll(conn);
      return users;

    } catch (SQLException e) {
      LOG.error("It was not possible to delete all users.", e);
      throw new FileFetcherException(e);
    }
  }

  /**
   * Přiřadí kategorie uživateli.
   */
  public List<Category> assignCategoriesToUser(ChangeCategoriesModelForm changeForm) {
    try (Connection conn = connect()) {
      User user = userDao.read(conn, changeForm.getLogin());
      boolean append = changeForm.isAppend();
      List<Category> categories = new ArrayList<>();

      if (user != null) {
        if (!append) {
          // smaže všechny oprávnění pro daného uživatele
          int deletedUserCategories = userDao.deleteUserCategories(conn, changeForm.getLogin());
          LOG.debug("{} category accesses for '{}' user have been removed.", deletedUserCategories,
                  changeForm.getLogin());
        }
        if (changeForm.getCategories() != null) {
          for (String categoryGuid : changeForm.getCategories()) {
            Category category = categoryDao.read(conn, categoryGuid);

            // pokud kategorie bude existovat, tak jí přidá oprávnění pro uživatele
            if (category != null) {
              if (tryAssignUserCategory(conn, user, category)) {
                categories.add(category);
                LOG.info("The '{}' category has been assigned to the user: {}", category.getGuid(), user.getLogin());
              }
              // automaticky přidá oprávnění i pro všechny podkateogrie kategorie
              List<Category> subcategories = categoryDao.getAllSubcategories(conn, categoryGuid);
              for (Category subcategory : subcategories) {
                if (tryAssignUserCategory(conn, user, subcategory)) {
                  categories.add(subcategory);
                  LOG.info("The '{}' -> '{}' subcategory was automatically assigned to user: {}", categoryGuid,
                          subcategory.getGuid(), user.getLogin());
                }
              }
            } else {
              LOG.warn("Category '{}' does not exist and cannot be assigned to user: {}", categoryGuid,
                      user.getLogin());
            }
          }
        }
      }
      return categories;

    } catch (Exception e) {
      LOG.error("Could not assign categories to user: " + changeForm.getLogin(), e);
      throw new FileFetcherException(e);
    }
  }

  private boolean tryAssignUserCategory(Connection conn, User user, Category category) throws SQLException {
    try {
      return userDao.assignUserCategory(conn, user.getId(), category.getId());

    } catch (SQLiteException e) {
      // uživatel kategorii již má
      if (e.getResultCode() == SQLITE_CONSTRAINT) {
        return true;
      }
      throw new FileFetcherException(
              "An unexpected error occurred while adding a category '" + category.getGuid() + "' to the user:" +
              user.getLogin(), e);
    }
  }

  private static FileFetcherException catchUserExistsException(SQLiteException e, String login) {
    if (e.getResultCode() == SQLITE_CONSTRAINT) {
      LOG.warn("This user already exists: Login={}", login);
    } else {
      LOG.error("Could not create or update user.", e);
    }
    return new FileFetcherException(e);
  }
}
