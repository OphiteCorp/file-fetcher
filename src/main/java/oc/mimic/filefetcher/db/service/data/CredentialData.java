package oc.mimic.filefetcher.db.service.data;

import oc.mimic.filefetcher.config.ApplicationConfig;
import oc.mimic.filefetcher.utils.Utils;

/**
 * Pověření uživatele.
 *
 * @author mimic
 */
public final class CredentialData {

  private static final String TOKEN_DELIMITER = ":";

  private final String login;
  private final String password;

  public CredentialData(String token, ApplicationConfig config) {
    String[] data = Utils.readToken(token, TOKEN_DELIMITER, config.getTokenOrigin(), config.getTokenMixed());

    if (data != null && data.length == 2) {
      login = data[0];
      password = data[1];
    } else {
      login = null;
      password = null;
    }
  }

  public String getLogin() {
    return login;
  }

  public String getPassword() {
    return password;
  }
}
