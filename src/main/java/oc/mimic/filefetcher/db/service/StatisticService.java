package oc.mimic.filefetcher.db.service;

import oc.mimic.filefetcher.db.dao.CategoryDao;
import oc.mimic.filefetcher.db.dao.FetchFileDao;
import oc.mimic.filefetcher.db.dao.UserDao;
import oc.mimic.filefetcher.db.dao.data.FetchFileStatData;
import oc.mimic.filefetcher.db.entity.Category;
import oc.mimic.filefetcher.db.entity.User;
import oc.mimic.filefetcher.db.service.data.StatisticsData;
import oc.mimic.filefetcher.exception.FileFetcherException;
import oc.mimic.filefetcher.utils.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.sql.Connection;
import java.util.List;

/**
 * Služba pro statistiku.
 *
 * @author mimic
 */
@Service
public class StatisticService extends AbstractService {

  private static final Logger LOG = LoggerFactory.getLogger(StatisticService.class);

  @Autowired
  private UserDao userDao;

  @Autowired
  private CategoryDao categoryDao;

  @Autowired
  private FetchFileDao fetchFileDao;

  /**
   * Získá statistiku.
   */
  public StatisticsData getStatistics() {
    try (Connection conn = connect()) {
      List<Category> categories = categoryDao.getAll(conn);
      List<FetchFileStatData> files = fetchFileDao.getAllForStats(conn);
      List<User> users = userDao.getAll(conn);

      long totalFilesSize = 0;
      long hiddenFiles = 0;
      long totalImages = 0;
      long totalVideos = 0;

      for (FetchFileStatData data : files) {
        File file = new File(data.getSource());

        if (Validator.isFileExists(file)) {
          totalFilesSize += file.length();
        }
        if (data.isHidden()) {
          hiddenFiles++;
        }
        switch (data.getExtraType()) {
          case IMAGE:
            totalImages++;
            break;

          case VIDEO:
            totalVideos++;
            break;
        }
      }
      StatisticsData data = new StatisticsData();
      data.setTotalCategories(categories.size());
      data.setTotalFiles(files.size());
      data.setTotalImages(totalImages);
      data.setTotalVideos(totalVideos);
      data.setTotalHiddenFiles(hiddenFiles);
      data.setTotalUsers(users.size());
      data.setTotalFilesSize(totalFilesSize);
      return data;

    } catch (Exception e) {
      LOG.error("Could not get statistics.", e);
      throw new FileFetcherException(e);
    }
  }
}
