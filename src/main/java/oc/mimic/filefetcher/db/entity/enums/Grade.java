package oc.mimic.filefetcher.db.entity.enums;

/**
 * Známka souboru.
 *
 * @author mimic
 */
public enum Grade {

  S("S"), // nejlepší

  A_PLUS("A+"),
  A("A"),
  A_MINUS("A-"),

  B_PLUS("B+"),
  B("B"),
  B_MINUS("B-"),

  C_PLUS("C+"),
  C("C"),
  C_MINUS("C-"),

  D_PLUS("D+"),
  D("D"),
  D_MINUS("D-"),

  E_PLUS("E+"),
  E("E"),
  E_MINUS("E-"),

  F("F"); // nejhorší

  private final String code;

  Grade(String code) {
    this.code = code;
  }

  public String getCode() {
    return code;
  }

  /**
   * Získá typ podle kódu.
   */
  public static Grade getByCode(String code) {
    for (Grade t : values()) {
      if (t.getCode().equalsIgnoreCase(code)) {
        return t;
      }
    }
    return null;
  }
}
