package oc.mimic.filefetcher.db.entity;

import oc.mimic.filefetcher.db.entity.enums.SortColumnType;

import java.util.Date;
import java.util.Set;

/**
 * DB entita kategorie.
 *
 * @author mimic
 */
public final class Category {

  public static final int DEFAULT_ORDER = 0;

  // řadící sloupce
  public static final String COLUMN_GUID = "guid";
  public static final String COLUMN_NAME = "name";
  public static final String COLUMN_ORDER = "order_code";
  public static final String COLUMN_CREATED = "created";
  public static final String COLUMN_UPDATED = "updated";

  private Long id;
  private String guid;
  private Date created;
  private Date updated;
  private String name;
  private String description;
  private int order;
  private SortColumnType sortColumn;
  private String parentGuid;

  private FetchFile previewFile; // join
  private Set<String> users; // join
  private Set<String> files; // join

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getGuid() {
    return guid;
  }

  public void setGuid(String guid) {
    this.guid = guid;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getOrder() {
    return order;
  }

  public void setOrder(int order) {
    this.order = order;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getParentGuid() {
    return parentGuid;
  }

  public void setParentGuid(String parentGuid) {
    this.parentGuid = parentGuid;
  }

  public Date getCreated() {
    return created;
  }

  public void setCreated(Date created) {
    this.created = created;
  }

  public Date getUpdated() {
    return updated;
  }

  public void setUpdated(Date updated) {
    this.updated = updated;
  }

  public Set<String> getUsers() {
    return users;
  }

  public void setUsers(Set<String> users) {
    this.users = users;
  }

  public Set<String> getFiles() {
    return files;
  }

  public void setFiles(Set<String> files) {
    this.files = files;
  }

  public FetchFile getPreviewFile() {
    return previewFile;
  }

  public void setPreviewFile(FetchFile previewFile) {
    this.previewFile = previewFile;
  }

  public SortColumnType getSortColumn() {
    return sortColumn;
  }

  public void setSortColumn(SortColumnType sortColumn) {
    this.sortColumn = sortColumn;
  }
}
