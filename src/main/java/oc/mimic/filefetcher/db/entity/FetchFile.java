package oc.mimic.filefetcher.db.entity;

import oc.mimic.filefetcher.db.entity.enums.FileExtraType;
import oc.mimic.filefetcher.db.entity.enums.Grade;
import oc.mimic.filefetcher.db.service.data.IFileExtraData;

import java.util.Date;

/**
 * DB entita souboru.
 *
 * @author mimic
 */
public final class FetchFile {

  public static final int DEFAULT_ORDER = 0;
  public static final long DEFAULT_CHECKSUM = 0;

  private Long id;
  private String guid;
  private Date created;
  private Date updated;
  private String name;
  private String author;
  private int order;
  private String description;
  private String categoryGuid;
  private String source;
  private long checksum;
  private Grade grade;
  private boolean hidden;
  private FileExtraType extraType;
  private IFileExtraData extra;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getGuid() {
    return guid;
  }

  public void setGuid(String guid) {
    this.guid = guid;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getOrder() {
    return order;
  }

  public void setOrder(int order) {
    this.order = order;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Date getCreated() {
    return created;
  }

  public void setCreated(Date created) {
    this.created = created;
  }

  public Date getUpdated() {
    return updated;
  }

  public void setUpdated(Date updated) {
    this.updated = updated;
  }

  public String getSource() {
    return source;
  }

  public void setSource(String source) {
    this.source = source;
  }

  public String getCategoryGuid() {
    return categoryGuid;
  }

  public void setCategoryGuid(String categoryGuid) {
    this.categoryGuid = categoryGuid;
  }

  public String getAuthor() {
    return author;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  public long getChecksum() {
    return checksum;
  }

  public void setChecksum(long checksum) {
    this.checksum = checksum;
  }

  public Grade getGrade() {
    return grade;
  }

  public void setGrade(Grade grade) {
    this.grade = grade;
  }

  public boolean isHidden() {
    return hidden;
  }

  public void setHidden(boolean hidden) {
    this.hidden = hidden;
  }

  public FileExtraType getExtraType() {
    return extraType;
  }

  public void setExtraType(FileExtraType extraType) {
    this.extraType = extraType;
  }

  public IFileExtraData getExtra() {
    return extra;
  }

  public void setExtra(IFileExtraData extra) {
    this.extra = extra;
  }
}
