package oc.mimic.filefetcher.db.entity.enums;

import oc.mimic.filefetcher.db.service.data.ImageExtraData;
import oc.mimic.filefetcher.db.service.data.VideoExtraData;

/**
 * Typ extra informací.
 *
 * @author mimic
 */
public enum FileExtraType {

  IMAGE("image", ImageExtraData.class),
  VIDEO("video", VideoExtraData.class);

  private final String code;
  private final Class<?> clazz;

  FileExtraType(String code, Class<?> clazz) {
    this.code = code;
    this.clazz = clazz;
  }

  public String getCode() {
    return code;
  }

  public Class<?> getExtraClass() {
    return clazz;
  }

  /**
   * Získá typ podle kódu.
   */
  public static FileExtraType getByCode(String code) {
    for (FileExtraType t : values()) {
      if (t.getCode().equalsIgnoreCase(code)) {
        return t;
      }
    }
    return null;
  }
}
