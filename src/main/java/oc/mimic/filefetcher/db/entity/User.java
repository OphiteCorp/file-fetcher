package oc.mimic.filefetcher.db.entity;

import java.util.Set;

/**
 * DB entita uživatele.
 *
 * @author mimic
 */
public final class User {

  private static final String REGEX_PASSWORD_IN = "@hash\\[(.+?)]";

  private Long id;
  private String login;
  private String password;

  private Set<String> categories; // join

  /**
   * Získá hash hesla bez "hash" tagu.
   */
  public static String getRawPassword(String password) {
    return password.replaceAll(REGEX_PASSWORD_IN, "$1");
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getLogin() {
    return login;
  }

  public void setLogin(String login) {
    this.login = login;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public Set<String> getCategories() {
    return categories;
  }

  public void setCategories(Set<String> categories) {
    this.categories = categories;
  }
}
