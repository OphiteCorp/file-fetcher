package oc.mimic.filefetcher.db.entity.enums;

import oc.mimic.filefetcher.db.entity.Category;

/**
 * Typ sloupce pro řazení podkategorii.
 *
 * @author mimic
 */
public enum SortColumnType {

  GUID(Category.COLUMN_GUID),
  NAME(Category.COLUMN_NAME),
  ORDER(Category.COLUMN_ORDER),
  CREATED(Category.COLUMN_CREATED),
  UPDATED(Category.COLUMN_UPDATED);

  private final String columnName;

  SortColumnType(String columnName) {
    this.columnName = columnName;
  }

  public String getColumnName() {
    return columnName;
  }

  /**
   * Získá typ podle názvu sloupce.
   */
  public static SortColumnType getByColumnName(String columnName) {
    for (SortColumnType t : values()) {
      if (t.getColumnName().equalsIgnoreCase(columnName)) {
        return t;
      }
    }
    return null;
  }
}
