package oc.mimic.filefetcher.utils;

import oc.mimic.filefetcher.db.entity.FetchFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.zip.CRC32C;
import java.util.zip.Checksum;

/**
 * Obecné pomocné metody.
 *
 * @author mimic
 */
public final class Utils {

  private static final Logger LOG = LoggerFactory.getLogger(Utils.class);
  private static final String SDP_FORMAT = "dd.MM.yyyy | HH:mm:ss.SSS";

  /**
   * Získá aktuální datum a čas ve formátu z {@link Utils#SDP_FORMAT}.
   */
  public static String getCurrentDateTime() {
    return formatDateTime(new Date());
  }

  /**
   * Naformátuje datum podle {@link Utils#SDP_FORMAT}.
   */
  public static String formatDateTime(Date date) {
    return (date != null) ? new SimpleDateFormat(SDP_FORMAT).format(date) : null;
  }

  /**
   * Připočte dny k datu.
   */
  public static Date addDaysToDate(Date date, int days) {
    Calendar c = Calendar.getInstance();
    c.setTime(date);
    c.add(Calendar.DAY_OF_MONTH, days);
    return c.getTime();
  }

  /**
   * Získá uplynulý čas v msec. Pokud bude vstupní datum menší než aktuální, tak vrací 0.
   */
  public static long getElapsedTimeFromNow(Date date) {
    return Math.max(0, date.getTime() - new Date().getTime());
  }

  /**
   * Vygeneruje náhodné a unikátní UUID.
   */
  public static String generateUuid() {
    return UUID.randomUUID().toString().replaceAll("-", "");
  }

  /**
   * Získá absolutní URL s parametry.
   */
  public static String getAbsoluteUrl(HttpServletRequest httpRequest) {
    String query = httpRequest.getQueryString();
    query = (query != null) ? "?" + query : "";
    return httpRequest.getRequestURL().toString() + query;
  }

  /**
   * Překopíruje soubor.
   */
  public static void copyFile(File source, File dest) throws IOException {
    try (InputStream is = new FileInputStream(source); OutputStream os = new FileOutputStream(dest)) {
      byte[] buffer = new byte[4096];
      int length;

      while ((length = is.read(buffer)) > 0) {
        os.write(buffer, 0, length);
      }
    }
  }

  /**
   * Získá typ/příponu souboru.
   */
  public static String getFileExtension(String filename) {
    Optional<String> opt = Optional.ofNullable(filename).filter(f -> f.contains("."))
            .map(f -> f.substring(filename.lastIndexOf(".") + 1));
    return opt.orElse("").toLowerCase();
  }

  /**
   * Vypočte checksum souboru.
   */
  public static long calculateFileChecksum(File file) throws IOException {
    if (file == null || !file.exists()) {
      return FetchFile.DEFAULT_CHECKSUM;
    }
    Checksum checksum = new CRC32C();

    try (BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file))) {
      byte[] bytes = new byte[16 * 1024];
      long totalRead = 0;
      int prevPercent = -1;
      int read;

      LOG.info("Checksum calculation in progress for: {}", file);

      while ((read = bis.read(bytes)) != -1) {
        int percent = (int) Math.round((100. / file.length()) * totalRead);

        // kazdych 10% za loguje
        if (prevPercent < percent && percent % 10 == 0) {
          LOG.info("> {}%", percent);
          prevPercent = percent;
        }
        checksum.update(bytes, 0, read);
        totalRead += read;
      }
    }
    return checksum.getValue();
  }

  /**
   * Prevede base64 zpet na text.
   */
  public static String fromUrlBase64(String base64) {
    return new String(Base64.getUrlDecoder().decode(base64));
  }

  /**
   * Nacte login a hash hasla uzivatele z auth tokenu.
   */
  public static String[] readToken(String token, String tokenDelimiter, String unmixOrigin, String unmix) {
    try {
      token = unmixBase64(token, unmixOrigin, unmix);
      token = Utils.fromUrlBase64(token);
      int index = token.indexOf(tokenDelimiter);
      String login = token.substring(0, index);
      String password = token.substring(index + 1);
      return new String[]{ login, password };

    } catch (Exception e) {
      return null;
    }
  }

  private static String unmixBase64(String value, String origin, String mixed) {
    StringBuilder sb = new StringBuilder();

    for (int i = 0; i < value.length(); i++) {
      boolean found = false;
      int j = 0;

      for (; j < mixed.length(); j++) {
        if (mixed.charAt(j) == value.charAt(i)) {
          found = true;
          break;
        }
      }
      sb.append(found ? origin.charAt(j) : value.charAt(i));
    }
    return sb.toString();
  }
}
