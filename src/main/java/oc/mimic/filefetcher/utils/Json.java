package oc.mimic.filefetcher.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.reflect.Type;

/**
 * Pomocné metody pro práci s JSON.
 *
 * @author mimic
 */
public final class Json {

  private static final Gson GSON = new GsonBuilder().setLenient().create();
  private static final Gson PRETTY_GSON = new GsonBuilder().setLenient().setPrettyPrinting()
          .enableComplexMapKeySerialization().disableHtmlEscaping().generateNonExecutableJson().create();

  // jedná se o 5 bytů, které se přidají na začátek každého JSON a je potřeba je opět odebrat
  // tohle způsobuje generateNonExecutableJson(), který je zase potřeba, aby nedocházelo k escape ani konverzi
  private static final byte[] NON_EXECUTABLE_PREFIX = { 41, 93, 125, 39, 10 };

  /**
   * Převede objekt do JSON.
   */
  public static String toJson(Object object) {
    return GSON.toJson(object);
  }

  /**
   * Převede objekt do JSON. Podporuje formátovaný JSON, který nemusí být zpětně možné převést zpět do objektu.
   */
  public static String toPrettyJson(Object object) {
    String json = PRETTY_GSON.toJson(object);
    StringBuilder sb = new StringBuilder(json);

    if (sb.lastIndexOf(new String(NON_EXECUTABLE_PREFIX)) >= 0) {
      json = sb.substring(NON_EXECUTABLE_PREFIX.length);
    }
    return json;
  }

  /**
   * Převede JSON zpět do původního objektu.
   */
  public static <T> T fromJson(String json, Type type) {
    return GSON.fromJson(json, type);
  }

  /**
   * Pomocí JSON ytvoří kopii objektu.
   */
  public static <T> T clone(Object obj) {
    String json = toJson(obj);
    return fromJson(json, obj.getClass());
  }
}
