package oc.mimic.filefetcher.utils;

import oc.mimic.filefetcher.exception.FileFetcherException;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

/**
 * Pomocné metody pro práci s hashem.
 *
 * @author mimic
 */
public final class Hash {

  /**
   * Vytvoří hash z hesla.
   */
  public static String hashPassword(String password) {
    if (password == null) {
      return null;
    }
    return toSha3_512(password);
  }

  /**
   * Ověří platnost hesla vůči očekávanému hash.
   */
  public static boolean isPasswordValid(String password, String expectedHash) {
    if (expectedHash == null) {
      return true;
    }
    String hash = toSha3_512(password);
    return expectedHash.equals(hash);
  }

  private static String toSha3_512(String password) {
    if (password == null || password.isEmpty()) {
      return null;
    }
    try {
      MessageDigest digest = MessageDigest.getInstance("SHA3-512");
      byte[] bytes = digest.digest(password.getBytes(StandardCharsets.UTF_8));
      return bytesToHex(bytes);
    } catch (Exception e) {
      throw new FileFetcherException("An error occurred while hashing into sha3-512");
    }
  }

  private static String bytesToHex(byte[] hash) {
    StringBuffer sb = new StringBuffer();

    for (byte b : hash) {
      String hex = Integer.toHexString(0xff & b);

      if (hex.length() == 1) {
        sb.append('0');
      }
      sb.append(hex);
    }
    return sb.toString();
  }
}
