package oc.mimic.filefetcher.utils;

import java.io.File;
import java.util.Date;
import java.util.List;

/**
 * Validace ruzných dat.
 *
 * @author mimic
 */
public final class Validator {

  /**
   * Otestuje, zda je řetězec prázdný nebo null.
   */
  public static boolean isNullOrBlank(String value) {
    return (value == null || value.isBlank());
  }

  /**
   * Validace existence souboru.
   */
  public static boolean isFileExists(File file) {
    return (file.exists() && file.isFile() && file.canRead());
  }

  /**
   * Zhodnotí, jestli soubor existuje a je možné ho použít.
   */
  public static boolean isFileExists(String path) {
    return isFileExists(new File(path));
  }

  /**
   * Validace na podporující typ souboru.
   */
  public static boolean isSupportedFile(File file, List<String> supportedTypes) {
    if (file.exists() && file.canRead()) {
      String fileName = file.getName().toLowerCase();
      String ext = Utils.getFileExtension(fileName);
      return supportedTypes.contains(ext);
    }
    return false;
  }

  /**
   * Vyhodnotí, zda je datum s dny novější než aktuální.
   */
  public static boolean isDateNewerThen(Date date, int days) {
    Date dateWithDays = Utils.addDaysToDate(date, days);
    return (Utils.getElapsedTimeFromNow(dateWithDays) > 0);
  }
}
