package oc.mimic.filefetcher.component;

import oc.mimic.filefetcher.config.ApplicationConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.AbstractHandlerMethodMapping;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import java.lang.reflect.Field;
import java.text.MessageFormat;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Pomocná komponenta pro debug springu.
 *
 * @author mimic
 */
@Component
public class SpringDebug {

  private static final Logger LOG = LoggerFactory.getLogger(SpringDebug.class);

  @Autowired
  private RequestMappingHandlerMapping requestMappingHandlerMapping;

  @Autowired
  private ApplicationConfig applicationConfig;

  /**
   * Vypíše do konzole všechny request mapování.
   */
  public void printRequestMappings() {
    try {
      Field mappingRegistry = AbstractHandlerMethodMapping.class.getDeclaredField("mappingRegistry");
      mappingRegistry.setAccessible(true);

      Class<?> mappingRegistryInnerKlass = Class
              .forName("org.springframework.web.servlet.handler.AbstractHandlerMethodMapping$MappingRegistry");
      Field urlLookup = mappingRegistryInnerKlass.getDeclaredField("urlLookup");
      Field mappingLookup = mappingRegistryInnerKlass.getDeclaredField("mappingLookup");
      urlLookup.setAccessible(true);
      mappingLookup.setAccessible(true);

      Object registry = mappingRegistry.get(requestMappingHandlerMapping);
      Map<RequestMappingInfo, HandlerMethod> requestMappingInfoHandlerMethodMap = (Map<RequestMappingInfo, HandlerMethod>) mappingLookup
              .get(registry);

      String contextPath = applicationConfig.getContextPath();
      Set<RequestMappingInfo> requestMappingInfos = requestMappingInfoHandlerMethodMap.keySet();
      List<String> paths = requestMappingInfos.stream().map(p -> {
        String pattern = p.getPatternsCondition().getPatterns().iterator().next();
        return MessageFormat.format("{0}{1} {2}", contextPath, pattern, p.getMethodsCondition().toString());
      }).sorted().collect(Collectors.toList());

      StringBuilder sb = new StringBuilder();
      sb.append("Mapping list:\n\n");

      for (String path : paths) {
        sb.append(path).append("\n");
      }
      LOG.info(sb.toString());

    } catch (Exception e) {
      LOG.error("An error occurred while listing the mapping.", e);
    }
  }
}
