package oc.mimic.filefetcher.component;

import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.resizers.Resizers;
import net.coobird.thumbnailator.resizers.configurations.Rendering;
import oc.mimic.filefetcher.config.ApplicationConfig;
import oc.mimic.filefetcher.exception.FileFetcherException;
import oc.mimic.filefetcher.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.MessageFormat;

/**
 * Zpracuje obrázek.
 *
 * @author mimic
 */
@Component
public class ImageProcessor {

  private static final Logger LOG = LoggerFactory.getLogger(ImageProcessor.class);

  @Autowired
  private ApplicationConfig applicationConfig;

  /**
   * Provede resize a úpravu vstupního obrázku.
   *
   * @param imageFile Obrázek již musí existovat!
   * @param width     Není nastaven limit!
   * @param height    Není nastaven limit!
   * @param rotate
   *
   * @return
   */
  public InputStream resize(File imageFile, Integer width, Integer height, Double rotate) {
    Path path = imageFile.toPath();
    InputStream in = null;

    try (ByteArrayOutputStream os = new ByteArrayOutputStream()) {
      boolean thumbEnabled = true;
      Thumbnails.Builder<File> thumb = Thumbnails.of(imageFile);
      thumb.resizer(Resizers.PROGRESSIVE);
      thumb.rendering(Rendering.QUALITY);
      thumb.outputQuality(0.85f);

      if (rotate != null) {
        thumb.rotate(rotate);
      }
      if (width != null && height != null && width > 0 && height > 0) {
        thumb.forceSize(width, height);

      } else if (width != null && width > 0) {
        thumb.width(width);
        thumb.keepAspectRatio(true);

      } else if (height != null && height > 0) {
        thumb.height(height);
        thumb.keepAspectRatio(true);

      } else {
        if (rotate == null) {
          in = Files.newInputStream(path); // originální obrázek
          thumbEnabled = false;
        } else {
          // pokud bude rotace, tak se musí jít přes thumb stream
          BufferedImage image = ImageIO.read(imageFile);
          thumb.size(image.getWidth(), image.getHeight());
        }
      }
      // nastane v případě, že proběhl resize
      if (in == null) {
        thumb.toOutputStream(os);
        in = new ByteArrayInputStream(os.toByteArray());
      }
      if (applicationConfig.isStorageEnabled()) {
        Path storagePath = Paths.get(applicationConfig.getStorageDirectory()).toAbsolutePath();
        File storageDir = storagePath.toFile();

        if (!storageDir.exists()) {
          if (storageDir.mkdirs()) {
            LOG.info("Directory created: {}", storageDir);
          }
        }
        String filename = prepareStorageFileName(imageFile, width, height, rotate);
        LOG.info("Saving output image to: {}", filename);

        if (thumbEnabled) {
          thumb.toFile(Paths.get(storagePath.toString(), filename).toString());
          LOG.info("Image resizing is complete: {}", filename);
        } else {
          Utils.copyFile(imageFile, Paths.get(storagePath.toString(), filename).toFile());
        }
      }
      return in;

    } catch (Exception e) {
      LOG.error("There was an error resizing the image on (" + width + "x" + height + "): " + imageFile, e);
      throw new FileFetcherException(e);
    }
  }

  private static String prepareStorageFileName(File imageFile, Integer width, Integer height, Double rotate) {
    String name = imageFile.getName();
    String ext = null;
    String sizePart = null;
    String rotatePart = null;
    int dotIndex = name.lastIndexOf('.');

    if (dotIndex >= 0) {
      name = name.substring(0, dotIndex);
      ext = imageFile.getName().substring(name.length() + 1);
    }

    if (width != null && height != null) {
      sizePart = MessageFormat.format("_[{0}x{1}]", width, height);
    } else if (width != null) {
      sizePart = MessageFormat.format("_[w{0}]", width);
    } else if (height != null) {
      sizePart = MessageFormat.format("_[h{0}]", height);
    }

    if (rotate != null) {
      rotatePart = MessageFormat.format("_[r{0}]", rotate);
    }
    if (sizePart != null) {
      name += sizePart;
    }
    if (rotatePart != null) {
      name += rotatePart;
    }
    name += MessageFormat.format("_[{0}]", String.valueOf(System.currentTimeMillis()));
    if (ext != null) {
      name += "." + ext;
    }
    return name;
  }
}
