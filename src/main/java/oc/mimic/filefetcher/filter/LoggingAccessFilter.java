package oc.mimic.filefetcher.filter;

import oc.mimic.filefetcher.Consts;
import oc.mimic.filefetcher.exception.FileFetcherException;
import oc.mimic.filefetcher.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Filter pro logování přístupu na server.
 *
 * @author mimic
 */
@Component
@Order(Integer.MIN_VALUE)
public class LoggingAccessFilter implements Filter {

  private static final Logger LOG = LoggerFactory.getLogger(LoggingAccessFilter.class);
  private static final List<String> CLASSIC_METHODS = Arrays.asList("GET", "POST");

  public static final String ATTR_ABSOLUTE_URL = "absoluteUrl";

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
          throws IOException, ServletException {

    if (request instanceof HttpServletRequest) {
      HttpServletRequest httpRequest = (HttpServletRequest) request;
      String url = Utils.getAbsoluteUrl(httpRequest);

      request.setAttribute(ATTR_ABSOLUTE_URL, url);

      // nebude logovat favicon, pokud je vypnutá, jinak za loguje vše
      if (!url.endsWith(Consts.FAVICON_NAME)) {
        if (CLASSIC_METHODS.contains(httpRequest.getMethod())) {
          LOG.debug("[{}] [{}] {}", request.getRemoteAddr(), httpRequest.getMethod(), url);
        } else {
          LOG.warn("[{}] [{}] {}", request.getRemoteAddr(), httpRequest.getMethod(), url);
        }
      }
    } else {
      LOG.error("[{}] [{}]", request.getRemoteAddr(), request.getClass().getName());
      LOG.error("Invalid request type. The class instance is: {}", request.getClass().getName());
      throw new FileFetcherException();
    }
    chain.doFilter(request, response);
  }

  @Override
  public void init(FilterConfig filterConfig) {
  }

  @Override
  public void destroy() {
  }
}
