package oc.mimic.filefetcher;

/**
 * Konstanty aplikace.
 *
 * @author mimic
 */
public final class Consts {

  /**
   * Název favicon souboru.
   */
  public static final String FAVICON_NAME = "favicon.ico";
}
