package oc.mimic.filefetcher.controller;

import oc.mimic.filefetcher.component.MimeTypeResolver;
import oc.mimic.filefetcher.config.ApplicationConfig;
import oc.mimic.filefetcher.controller.mapper.ModelMapper;
import oc.mimic.filefetcher.controller.model.category.AssignFilesToCategoryModelForm;
import oc.mimic.filefetcher.controller.model.category.CategoryModel;
import oc.mimic.filefetcher.controller.model.category.CreateCategoryModelForm;
import oc.mimic.filefetcher.controller.model.category.UpdateCategoryModelForm;
import oc.mimic.filefetcher.db.dao.data.CategoryPath;
import oc.mimic.filefetcher.db.entity.Category;
import oc.mimic.filefetcher.db.entity.FetchFile;
import oc.mimic.filefetcher.db.service.AbstractService;
import oc.mimic.filefetcher.db.service.CategoryService;
import oc.mimic.filefetcher.db.service.FetchFileService;
import oc.mimic.filefetcher.db.service.data.CategoryData;
import oc.mimic.filefetcher.db.service.data.CredentialData;
import oc.mimic.filefetcher.exception.FileFetcherException;
import oc.mimic.filefetcher.utils.Json;
import oc.mimic.filefetcher.utils.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * Controller pro správu kategorie.
 *
 * @author mimic
 */
@RestController
public class CategoryController extends AbstractController {

  private static final Logger LOG = LoggerFactory.getLogger(CategoryController.class);
  private static final String CATEGORY = "/cat";

  @Autowired
  private MimeTypeResolver mimeTypeResolver;

  @Autowired
  private CategoryService categoryService;

  @Autowired
  private FetchFileService fetchFileService;

  @Autowired
  private ApplicationConfig applicationConfig;

  /**
   * Hlavní metoda, která slouží pro získání kategorii a jejich informací.
   */
  @GetMapping(CATEGORY)
  public ResponseEntity<CategoryModel> cat( //
          @RequestParam(value = "guid", required = false) String guidParam,
          @RequestParam(value = "token", required = false) String tokenParam) {

    boolean emptyParent = Validator.isNullOrBlank(guidParam);
    if (emptyParent) {
      guidParam = null;
    }
    CredentialData credential = new CredentialData(tokenParam, applicationConfig);
    CategoryModel model = new CategoryModel();
    Category category = null;

    if (Validator.isNullOrBlank(guidParam)) {
      guidParam = null;
    } else {
      category = categoryService.getByGuid(guidParam);
    }
    // GUID nesmí v parametru existovat nebo musí mít uživatel oprávnění (pokud kategorie takové vyžaduje)
    if (guidParam == null || categoryService.isAccessAllowed(guidParam, credential)) {
      List<CategoryData> subcategories = categoryService.getSubcategories(guidParam, credential, true);

      if (!subcategories.isEmpty()) {
        // odebere podkategorie, na které nemá uživatel oprávnění
        if (applicationConfig.isHideCategoriesWithoutPermission()) {
          subcategories.removeIf(categoryData -> !categoryData.isAccessAllowed());
        }
        if (!subcategories.isEmpty()) {
          // vlastní řazení celé kategorie
          if (category != null && category.getSortColumn() != null) {
            Collections.sort(subcategories);
          }
          for (CategoryData categoryData : subcategories) {
            ModelMapper.mapToCategoryModel(model, categoryData);
          }
        }
      }
      // cesta od root po kategorii
      List<CategoryPath> paths = categoryService.getCategoryPaths(guidParam);
      for (CategoryPath path : paths) {
        CategoryModel.InnerCategoryPath inner = new CategoryModel.InnerCategoryPath(path.getName(),
                path.getDescription());
        model.getPaths().put(path.getGuid(), inner);
      }
      // soubory v kategorii
      List<FetchFile> files = fetchFileService.getByCategory(guidParam);
      model.getFiles().addAll(ModelMapper.mapToFetchFilesModel(files, applicationConfig, mimeTypeResolver));

      LOG.debug("{} categories by GUID '{}' loaded.", model.getCategories().size(), guidParam);

    } else if (category == null) {
      LOG.debug("Category '{}' does not exist.", guidParam);
      CategoryPath rootPath = categoryService.getCategoryPaths(null).get(0); // vrátí pouze root
      model.getPaths().put(rootPath.getGuid(), new CategoryModel.InnerCategoryPath(rootPath.getName(), null));
    } else {
      throw new FileFetcherException(
              "Insufficient permissions to category: " + category.getName() + " (" + guidParam + ")");
    }
    return new ResponseEntity<>(model, HttpStatus.OK);
  }

  /**
   * Přiřadí soubory do kategorie.
   */
  @PostMapping(CATEGORY + "/assign-to-category")
  public ResponseEntity<List<AssignFilesToCategoryModelForm>> assignFilesToCategory(
          @RequestBody List<AssignFilesToCategoryModelForm> formList) {

    List<AssignFilesToCategoryModelForm> model = new ArrayList<>();

    for (AssignFilesToCategoryModelForm form : formList) {
      checkValidModelForm(form);

      List<FetchFile> assignFiles = categoryService
              .assignFilesToCategory(form.getFiles(), form.getCategoryGuid(), form.getWithoutCategory());
      AssignFilesToCategoryModelForm newModelForm = ModelMapper
              .mapToAssignFilesToCategoryModelForm(form.getCategoryGuid(), assignFiles, form.getWithoutCategory());
      model.add(newModelForm);

      LOG.info("File assignment to category '{}' is complete.", form.getCategoryGuid());
    }
    return new ResponseEntity<>(model, HttpStatus.OK);
  }

  /**
   * Vytvoří novou kategorii.
   */
  @PostMapping(CATEGORY + "/create")
  public ResponseEntity<List<CreateCategoryModelForm>> create(@RequestBody List<CreateCategoryModelForm> formList) {
    List<CreateCategoryModelForm> modelList = new ArrayList<>();
    List<String> existingCategories = new ArrayList<>();

    // pouze zkontroluje platnost a existenci kategorii
    for (CreateCategoryModelForm form : formList) {
      checkValidModelForm(form);

      Category category = categoryService.getByGuid(form.getGuid());
      if (category != null) {
        existingCategories.add(form.getGuid());
      }
    }
    // vytvoření kategorii, který ještě neexistují
    if (existingCategories.isEmpty()) {
      for (CreateCategoryModelForm form : formList) {
        Category category = categoryService.create(form);
        modelList.add(ModelMapper.mapToCreateCategoryModelForm(category));
        LOG.info("Category '{}' was created with GUID: {}", category.getName(), category.getGuid());
      }
    } else {
      LOG.warn("These categories already exist:\n{}", Json.toPrettyJson(existingCategories));
    }
    return new ResponseEntity<>(modelList, HttpStatus.OK);
  }

  /**
   * Aktualizuje kategorii.
   */
  @PostMapping(CATEGORY + "/update")
  public ResponseEntity<List<UpdateCategoryModelForm>> update(@RequestBody List<UpdateCategoryModelForm> formList) {
    List<UpdateCategoryModelForm> modelList = new ArrayList<>();
    List<String> invalidCategories = new ArrayList<>();

    // pouze zkontroluje platnost a existenci kategorii
    for (UpdateCategoryModelForm form : formList) {
      checkValidModelForm(form);

      Category category = categoryService.getByGuid(form.getGuid());
      if (category == null) {
        invalidCategories.add(AbstractService.removeGuidSlash(form.getGuid()));
      }
    }
    // aktualizace kategorii
    if (invalidCategories.isEmpty()) {
      for (UpdateCategoryModelForm form : formList) {
        Category category = categoryService.update(form);
        checkCategoryExists(category, form.getGuid());

        modelList.add(ModelMapper.mapToUpdateCategoryModelForm(category));
        LOG.info("Category '{}' with GUID '{}' was updated.", category.getName(), category.getGuid());
      }
    } else {
      LOG.warn("These categories do not exist:\n{}", Json.toPrettyJson(invalidCategories));
    }
    return new ResponseEntity<>(modelList, HttpStatus.OK);
  }

  /**
   * Smaže kategorii podle GUID.
   * Pokud kategorie bude obsahovat soubory, tak budou bez kategorie.
   * Pokud nějaký uživatel bude mít oprávnění na kategorii, tak toto oprávnění bude odebráno.
   */
  @PostMapping(CATEGORY + "/delete/{guid}")
  public ResponseEntity<Map<String, Boolean>> delete(@PathVariable("guid") String guid) {
    Category category = categoryService.getByGuid(guid);
    boolean deleted = categoryService.delete(guid);

    if (category != null && deleted) {
      LOG.info("Category with GUID '{}' was deleted.", guid);
    } else {
      LOG.warn("Could not delete category. Category '{}' does not exist.", guid);
    }
    Map<String, Boolean> modelMap = new HashMap<>();
    modelMap.put(guid, deleted);
    return new ResponseEntity<>(modelMap, HttpStatus.OK);
  }

  /**
   * Smaže všechny kategorie.
   * Pokud kategorie bude obsahovat soubory, tak budou bez kategorie.
   * Pokud nějaký uživatel bude mít oprávnění na kategorii, tak toto oprávnění bude odebráno.
   */
  @PostMapping(CATEGORY + "/clean")
  public ResponseEntity<Map<String, String>> clean() {
    List<Category> categories = categoryService.deleteAll();
    Map<String, String> deletedCategories = new LinkedHashMap<>(categories.size());

    for (Category category : categories) {
      deletedCategories.put(category.getGuid(), category.getName());
    }
    LOG.info("Deleted ({}) categories:\n{}", deletedCategories.size(), Json.toPrettyJson(deletedCategories));
    return new ResponseEntity<>(deletedCategories, HttpStatus.OK);
  }

  // === PRIVATE ==============================================================
  // ==========================================================================

  private void checkCategoryExists(Category category, String guid) {
    if (category == null) {
      LOG.warn("The '{}' category does not exist.", guid);
      throw new FileFetcherException();
    }
  }

  private static void checkValidModelForm(AssignFilesToCategoryModelForm modelForm) {
    if ((modelForm.getWithoutCategory() != null && !modelForm.getWithoutCategory()) &&
        (modelForm.getFiles() == null || modelForm.getFiles().isEmpty())) {
      LOG.warn("Category files are required.");
      throw new FileFetcherException();

    } else if (modelForm.getWithoutCategory() != null && modelForm.getWithoutCategory() &&
               modelForm.getCategoryGuid() == null) {
      LOG.warn("Category GUID is required.");
      throw new FileFetcherException();
    }
  }

  private static void checkValidModelForm(CreateCategoryModelForm modelForm) {
    if (Validator.isNullOrBlank(modelForm.getName())) {
      LOG.warn("Category name is required.");
      throw new FileFetcherException();
    }
    if (modelForm.getGuid() != null && modelForm.getGuid().equals(modelForm.getParentGuid())) {
      LOG.warn("The category '{}' contains the same values in the GUID and the parent category.", modelForm.getGuid());
      throw new FileFetcherException();
    }
  }

  private static void checkValidModelForm(UpdateCategoryModelForm modelForm) {
    if (Validator.isNullOrBlank(modelForm.getGuid())) {
      LOG.warn("Category GUID is required.");
      throw new FileFetcherException();
    }
    if (Validator.isNullOrBlank(modelForm.getName())) {
      LOG.warn("Category name is required.");
      throw new FileFetcherException();
    }
    if (modelForm.getGuid().equals(modelForm.getParentGuid())) {
      LOG.warn("The category '{}' contains the same values in the GUID and the parent category.", modelForm.getGuid());
      throw new FileFetcherException();
    }
  }
}
