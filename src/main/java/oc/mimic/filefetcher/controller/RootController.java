package oc.mimic.filefetcher.controller;

import oc.mimic.filefetcher.Application;
import oc.mimic.filefetcher.Consts;
import oc.mimic.filefetcher.controller.model.RootModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Hlavní controller pro root.
 *
 * @author mimic
 */
@RestController
public class RootController extends AbstractController {

  /**
   * Mapování pro root /.
   */
  @GetMapping("/")
  @ResponseBody
  public ResponseEntity<Object> root() {
    RootModel model = new RootModel();
    model.setMessage("The system to fetch files is running properly.");
    model.setVersion(Application.VERSION);
    return new ResponseEntity<>(model, HttpStatus.OK);
  }

  /**
   * Odebere favicon z aplikace.
   */
  @GetMapping(Consts.FAVICON_NAME)
  @ResponseBody
  public void returnNoFavicon() {
  }
}
