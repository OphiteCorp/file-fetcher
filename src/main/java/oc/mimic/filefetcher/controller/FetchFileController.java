package oc.mimic.filefetcher.controller;

import oc.mimic.filefetcher.component.ImageProcessor;
import oc.mimic.filefetcher.component.MimeTypeResolver;
import oc.mimic.filefetcher.config.ApplicationConfig;
import oc.mimic.filefetcher.controller.mapper.ModelMapper;
import oc.mimic.filefetcher.controller.model.file.CreateFetchFileModelForm;
import oc.mimic.filefetcher.controller.model.file.FetchFileAccessModel;
import oc.mimic.filefetcher.controller.model.file.FetchFileModel;
import oc.mimic.filefetcher.controller.model.file.UpdateFetchFileModelForm;
import oc.mimic.filefetcher.db.entity.FetchFile;
import oc.mimic.filefetcher.db.entity.enums.FileExtraType;
import oc.mimic.filefetcher.db.service.AbstractService;
import oc.mimic.filefetcher.db.service.FetchFileService;
import oc.mimic.filefetcher.db.service.data.CredentialData;
import oc.mimic.filefetcher.db.service.data.VideoExtraData;
import oc.mimic.filefetcher.exception.FileFetcherException;
import oc.mimic.filefetcher.utils.Json;
import oc.mimic.filefetcher.utils.Utils;
import oc.mimic.filefetcher.utils.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.*;

/**
 * Controller pro správu souborů pro fetch.
 *
 * @author mimic
 */
@RestController
public class FetchFileController extends AbstractController {

  private static final Logger LOG = LoggerFactory.getLogger(FetchFileController.class);
  public static final String FILE = "/file";

  private static final String HEADER_FILE_CHECKSUM = "File-Checksum";
  private static final String HEADER_FILE_EXTENSION = "File-Extension";

  @Autowired
  private MimeTypeResolver mimeTypeResolver;

  @Autowired
  private FetchFileService fetchFileService;

  @Autowired
  private ImageProcessor imageProcessor;

  @Autowired
  private ApplicationConfig applicationConfig;

  /**
   * Kontrola oprávnění a přístup na soubor.
   */
  @GetMapping(FILE + "/fetch/{guid}/access")
  public ResponseEntity<FetchFileAccessModel> checkAuthorization( //
          @PathVariable("guid") String guidParam, //
          @RequestParam(value = "token", required = false) String tokenParam) {

    FetchFile ff = fetchFileService.getByGuid(guidParam);
    boolean available = false;

    try {
      checkFetchFileGuidExists(ff, guidParam);
      checkFileCredential(ff.getGuid(), tokenParam);
      available = true;

    } catch (Exception e) {
      if (ff != null) {
        LOG.debug("User does not have access to the file: {}", ff.getSource());
      } else {
        LOG.debug("The GUID file does not exist: {}", guidParam);
      }
    }
    FetchFileAccessModel model = new FetchFileAccessModel();
    model.setGuid(guidParam);
    model.setAvailable(available);
    return new ResponseEntity<>(model, HttpStatus.OK);
  }

  /**
   * Získá informace o souboru.
   */
  @GetMapping(FILE + "/fetch/{guid}/info")
  public ResponseEntity<FetchFileModel> fileInfo( //
          @PathVariable("guid") String guidParam, //
          @RequestParam(value = "token", required = false) String tokenParam) {

    FetchFile ff = fetchFileService.getByGuid(guidParam);
    checkFetchFileGuidExists(ff, guidParam);
    checkFileCredential(ff.getGuid(), tokenParam);

    LOG.debug("File information with GUID '{}' has been loaded.", guidParam);
    FetchFileModel model = ModelMapper.mapToFetchFileModel(ff, applicationConfig, mimeTypeResolver);
    return new ResponseEntity<>(model, HttpStatus.OK);
  }

  /**
   * Stáhne soubor.
   */
  @GetMapping(FILE + "/fetch/{guid}/download")
  public ResponseEntity<InputStreamResource> fileDownload( //
          @PathVariable("guid") String guidParam, //
          @RequestParam(value = "token", required = false) String tokenParam, //
          HttpServletResponse httpResponse) {

    FetchFile ff = fetchFileService.getByGuid(guidParam);
    checkFetchFileGuidExists(ff, guidParam);
    checkFileCredential(ff.getGuid(), tokenParam);

    File file = checkFileExists(ff.getSource());
    return openOrDownloadFile(httpResponse, file, ff.getGuid(), true);
  }

  /**
   * Otevře soubor.
   */
  @GetMapping(FILE + "/fetch/{guid}/open")
  public ResponseEntity<InputStreamResource> fileOpen( //
          @PathVariable("guid") String guidParam, //
          @RequestParam(value = "token", required = false) String tokenParam, //
          HttpServletResponse httpResponse) {

    FetchFile ff = fetchFileService.getByGuid(guidParam);
    checkFetchFileGuidExists(ff, guidParam);
    checkFileCredential(ff.getGuid(), tokenParam);

    File file = checkFileExists(ff.getSource());
    return openOrDownloadFile(httpResponse, file, ff.getGuid(), false);
  }

  /**
   * Získá soubor obrázku.
   */
  @GetMapping(FILE + "/fetch/{guid}/image")
  public ResponseEntity<InputStreamResource> fileImage( //
          @PathVariable("guid") String guidParam, //
          @RequestParam(value = "width", required = false) String widthParam,
          @RequestParam(value = "height", required = false) String heightParam,
          @RequestParam(value = "rotate", required = false) String rotateParam,
          @RequestParam(value = "token", required = false) String tokenParam, //
          HttpServletResponse httpResponse) {

    FetchFile ff = fetchFileService.getByGuid(guidParam);
    checkFetchFileGuidExists(ff, guidParam);
    checkFileCredential(ff.getGuid(), tokenParam);

    checkFileExists(ff.getSource());
    return getFileImageInputStream(widthParam, heightParam, rotateParam, httpResponse, ff);
  }

  /**
   * Získá informace o veřejných náhodných souborech.
   */
  @GetMapping(FILE + "/random/info")
  public ResponseEntity<List<FetchFileModel>> randomFilesInfo( //
          @RequestParam(value = "category", required = false) String categoryGuidParam,
          @RequestParam(value = "count", required = false) String countParam) {

    if (Validator.isNullOrBlank(categoryGuidParam)) {
      categoryGuidParam = null;
    }
    int count = parseCount(Validator.isNullOrBlank(countParam) ? "1" : countParam);
    List<FetchFile> files = fetchFileService.getRandomPublicFiles(categoryGuidParam, count);

    for (FetchFile file : files) {
      LOG.debug("File information with GUID '{}' has been loaded.", file.getGuid());
    }
    List<FetchFileModel> model = ModelMapper.mapToFetchFilesModel(files, applicationConfig, mimeTypeResolver);
    return new ResponseEntity<>(model, HttpStatus.OK);
  }

  /**
   * Získá veřejný náhodný soubor obrázku.
   */
  @GetMapping(FILE + "/random/image")
  public ResponseEntity<InputStreamResource> randomFileImage( //
          @RequestParam(value = "category", required = false) String categoryGuidParam,
          @RequestParam(value = "width", required = false) String widthParam,
          @RequestParam(value = "height", required = false) String heightParam,
          @RequestParam(value = "rotate", required = false) String rotateParam, //
          HttpServletResponse httpResponse) {

    if (Validator.isNullOrBlank(categoryGuidParam)) {
      categoryGuidParam = null;
    }
    List<FetchFile> files = fetchFileService.getRandomPublicFiles(categoryGuidParam, 1);
    FetchFile ff = files.isEmpty() ? null : files.get(0);

    checkFetchFileGuidExists(ff, null);
    checkFileExists(Objects.requireNonNull(ff).getSource());
    return getFileImageInputStream(widthParam, heightParam, rotateParam, httpResponse, ff);
  }

  /**
   * Vytvoří nový soubor.
   */
  @PostMapping(FILE + "/create")
  public ResponseEntity<List<CreateFetchFileModelForm>> create(@RequestBody List<CreateFetchFileModelForm> formList) {
    List<CreateFetchFileModelForm> modelList = new ArrayList<>();
    List<String> existingFiles = new ArrayList<>();

    // pouze zkontroluje platnost a existenci souborů
    for (CreateFetchFileModelForm form : formList) {
      checkValidModelForm(form);

      FetchFile ff = fetchFileService.getByGuid(form.getGuid());
      if (ff != null) {
        existingFiles.add(form.getGuid());
      }
    }
    // vytvoření souborů, který ještě neexistují
    if (existingFiles.isEmpty()) {
      for (CreateFetchFileModelForm form : formList) {
        FetchFile ff = fetchFileService.create(form);
        modelList.add(ModelMapper.mapToCreateFetchFileModelForm(ff));
        LOG.info("File '{}' was created with source: {}", ff.getGuid(), ff.getSource());
      }
    } else {
      LOG.warn("These files already exist:\n{}", Json.toPrettyJson(existingFiles));
    }
    return new ResponseEntity<>(modelList, HttpStatus.OK);
  }

  /**
   * Aktualizuje soubor.
   */
  @PostMapping(FILE + "/update")
  public ResponseEntity<List<UpdateFetchFileModelForm>> update(@RequestBody List<UpdateFetchFileModelForm> formList) {
    List<UpdateFetchFileModelForm> modelList = new ArrayList<>();
    List<String> invalidFiles = new ArrayList<>();

    // pouze zkontroluje platnost a existenci souborů
    for (UpdateFetchFileModelForm form : formList) {
      checkValidModelForm(form);

      FetchFile ff = fetchFileService.getByGuid(form.getGuid());
      if (ff == null) {
        invalidFiles.add(AbstractService.removeGuidSlash(form.getGuid()));
      }
    }
    // aktualizace souborů
    if (invalidFiles.isEmpty()) {
      for (UpdateFetchFileModelForm form : formList) {
        FetchFile ff = fetchFileService.update(form);
        modelList.add(ModelMapper.mapToUpdateFetchFileModelForm(ff));
        LOG.info("File '{}' with source '{}' was updated.", ff.getGuid(), ff.getSource());
      }
    } else {
      LOG.warn("These files do not exist:\n{}", Json.toPrettyJson(invalidFiles));
    }
    return new ResponseEntity<>(modelList, HttpStatus.OK);
  }

  /**
   * Smaže soubor.
   */
  @PostMapping(FILE + "/delete/{guid}")
  public ResponseEntity<Map<String, Boolean>> delete(@PathVariable("guid") String guid) {
    FetchFile ff = fetchFileService.getByGuid(guid);
    boolean deleted = fetchFileService.delete(guid);

    if (ff != null && deleted) {
      LOG.info("File with GUID '{}' was deleted.", guid);
    } else {
      LOG.warn("Could not delete file. File '{}' does not exist.", guid);
    }
    Map<String, Boolean> modelMap = new HashMap<>();
    modelMap.put(guid, deleted);
    return new ResponseEntity<>(modelMap, HttpStatus.OK);
  }

  /**
   * Smaže všechny soubory.
   */
  @PostMapping(FILE + "/clean")
  public ResponseEntity<Map<String, String>> clean() {
    List<FetchFile> files = fetchFileService.deleteAll();
    Map<String, String> deletedFiles = new LinkedHashMap<>(files.size());

    for (FetchFile ff : files) {
      deletedFiles.put(ff.getGuid(), ff.getSource());
    }
    LOG.info("Deleted ({}) files:\n{}", deletedFiles.size(), Json.toPrettyJson(deletedFiles));
    return new ResponseEntity<>(deletedFiles, HttpStatus.OK);
  }

  // === PRIVATE ==============================================================
  // ==========================================================================

  private ResponseEntity<InputStreamResource> getFileImageInputStream(String widthParam, String heightParam,
          String rotateParam, HttpServletResponse httpResponse, FetchFile ff) {

    FileExtraType extraType = ff.getExtraType();
    File file = new File(ff.getSource());

    // videa maji ulozeny nahled ve specialnim adresari
    if (extraType == FileExtraType.VIDEO) {
      file = VideoExtraData.getPreviewPath(applicationConfig, ff.getGuid()).toFile();
    }
    checkFileExists(file.getAbsolutePath());

    Integer width = parseWidthParam(widthParam);
    Integer height = parseHeightParam(heightParam);
    Double rotate = parseRotateParam(rotateParam);
    return processImage(httpResponse, file, ff, width, height, rotate);
  }

  private static void checkFetchFileGuidExists(FetchFile ff, String guid) {
    if (ff == null) {
      if (guid != null) {
        LOG.warn("The '{}' file does not exist.", guid);
      } else {
        LOG.warn("The file does not exist.");
      }
      throw new FileFetcherException();
    }
  }

  private static File checkFileExists(String fileSource) {
    File file = new File(fileSource);

    if (!Validator.isFileExists(file)) {
      LOG.warn("The file does not exist or has been deleted: {}", file);
      throw new FileFetcherException();
    }
    return file;
  }

  private static void checkValidModelForm(CreateFetchFileModelForm modelForm) {
    if (Validator.isNullOrBlank(modelForm.getSource())) {
      LOG.warn("Source is required.");
      throw new FileFetcherException();
    }
  }

  private static void checkValidModelForm(UpdateFetchFileModelForm modelForm) {
    if (Validator.isNullOrBlank(modelForm.getSource())) {
      LOG.warn("Source is required.");
      throw new FileFetcherException();
    }
    if (Validator.isNullOrBlank(modelForm.getGuid())) {
      LOG.warn("Fetch file GUID is required.");
      throw new FileFetcherException();
    }
  }

  private void checkFileCredential(String guid, String token) {
    CredentialData credential = new CredentialData(token, applicationConfig);
    if (!fetchFileService.isAccessAllowed(guid, credential)) {
      throw new FileFetcherException("Insufficient permissions to file: " + guid);
    }
  }

  private Integer parseWidthParam(String widthParam) {
    Integer width = null;

    if (widthParam != null) {
      try {
        width = Integer.parseInt(widthParam);
        if (width <= 0 || width > applicationConfig.getMaxResizeWidth()) {
          LOG.warn("The input width '{}' is too large. The maximum is: {}.", widthParam,
                  applicationConfig.getMaxResizeWidth());
          throw new FileFetcherException();
        }
      } catch (NumberFormatException e) {
        LOG.warn("The input width '{}' is not valid.", widthParam);
        throw new FileFetcherException(e);
      }
    }
    return width;
  }

  private Integer parseHeightParam(String heightParam) {
    Integer height = null;

    if (heightParam != null) {
      try {
        height = Integer.parseInt(heightParam);
        if (height <= 0 || height > applicationConfig.getMaxResizeHeight()) {
          LOG.warn("The input height '{}' is too large. The maximum is: {}.", heightParam,
                  applicationConfig.getMaxResizeHeight());
          throw new FileFetcherException();
        }
      } catch (NumberFormatException e) {
        LOG.warn("The input height '{}' is not valid.", heightParam);
        throw new FileFetcherException(e);
      }
    }
    return height;
  }

  private Double parseRotateParam(String rotateParam) {
    Double rotate = null;

    if (rotateParam != null) {
      try {
        rotate = Double.parseDouble(rotateParam);
      } catch (NumberFormatException e) {
        LOG.warn("The input rotate '{}' is not valid.", rotateParam);
        throw new FileFetcherException(e);
      }
    }
    return rotate;
  }

  private static Integer parseCount(String countParam) {
    Integer count = null;

    if (countParam != null) {
      try {
        count = Integer.parseInt(countParam);

      } catch (NumberFormatException e) {
        LOG.warn("The input count '{}' is not valid.", countParam);
        throw new FileFetcherException(e);
      }
    }
    return count;
  }

  private ResponseEntity<InputStreamResource> processImage(HttpServletResponse httpResponse, File file, FetchFile ff,
          Integer width, Integer height, Double rotate) {

    // soubory typu GIF nebudeme měnit
    String fileExtension = Utils.getFileExtension(file.getName());
    if (fileExtension.equals("gif")) {
      width = null;
      height = null;
      rotate = null;
    }
    // resize, případně získání streamu na soubor
    InputStream in = imageProcessor.resize(file, width, height, rotate);
    try {
      int available = in.available();
      String mimeType = mimeTypeResolver.resolveMimeType(file.toPath());
      httpResponse.setContentType(mimeType);
      httpResponse.setHeader(HEADER_FILE_CHECKSUM, String.valueOf(ff.getChecksum()));
      httpResponse.setHeader(HEADER_FILE_EXTENSION, Utils.getFileExtension(file.getName()));

      return ResponseEntity.ok() //
              .contentType(MediaType.parseMediaType(mimeType)) //
              .contentLength(available) //
              .body(new InputStreamResource(in));

    } catch (Exception e) {
      LOG.error("Unable to get mineType of file: " + file, e);
      throw new FileFetcherException(e);
    }
  }

  private ResponseEntity<InputStreamResource> openOrDownloadFile(HttpServletResponse httpResponse, File file,
          String guid, boolean download) {

    try {
      Charset charset = StandardCharsets.UTF_8;
      String fileExtension = Utils.getFileExtension(file.getName());
      String filename = URLEncoder.encode(guid + "." + fileExtension, charset);
      String mimeType = mimeTypeResolver.resolveMimeType(file.toPath());
      httpResponse.setCharacterEncoding(charset.displayName());

      if (download) {
        httpResponse.setHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + filename);
      }
      return ResponseEntity.ok() //
              .contentType(MediaType.valueOf(mimeType)) //
              .contentLength(file.length()) //
              .body(new InputStreamResource(new FileInputStream(file)));

    } catch (Exception e) {
      LOG.error("An error occurred while downloading the file: " + file, e);
      throw new FileFetcherException(e);
    }
  }
}
