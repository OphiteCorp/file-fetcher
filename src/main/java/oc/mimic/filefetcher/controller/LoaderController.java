package oc.mimic.filefetcher.controller;

import com.fasterxml.jackson.annotation.JsonProperty;
import oc.mimic.filefetcher.controller.mapper.ModelMapper;
import oc.mimic.filefetcher.controller.model.category.CreateCategoryModelForm;
import oc.mimic.filefetcher.controller.model.category.UpdateCategoryModelForm;
import oc.mimic.filefetcher.controller.model.file.CreateFetchFileModelForm;
import oc.mimic.filefetcher.controller.model.loader.*;
import oc.mimic.filefetcher.controller.model.user.ChangeCategoriesModelForm;
import oc.mimic.filefetcher.controller.model.user.CreateUserModelForm;
import oc.mimic.filefetcher.db.entity.Category;
import oc.mimic.filefetcher.db.entity.FetchFile;
import oc.mimic.filefetcher.db.entity.User;
import oc.mimic.filefetcher.db.service.CategoryService;
import oc.mimic.filefetcher.db.service.FetchFileService;
import oc.mimic.filefetcher.db.service.UserService;
import oc.mimic.filefetcher.db.service.data.CategoryData;
import oc.mimic.filefetcher.exception.FileFetcherException;
import oc.mimic.filefetcher.utils.Json;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.lang.reflect.Field;
import java.util.*;

/**
 * Controller pro nahrání velkého množství dat.
 *
 * @author mimic
 */
@RestController
public class LoaderController extends AbstractController {

  private static final Logger LOG = LoggerFactory.getLogger(LoaderController.class);
  private static final String LOADER = "/loader";

  @Autowired
  private CategoryService categoryService;

  @Autowired
  private FetchFileService fetchFileService;

  @Autowired
  private UserService userService;

  /**
   * Importuje data.
   */
  @PostMapping(LOADER + "/import")
  public ResponseEntity<LoaderForm> importData(@RequestBody LoaderForm form) {
    LOG.info("Import started.");
    LoaderForm updatedForm;

    try {
      // použije vlastní proměnné na všechny řetězce, vstupní formulář nezmění
      updatedForm = VariablesHelper.apply(form);
    } catch (Exception e) {
      LOG.error("An error occurred while applying variables to the loader.", e);
      throw new FileFetcherException(e);
    }
    // získá vlastní nastavení pro import
    boolean regenerateGuids = getBooleanConfig(form, LoaderForm.CONF_REGENERATE_GUIDS);
    boolean regenerateOrdersCategory = getBooleanConfig(form, LoaderForm.CONF_REGENERATE_ORDERS_CATEGORY);
    boolean regenerateOrdersFile = getBooleanConfig(form, LoaderForm.CONF_REGENERATE_ORDERS_FILE);

    // drží původní data z formuláře podle ID - změny nad těmito daty se promítnou i v response
    Map<String, ILoaderFormWrapper> wrappedMap = convertToMap(form);
    Map<String, PreviewFileCategoryData> categoriesWithPreviewFileMap = new HashMap<>();
    int categoryOrder = categoryService.getMaxOrder() + 1;
    int filesOrder = fetchFileService.getMaxOrder() + 1;

    // projde uživatele a založí je
    for (LoaderUserForm userForm : updatedForm.getUsers()) {
      User user = userService.create(userForm);
      ((LoaderUserForm) wrappedMap.get(userForm.getId())).setPassword(user.getPassword());
      LOG.info("User created: {}", userForm.getLogin());
    }
    // projde data na formuláři - nutné pracovat s kopii!
    for (LoaderCategoryForm categoryForm : updatedForm.getData()) {
      Stack<LoaderCategoryForm> stack = new Stack<>();
      stack.push(categoryForm);

      // pouze temp, pro budoucí přiřazení uživatelů do kategorii
      Map<String, Set<String>> categoryUsersTempMap = new LinkedHashMap<>();

      while (!stack.isEmpty()) {
        LoaderCategoryForm popCategoryForm = stack.pop();
        LoaderCategoryForm popCategoryFormWrapper = (LoaderCategoryForm) wrappedMap.get(popCategoryForm.getId());

        // pokud bude True, tak přegeneruje všechny pořadí kategorii dle importu
        if (regenerateOrdersCategory) {
          popCategoryForm.setOrder(categoryOrder);
          popCategoryFormWrapper.setOrder(categoryOrder++);
        }
        // pokud bude True, tak přegeneruje všechny GUID i když budou vyplněný
        if (regenerateGuids) {
          popCategoryForm.setGuid(null);
        }
        // vytvoří novou kategorii
        Category category = categoryService.create(popCategoryForm);
        popCategoryForm.setGuid(category.getGuid());
        popCategoryFormWrapper.setGuid(category.getGuid());
        popCategoryFormWrapper.setParentGuid(null); // není nutné zobrazit rodiče na výstupu

        // uloží kategorie, kterým je potřeba později přiřadit vlastní náhled
        if (popCategoryForm.getPreviewFile() != null) {
          categoriesWithPreviewFileMap.put(popCategoryForm.getId(),
                  new PreviewFileCategoryData(category, popCategoryForm.getPreviewFile()));
        }
        popCategoryFormWrapper.setPreviewFile(null); // nutné nastavit null, protože nevíme, zda hodnota existuje
        LOG.info("Category was created: {} ({})", category.getGuid(), category.getName());

        // vyhodnocuje uživatele v každé kategorii pro pozdější přiřazení
        for (String user : popCategoryForm.getUsers()) {
          // uživatel musí existovat v definici (být vytvořen)
          if (form.getUsers().stream().anyMatch(p -> user.equals(p.getLogin()))) {
            Set<String> set = categoryUsersTempMap.computeIfAbsent(user, k -> new LinkedHashSet<>());
            set.add(category.getGuid());
          } else {
            // koukne se po uživateli do DB
            User userFromDatabase = userService.getByLogin(user);
            if (userFromDatabase != null) {
              Set<String> set = categoryUsersTempMap.computeIfAbsent(user, k -> new LinkedHashSet<>());
              set.add(category.getGuid());
            } else {
              LOG.error("The '{}' user on the category '{}' ({}) was not defined among the users.", user,
                      category.getGuid(), category.getName());
              throw new FileFetcherException("Missing user '" + user + "' definition.");
            }
          }
        }
        // import souborů do kategorie
        for (LoaderFetchFileForm fileForm : popCategoryForm.getFiles()) {
          LoaderFetchFileForm fileFormWrapper = (LoaderFetchFileForm) wrappedMap.get(fileForm.getId());

          fileForm.setCategory(category.getGuid());
          fileFormWrapper.setCategory(category.getGuid());

          // pokud bude True, tak přegeneruje všechny pořadí souborů dle importu
          if (regenerateOrdersFile) {
            fileForm.setOrder(filesOrder);
            fileFormWrapper.setOrder(filesOrder++);
          }
          // pokud bude True, tak přegeneruje všechny GUID i když budou vyplněný
          if (regenerateGuids) {
            fileForm.setGuid(null);
          }
          createFile(fileForm, fileFormWrapper);
        }
        // import podkategorie
        for (LoaderCategoryForm subCategoryForm : popCategoryForm.getSubcategories()) {
          subCategoryForm.setParentGuid(category.getGuid());
          ((LoaderCategoryForm) wrappedMap.get(subCategoryForm.getId())).setParentGuid(category.getGuid());
          stack.push(subCategoryForm);
        }
      }
      // přiřadí uživatele do kategorii
      for (Map.Entry<String, Set<String>> entry : categoryUsersTempMap.entrySet()) {
        ChangeCategoriesModelForm changeForm = new ChangeCategoriesModelForm();
        changeForm.setLogin(entry.getKey());
        changeForm.setCategories(new ArrayList<>(entry.getValue()));
        changeForm.setAppend(true);
        userService.assignCategoriesToUser(changeForm);
      }
    }
    // projde soubory bez kategorie
    for (LoaderFetchFileForm fileForm : updatedForm.getFilesWithoutCategory()) {
      LoaderFetchFileForm fileFormWrapper = (LoaderFetchFileForm) wrappedMap.get(fileForm.getId());

      // pokud bude True, tak přegeneruje všechny pořadí souborů dle importu
      if (regenerateOrdersFile) {
        fileForm.setOrder(filesOrder);
        fileFormWrapper.setOrder(filesOrder++);
      }
      // pokud bude True, tak přegeneruje všechny GUID i když budou vyplněný
      if (regenerateGuids) {
        fileForm.setGuid(null);
      }
      createFile(fileForm, fileFormWrapper);
    }
    // dodatečně aktualizuje kategorie
    for (Map.Entry<String, PreviewFileCategoryData> entry : categoriesWithPreviewFileMap.entrySet()) {
      LoaderCategoryForm wrapper = (LoaderCategoryForm) wrappedMap.get(entry.getKey());
      UpdateCategoryModelForm categoryUpdate = ModelMapper.mapToUpdateCategoryModelForm(entry.getValue().getCategory());
      categoryUpdate.setPreviewFile(entry.getValue().getPreviewFileGuid());
      Category updatedCategory = categoryService.update(categoryUpdate);

      if (updatedCategory.getPreviewFile() != null) {
        wrapper.setPreviewFile(updatedCategory.getPreviewFile().getGuid());
      }
    }
    LOG.info("Import completed successfully.");
    return new ResponseEntity<>(form, HttpStatus.OK);
  }

  /**
   * Exportuje data.
   */
  @PostMapping(LOADER + "/export")
  public ResponseEntity<LoaderForm> exportData(@RequestBody LoaderExportForm exportForm) {
    // získá vlastní nastavení pro export
    boolean removeGuids = getBooleanConfig(exportForm, LoaderExportForm.CONF_REMOVE_GUIDS);
    boolean removeOrdersCategory = getBooleanConfig(exportForm, LoaderExportForm.CONF_REMOVE_ORDERS_CATEGORY);
    boolean removeOrdersFile = getBooleanConfig(exportForm, LoaderExportForm.CONF_REMOVE_ORDERS_FILE);
    boolean removeUserAccess = getBooleanConfig(exportForm, LoaderExportForm.CONF_REMOVE_USER_ACCESS);

    LoaderForm form = new LoaderForm();
    form.getConfig().put(LoaderForm.CONF_REGENERATE_GUIDS, false);
    form.getConfig().put(LoaderForm.CONF_REGENERATE_ORDERS_CATEGORY, removeOrdersCategory);
    form.getConfig().put(LoaderForm.CONF_REGENERATE_ORDERS_FILE, removeOrdersFile);

    // projde uživatele
    List<User> users = userService.getAll();
    for (User user : users) {
      form.getUsers().add(mapToLoaderUserForm(user));
    }
    // projde hlavní kategorie
    List<CategoryData> rootCategories = categoryService.getSubcategories(null, null, false);

    for (CategoryData data : rootCategories) {
      LoaderCategoryForm categoryForm = mapToLoaderCategoryForm(data.getCategory(), removeGuids, removeOrdersCategory,
              removeUserAccess, data.getCategory().getUsers());
      form.getData().add(categoryForm);

      Stack<LocalStackCategory> stack = new Stack<>();
      stack.push(new LocalStackCategory(categoryForm, data));

      // projde jednotlivé hlavní kategorie a jejich podkategorie
      while (!stack.isEmpty()) {
        LocalStackCategory popLocalCategory = stack.pop();
        Category category = popLocalCategory.data.getCategory();
        List<CategoryData> subcategories = categoryService.getSubcategories(category.getGuid(), null, false);

        // projde soubory v kategorii
        List<FetchFile> files = fetchFileService.getByCategory(category.getGuid());
        for (FetchFile ff : files) {
          LoaderFetchFileForm fileForm = mapToLoaderFetchFileForm(ff, removeGuids, removeOrdersFile);
          popLocalCategory.parent.getFiles().add(fileForm);
        }
        // projde podkategorie
        for (CategoryData subcategory : subcategories) {
          LoaderCategoryForm subcategoryForm = mapToLoaderCategoryForm(subcategory.getCategory(), removeGuids,
                  removeOrdersCategory, removeUserAccess, subcategory.getCategory().getUsers());
          popLocalCategory.parent.getSubcategories().add(subcategoryForm);
          stack.push(new LocalStackCategory(subcategoryForm, subcategory));
        }
      }
    }
    // soubory bez kategorie
    List<FetchFile> rootFiles = fetchFileService.getByCategory(null);

    for (FetchFile ff : rootFiles) {
      LoaderFetchFileForm fileForm = mapToLoaderFetchFileForm(ff, removeGuids, removeOrdersFile);
      form.getFilesWithoutCategory().add(fileForm);
    }
    LOG.debug("The export was successful.");
    return new ResponseEntity<>(form, HttpStatus.OK);
  }

  // === PRIVATE ==============================================================
  // ==========================================================================

  private void createFile(LoaderFetchFileForm fileForm, LoaderFetchFileForm fileFormWrapper) {
    FetchFile ff = fetchFileService.create(fileForm);
    fileForm.setGuid(ff.getGuid());
    fileFormWrapper.setGuid(ff.getGuid());
    fileFormWrapper.setCategory(null); // není nutné zobrazit GUID kategorii na výstupu
    LOG.info("The file has been created: {} ({})", ff.getGuid(), ff.getSource());
  }

  private static LoaderUserForm mapToLoaderUserForm(User user) {
    LoaderUserForm userForm = new LoaderUserForm();
    userForm.setLogin(user.getLogin());
    userForm.setPassword(user.getPassword());
    return userForm;
  }

  private static LoaderCategoryForm mapToLoaderCategoryForm(Category category, boolean removeGuids,
          boolean removeOrders, boolean removeUserAccess, Set<String> users) {

    LoaderCategoryForm categoryForm = new LoaderCategoryForm();
    categoryForm.setName(category.getName());
    categoryForm.setDescription(category.getDescription());
    categoryForm.setSortColumn((category.getSortColumn() != null) ? category.getSortColumn().getColumnName() : null);

    if (!removeUserAccess && users != null && !users.isEmpty()) {
      categoryForm.getUsers().addAll(users);
    }
    if (!removeGuids) {
      categoryForm.setGuid(category.getGuid());
      categoryForm.setPreviewFile((category.getPreviewFile() != null) ? category.getPreviewFile().getGuid() : null);
    }
    if (!removeOrders) {
      categoryForm.setOrder(category.getOrder());
    }
    return categoryForm;
  }

  private static LoaderFetchFileForm mapToLoaderFetchFileForm(FetchFile ff, boolean removeGuids, boolean removeOrders) {
    LoaderFetchFileForm fileForm = new LoaderFetchFileForm();
    fileForm.setName(ff.getName());
    fileForm.setDescription(ff.getDescription());
    fileForm.setAuthor(ff.getAuthor());
    fileForm.setSource(ff.getSource());
    fileForm.setGrade((ff.getGrade() != null) ? ff.getGrade().getCode() : null);
    fileForm.setHidden(ff.isHidden());

    if (!removeGuids) {
      fileForm.setGuid(ff.getGuid());
    }
    if (!removeOrders) {
      fileForm.setOrder(ff.getOrder());
    }
    return fileForm;
  }

  private static Map<String, ILoaderFormWrapper> convertToMap(LoaderForm form) {
    Map<String, ILoaderFormWrapper> map = new HashMap<>();

    for (LoaderUserForm userForm : form.getUsers()) {
      map.put(userForm.getId(), userForm);
    }
    for (LoaderCategoryForm categoryForm : form.getData()) {
      Stack<LoaderCategoryForm> stack = new Stack<>();
      stack.push(categoryForm);

      while (!stack.isEmpty()) {
        LoaderCategoryForm popCategoryForm = stack.pop();
        map.put(popCategoryForm.getId(), popCategoryForm);

        for (LoaderFetchFileForm fileForm : popCategoryForm.getFiles()) {
          map.put(fileForm.getId(), fileForm);
        }
        for (LoaderCategoryForm subCategoryForm : popCategoryForm.getSubcategories()) {
          stack.push(subCategoryForm);
        }
      }
    }
    for (LoaderFetchFileForm fileForm : form.getFilesWithoutCategory()) {
      map.put(fileForm.getId(), fileForm);
    }
    return map;
  }

  private static boolean getBooleanConfig(LoaderForm form, String key) {
    Object value = form.getConfig().get(key);
    return (value != null && (boolean) value);
  }

  private static boolean getBooleanConfig(LoaderExportForm form, String key) {
    Object value = form.getConfig().get(key);
    return (value != null && (boolean) value);
  }

  /**
   * Pomocná třída pro export.
   */
  private static final class LocalStackCategory {

    private final LoaderCategoryForm parent;
    private final CategoryData data;

    private LocalStackCategory(LoaderCategoryForm parent, CategoryData data) {
      this.parent = parent;
      this.data = data;
    }
  }

  /**
   * Pomocná třída pro aplikování proměnných na fieldy.
   */
  private static final class VariablesHelper {

    public static LoaderForm apply(LoaderForm form) throws Exception {
      LoaderForm formClone = Json.clone(form);
      Map<String, Object> vars = formClone.getVariables();
      List<LoaderUserForm> users = formClone.getUsers();
      List<LoaderCategoryForm> data = formClone.getData();

      if (vars.isEmpty()) {
        return formClone;
      }
      for (LoaderUserForm userForm : users) {
        applyToUser(vars, userForm);
      }
      for (LoaderCategoryForm categoryForm : data) {
        Stack<LoaderCategoryForm> stack = new Stack<>();
        stack.push(categoryForm);

        while (!stack.isEmpty()) {
          LoaderCategoryForm popCategoryForm = stack.pop();
          applyToCategory(vars, popCategoryForm);
          applyToFiles(vars, popCategoryForm.getFiles());

          for (LoaderCategoryForm subCategoryForm : popCategoryForm.getSubcategories()) {
            stack.push(subCategoryForm);
          }
        }
      }
      return formClone;
    }

    private static void applyToFiles(Map<String, Object> vars, List<LoaderFetchFileForm> files)
            throws IllegalAccessException {

      for (LoaderFetchFileForm file : files) {
        Field[] fields = CreateFetchFileModelForm.class.getDeclaredFields(); // musí být třída, která se dědí
        applyToFields(vars, fields, file);
      }
    }

    private static void applyToCategory(Map<String, Object> vars, CreateCategoryModelForm category)
            throws IllegalAccessException {

      Field[] fields = CreateCategoryModelForm.class.getDeclaredFields();
      applyToFields(vars, fields, category);
    }

    private static void applyToUser(Map<String, Object> vars, CreateUserModelForm user) throws IllegalAccessException {
      Field[] fields = CreateUserModelForm.class.getDeclaredFields();
      applyToFields(vars, fields, user);
    }

    private static void applyToFields(Map<String, Object> vars, Field[] fields, Object instance)
            throws IllegalAccessException {

      for (Field field : fields) {
        if (field.isAnnotationPresent(JsonProperty.class)) {
          field.setAccessible(true);
          Object value = field.get(instance);

          if (value instanceof String) {
            String strValue = (String) value;

            for (String key : vars.keySet()) {
              if (strValue.contains(key)) {
                strValue = strValue.replace(key, vars.get(key).toString());
                field.set(instance, strValue);
              }
            }
          }
        }
      }
    }
  }

  /**
   * Pomocný objekt, který drží informace o vytvořené kategorii a náhledu kategorie pro pozdější nastavení.
   */
  private static final class PreviewFileCategoryData {

    private final Category category;
    private final String previewFileGuid;

    private PreviewFileCategoryData(Category category, String previewFileGuid) {
      this.category = category;
      this.previewFileGuid = previewFileGuid;
    }

    public Category getCategory() {
      return category;
    }

    public String getPreviewFileGuid() {
      return previewFileGuid;
    }
  }
}
