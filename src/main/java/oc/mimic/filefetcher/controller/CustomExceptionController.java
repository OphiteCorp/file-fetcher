package oc.mimic.filefetcher.controller;

import oc.mimic.filefetcher.controller.model.EmptyModel;
import oc.mimic.filefetcher.exception.FileFetcherException;
import oc.mimic.filefetcher.utils.Json;
import oc.mimic.filefetcher.utils.Utils;
import org.apache.catalina.connector.ClientAbortException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Controller pro zachycení výjimek a jejich zpracování.
 *
 * @author mimic
 */
@RestControllerAdvice
public class CustomExceptionController extends ResponseEntityExceptionHandler {

  private static final Logger LOG = LoggerFactory.getLogger(CustomExceptionController.class);

  private static final String HEADER_FILE_ERROR = "File-Error";

  @Autowired
  private HttpServletRequest httpRequest;

  /**
   * Zachytí běžné chyby.
   */
  @Override
  protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers,
          HttpStatus status, WebRequest request) {

    printAccessDetails();

    if (status == HttpStatus.METHOD_NOT_ALLOWED) {
      LOG.error("{} ({})", status.name(), status.value());
    } else {
      LOG.error("Unrecognized application error: " + status.name() + " (" + status.value() + ")", ex);
    }
    return new ResponseEntity<>(new EmptyModel(), HttpStatus.OK);
  }

  /**
   * Vnitřní chyba aplikace.
   */
  @ExceptionHandler(ClientAbortException.class)
  protected ResponseEntity<Object> handleClientAbortException() {
    // tato chyba se ignoruje - uzivatel vypnul stream souboru
    return new ResponseEntity<>(new EmptyModel(), HttpStatus.OK);
  }

  /**
   * Vnitřní chyba aplikace.
   */
  @ExceptionHandler(Throwable.class)
  protected ResponseEntity<Object> handleException(Throwable ex, HttpServletResponse response) {
    printAccessDetails();

    if (ex instanceof FileFetcherException) {
      if (ex.getMessage() != null) {
        LOG.error("Strange behavior occurred -> " + ex.getMessage());
      }
    } else {
      LOG.error("An unexpected error occurred.", ex);
    }
    response.setHeader(HEADER_FILE_ERROR, Boolean.TRUE.toString());
    return new ResponseEntity<>(new EmptyModel(), HttpStatus.OK);
  }

  private void printAccessDetails() {
    String url = Utils.getAbsoluteUrl(httpRequest);
    Enumeration<String> parameterNames = httpRequest.getParameterNames();
    Map<String, String> parameters = new LinkedHashMap<>();

    while (parameterNames.hasMoreElements()) {
      String key = parameterNames.nextElement();
      parameters.put(key, httpRequest.getParameter(key));
    }
    LOG.error("[{}] [{}] {}\n{}", httpRequest.getRemoteAddr(), httpRequest.getMethod(), url,
            Json.toPrettyJson(parameters));
  }
}
