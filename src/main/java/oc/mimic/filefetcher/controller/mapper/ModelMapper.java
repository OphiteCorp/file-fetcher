package oc.mimic.filefetcher.controller.mapper;

import oc.mimic.filefetcher.component.MimeTypeResolver;
import oc.mimic.filefetcher.config.ApplicationConfig;
import oc.mimic.filefetcher.controller.model.category.AssignFilesToCategoryModelForm;
import oc.mimic.filefetcher.controller.model.category.CategoryModel;
import oc.mimic.filefetcher.controller.model.category.CreateCategoryModelForm;
import oc.mimic.filefetcher.controller.model.category.UpdateCategoryModelForm;
import oc.mimic.filefetcher.controller.model.file.CreateFetchFileModelForm;
import oc.mimic.filefetcher.controller.model.file.FetchFileModel;
import oc.mimic.filefetcher.controller.model.file.UpdateFetchFileModelForm;
import oc.mimic.filefetcher.controller.model.statistics.StatisticsModel;
import oc.mimic.filefetcher.controller.model.user.CreateUserModelForm;
import oc.mimic.filefetcher.db.dao.data.CategoryExtra;
import oc.mimic.filefetcher.db.entity.Category;
import oc.mimic.filefetcher.db.entity.FetchFile;
import oc.mimic.filefetcher.db.entity.User;
import oc.mimic.filefetcher.db.service.data.CategoryData;
import oc.mimic.filefetcher.db.service.data.StatisticsData;
import oc.mimic.filefetcher.utils.Utils;
import oc.mimic.filefetcher.utils.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Mapování dat do medelu.
 *
 * @author mimic
 */
public final class ModelMapper {

  private static final Logger LOG = LoggerFactory.getLogger(ModelMapper.class);

  // STATISTICS

  public static StatisticsModel mapToStatisticsModel(StatisticsData data) {
    StatisticsModel model = new StatisticsModel();
    model.setTotalCategories(data.getTotalCategories());
    model.setTotalFiles(data.getTotalFiles());
    model.setTotalImages(data.getTotalImages());
    model.setTotalVideos(data.getTotalVideos());
    model.setTotalFilesSize(data.getTotalFilesSize());
    model.setTotalHiddenFiles(data.getTotalHiddenFiles());
    model.setTotalUsers(data.getTotalUsers());

    RuntimeMXBean runBean = ManagementFactory.getRuntimeMXBean();
    model.setUptime(runBean.getUptime());

    return model;
  }

  // USER

  public static CreateUserModelForm mapToCreateUserModelForm(User user) {
    CreateUserModelForm model = new CreateUserModelForm();
    model.setLogin(user.getLogin());
    model.setPassword(user.getPassword());
    return model;
  }

  // CATEGORY

  public static CreateCategoryModelForm mapToCreateCategoryModelForm(Category category) {
    CreateCategoryModelForm model = new CreateCategoryModelForm();
    model.setGuid(category.getGuid());
    model.setName(category.getName());
    model.setDescription(category.getDescription());
    model.setOrder(category.getOrder());
    model.setSortColumn((category.getSortColumn() != null) ? category.getSortColumn().getColumnName() : null);
    model.setParentGuid(category.getParentGuid());
    model.setPreviewFile((category.getPreviewFile() != null) ? category.getPreviewFile().getGuid() : null);
    return model;
  }

  public static UpdateCategoryModelForm mapToUpdateCategoryModelForm(Category category) {
    CreateCategoryModelForm createModel = mapToCreateCategoryModelForm(category);
    return new UpdateCategoryModelForm(createModel);
  }

  public static AssignFilesToCategoryModelForm mapToAssignFilesToCategoryModelForm(String categoryGuid,
          List<FetchFile> fetchFiles, Boolean withoutCategory) {

    AssignFilesToCategoryModelForm model = new AssignFilesToCategoryModelForm();
    model.setCategoryGuid(categoryGuid);
    model.setWithoutCategory(withoutCategory);

    List<String> files = new ArrayList<>();
    for (FetchFile ff : fetchFiles) {
      files.add(ff.getGuid());
    }
    model.setFiles(files);
    return model;
  }

  public static void mapToCategoryModel(CategoryModel model, CategoryData data) {
    if (data != null && data.getCategory() != null) {
      Category category = data.getCategory();
      CategoryExtra extra = data.getExtra();
      CategoryModel.InnerCategory inner = new CategoryModel.InnerCategory();
      inner.setGuid(category.getGuid());
      inner.setName(category.getName());
      inner.setDescription(category.getDescription());
      inner.setCreated(category.getCreated().getTime());
      inner.setNewCategory(data.isNewCategory());
      inner.setNewCategoryLeft(data.getNewCategoryLeft());
      inner.setProtected(data.isProtected());
      inner.setAccessAllowed(data.isAccessAllowed());

      // pokud bude mít kategorie vlastní náhled, tak ho přednostně použije
      if (category.getPreviewFile() != null) {
        CategoryModel.InnerPreviewFileData previewFileData = preparePreviewFileData(extra);
        inner.setPreviewFile(previewFileData);
      }
      if (data.isAccessAllowed()) {
        if (extra != null) { // pokud uživatel nemá oprávnění, tak bude null
          inner.setFilesCount(extra.getCategoryFiles());
          inner.setSubCategoriesCount(extra.getSubcategoryCount());
          inner.setTotalFilesCount(extra.getTotalCategoryFiles());
          inner.setTotalCategorySize(extra.getTotalCategorySize());

          if (inner.getPreviewFile() == null && extra.isPreviewFile()) {
            CategoryModel.InnerPreviewFileData previewFileData = preparePreviewFileData(extra);
            inner.setPreviewFile(previewFileData);
          }
        }
      }
      model.getCategories().add(inner);
    }
  }

  // FILE

  public static CreateFetchFileModelForm mapToCreateFetchFileModelForm(FetchFile ff) {
    CreateFetchFileModelForm model = new CreateFetchFileModelForm();
    model.setGuid(ff.getGuid());
    model.setName(ff.getName());
    model.setAuthor(ff.getAuthor());
    model.setDescription(ff.getDescription());
    model.setOrder(ff.getOrder());
    model.setSource(ff.getSource());
    model.setGrade((ff.getGrade() != null) ? ff.getGrade().getCode() : null);
    model.setHidden(ff.isHidden());
    model.setCategory(ff.getCategoryGuid());
    return model;
  }

  public static UpdateFetchFileModelForm mapToUpdateFetchFileModelForm(FetchFile ff) {
    CreateFetchFileModelForm createFetchFileModel = mapToCreateFetchFileModelForm(ff);
    return new UpdateFetchFileModelForm(createFetchFileModel);
  }

  public static FetchFileModel mapToFetchFileModel(FetchFile ff, ApplicationConfig applicationConfig,
          MimeTypeResolver mimeTypeResolver) {

    File file = new File(ff.getSource());
    boolean fileExists = Validator.isFileExists(ff.getSource());

    FetchFileModel model = new FetchFileModel();
    model.setGuid(ff.getGuid());
    model.setName(ff.getName());
    model.setAuthor(ff.getAuthor());
    model.setDescription(ff.getDescription());
    model.setCreatedRecord(ff.getCreated().getTime());
    model.setExtension(Utils.getFileExtension(ff.getSource()));
    model.setChecksum(ff.getChecksum());
    model.setGrade((ff.getGrade() != null) ? ff.getGrade().getCode() : null);
    model.setHidden(ff.isHidden());
    model.setExists(fileExists);
    model.setNewFile(Validator.isDateNewerThen(ff.getCreated(), applicationConfig.getMarkAsNewInDays()));
    model.setNewFileLeft(
            Utils.getElapsedTimeFromNow(Utils.addDaysToDate(ff.getCreated(), applicationConfig.getMarkAsNewInDays())));
    model.setExtraType((ff.getExtraType() != null) ? ff.getExtraType().getCode() : null);
    model.setExtraData(ff.getExtra());

    if (fileExists) {
      BasicFileAttributes attributes = getFileAttributes(ff.getSource());
      if (attributes != null) {
        // musí zde být lastModifiedTime, protože ten ukazuje reálný čas vytvoření nebo pořízení souboru/fotky
        model.setCreatedFile(attributes.lastModifiedTime().to(TimeUnit.MILLISECONDS));
      }
      model.setSize(file.length());
      model.setMimeType(mimeTypeResolver.resolveMimeType(Paths.get(ff.getSource())));
    }
    return model;
  }

  public static List<FetchFileModel> mapToFetchFilesModel(List<FetchFile> fetchFiles,
          ApplicationConfig applicationConfig, MimeTypeResolver mimeTypeResolver) {

    List<FetchFileModel> modelList = new ArrayList<>();

    for (FetchFile ff : fetchFiles) {
      FetchFileModel model = mapToFetchFileModel(ff, applicationConfig, mimeTypeResolver);
      modelList.add(model);
    }
    return modelList;
  }

  private static BasicFileAttributes getFileAttributes(String source) {
    try {
      return Files.readAttributes(Paths.get(source), BasicFileAttributes.class);
    } catch (IOException e) {
      LOG.error("Could not get file attributes: " + source, e);
      return null;
    }
  }

  private static CategoryModel.InnerPreviewFileData preparePreviewFileData(CategoryExtra extra) {
    CategoryModel.InnerPreviewFileData previewFile = new CategoryModel.InnerPreviewFileData();
    previewFile.setGuid(extra.getPreviewGuid());
    previewFile.setChecksum(extra.getPreviewChecksum());
    previewFile.setWidth(extra.getPreviewWidth());
    previewFile.setHeight(extra.getPreviewHeight());
    return previewFile;
  }
}
