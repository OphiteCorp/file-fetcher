package oc.mimic.filefetcher.controller;

import oc.mimic.filefetcher.config.ApplicationConfig;
import oc.mimic.filefetcher.controller.model.EmptyModel;
import oc.mimic.filefetcher.controller.model.ErrorModel;
import oc.mimic.filefetcher.utils.Json;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * Controller pro zachycení stránky nebo URL, která neexistuje.
 *
 * @author mimic
 */
@RestController
public class CustomErrorController extends AbstractController implements ErrorController {

  private static final Logger LOG = LoggerFactory.getLogger(CustomErrorController.class);
  private static final String ERROR_PAGE = "/error";

  @Autowired
  private ApplicationConfig applicationConfig;

  @RequestMapping(ERROR_PAGE)
  @ResponseBody
  public ResponseEntity<Object> error(HttpServletRequest httpRequest) {
    ErrorModel model = ErrorModel.create(httpRequest);

    if (applicationConfig.isControllerErrorDetailLog()) {
      LOG.error("[{}] [{}] {}\n{}", model.getRemoteHost(), model.getMethod(), model.getUrl(), Json.toPrettyJson(model));
    } else {
      LOG.error("[{}] [{}] {}", model.getRemoteHost(), model.getMethod(), model.getUrl());
    }
    return new ResponseEntity<>(new EmptyModel(), HttpStatus.OK);
  }

  @Override
  public String getErrorPath() {
    return ERROR_PAGE;
  }
}
