package oc.mimic.filefetcher.controller;

import oc.mimic.filefetcher.controller.mapper.ModelMapper;
import oc.mimic.filefetcher.controller.model.category.UpdateCategoryModelForm;
import oc.mimic.filefetcher.controller.model.file.UpdateFetchFileModelForm;
import oc.mimic.filefetcher.controller.model.tool.DirToJsonModel;
import oc.mimic.filefetcher.controller.model.tool.DirToJsonModelForm;
import oc.mimic.filefetcher.db.entity.Category;
import oc.mimic.filefetcher.db.entity.FetchFile;
import oc.mimic.filefetcher.db.entity.User;
import oc.mimic.filefetcher.db.service.AbstractService;
import oc.mimic.filefetcher.db.service.CategoryService;
import oc.mimic.filefetcher.db.service.FetchFileService;
import oc.mimic.filefetcher.db.service.UserService;
import oc.mimic.filefetcher.utils.Json;
import oc.mimic.filefetcher.utils.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Controller s nástroji.
 *
 * @author mimic
 */
@RestController
public class ToolsController extends AbstractController {

  private static final Logger LOG = LoggerFactory.getLogger(ToolsController.class);
  public static final String TOOL = "/tool";

  @Autowired
  private UserService userService;

  @Autowired
  private CategoryService categoryService;

  @Autowired
  private FetchFileService fetchFileService;

  /**
   * Smaže všechny data v aplikaci.
   */
  @PostMapping(TOOL + "/clean-everything")
  public ResponseEntity<Map<String, List<String>>> cleanEverything() {
    Map<String, List<String>> modelMap = new LinkedHashMap<>();

    List<FetchFile> files = fetchFileService.deleteAll();
    List<String> filesList = new ArrayList<>(files.size());
    for (FetchFile ff : files) {
      filesList.add(ff.getGuid());
    }
    modelMap.put("files", filesList);
    LOG.info("Deleted {} records for files.", files.size());

    List<Category> categories = categoryService.deleteAll();
    List<String> categoriesList = new ArrayList<>(categories.size());
    for (Category category : categories) {
      categoriesList.add(category.getGuid());
    }
    modelMap.put("categories", categoriesList);
    LOG.info("Deleted {} records for categories.", categories.size());

    List<User> users = userService.deleteAll();
    List<String> usersList = new ArrayList<>(users.size());
    for (User user : users) {
      usersList.add(user.getLogin());
    }
    modelMap.put("users", usersList);
    LOG.info("Deleted {} records for users.", users.size());

    LOG.info("All data has been deleted.\n{}", Json.toPrettyJson(modelMap));
    return new ResponseEntity<>(modelMap, HttpStatus.OK);
  }

  /**
   * Vynutí přegenerování všech GUID pro kategorie a soubory.
   */
  @PostMapping(TOOL + "/regenerate-all-guids")
  public ResponseEntity<Map<String, Map<String, String>>> regenerateGuids() {
    Map<String, Map<String, String>> modelMap = new LinkedHashMap<>();

    List<Long> allCategoriesIds = categoryService.getAll().stream().map(Category::getId).collect(Collectors.toList());
    Map<String, String> categoriesMap = new LinkedHashMap<>();

    allCategoriesIds.forEach(id -> {
      Category category = categoryService.getById(id);
      UpdateCategoryModelForm form = ModelMapper.mapToUpdateCategoryModelForm(category);
      form.setGuid(category.getGuid() + "/" + AbstractService.NEW_GUID_MODIFIER);
      Category updatedCategory = categoryService.update(form);
      categoriesMap.put(category.getGuid(), updatedCategory.getGuid());
      LOG.info("The GUID for category '{}' ({}) has been regenerated.", category.getGuid(), category.getName());
    });
    modelMap.put("categories", categoriesMap);

    List<Long> allFilesIds = fetchFileService.getAll().stream().map(FetchFile::getId).collect(Collectors.toList());
    Map<String, String> filesMap = new LinkedHashMap<>();

    allFilesIds.forEach(id -> {
      FetchFile ff = fetchFileService.getById(id);
      UpdateFetchFileModelForm form = ModelMapper.mapToUpdateFetchFileModelForm(ff);
      form.setGuid(ff.getGuid() + "/" + AbstractService.NEW_GUID_MODIFIER);
      FetchFile updatedFile = fetchFileService.update(form);
      filesMap.put(ff.getGuid(), updatedFile.getGuid());
      LOG.info("The GUID for file '{}' has been regenerated.", ff.getGuid());
    });
    modelMap.put("files", filesMap);

    LOG.info("All GUID's have been regenerated.\n{}", Json.toPrettyJson(modelMap));
    return new ResponseEntity<>(modelMap, HttpStatus.OK);
  }

  /**
   * Převede adresář do JSON formátu, který se používá pro import.
   */
  @PostMapping(TOOL + "/dir-to-json")
  public ResponseEntity<Map<String, List<DirToJsonModel>>> dirToJson(@RequestBody DirToJsonModelForm form) {
    File dir = new File(form.getPath());
    List<DirToJsonModel> list = new ArrayList<>();

    if (dir.exists()) {
      try (Stream<Path> paths = Files.walk(dir.toPath(), 1)) { // 1 - pouze vstupní adresář
        try (Stream<Path> file = paths.filter(p -> {
          String fileName = p.toFile().getName().strip().toLowerCase();
          return (Files.isRegularFile(p) && !fileName.endsWith("thumbs.db"));
        })) {
          file.forEach(path -> {
            File f = path.toFile();
            DirToJsonModel model = new DirToJsonModel();
            model.setAuthor(form.getAuthor());
            model.setSource(Validator.isNullOrBlank(form.getPathPrefix()) ? f.getAbsolutePath()
                                                                          : form.getPathPrefix() + "/" + f.getName());
            list.add(model);
          });
        }
      } catch (IOException e) {
        LOG.error("An error occurred while creating JSON from the directory: " + dir, e);
      }
    }
    Map<String, List<DirToJsonModel>> map = new HashMap<>();
    map.put("files", list);
    return new ResponseEntity<>(map, HttpStatus.OK);
  }
}
