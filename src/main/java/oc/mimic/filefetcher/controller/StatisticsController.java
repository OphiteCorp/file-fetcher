package oc.mimic.filefetcher.controller;

import oc.mimic.filefetcher.controller.mapper.ModelMapper;
import oc.mimic.filefetcher.controller.model.statistics.StatisticsModel;
import oc.mimic.filefetcher.db.service.StatisticService;
import oc.mimic.filefetcher.db.service.data.StatisticsData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller pro statistiku.
 *
 * @author mimic
 */
@RestController
public class StatisticsController extends AbstractController {

  private static final Logger LOG = LoggerFactory.getLogger(StatisticsController.class);
  public static final String STATS = "/stats";

  @Autowired
  private StatisticService statisticService;

  @GetMapping(STATS)
  public ResponseEntity<StatisticsModel> getStatistics() {
    StatisticsData data = statisticService.getStatistics();
    StatisticsModel model = ModelMapper.mapToStatisticsModel(data);
    LOG.debug("Statistics have been generated.");
    return new ResponseEntity<>(model, HttpStatus.OK);
  }
}
