package oc.mimic.filefetcher.controller.model.category;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Model pro aktualizaci kategorie.
 *
 * @author mimic
 */
@JsonInclude
public final class UpdateCategoryModelForm extends CreateCategoryModelForm {

  // musí být pro GSON builder
  public UpdateCategoryModelForm() {
  }

  public UpdateCategoryModelForm(ICategoryModelForm model) {
    setGuid(model.getGuid());
    setName(model.getName());
    setDescription(model.getDescription());
    setOrder(model.getOrder());
    setSortColumn(model.getSortColumn());
    setParentGuid(model.getParentGuid());
    setPreviewFile(model.getPreviewFile());
  }
}
