package oc.mimic.filefetcher.controller.model.file;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Model s informacemi o dostupnosti a oprávnění souboru pro uživatele.
 *
 * @author mimic
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public final class FetchFileAccessModel {

  @JsonProperty("guid")
  private String guid;

  @JsonProperty("available")
  private boolean available;

  public String getGuid() {
    return guid;
  }

  public void setGuid(String guid) {
    this.guid = guid;
  }

  public boolean isAvailable() {
    return available;
  }

  public void setAvailable(boolean available) {
    this.available = available;
  }
}
