package oc.mimic.filefetcher.controller.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import oc.mimic.filefetcher.filter.LoggingAccessFilter;
import oc.mimic.filefetcher.utils.Utils;
import org.springframework.http.HttpStatus;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Drží kompletní informace o chybě.
 *
 * @author mimic
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public final class ErrorModel {

  @JsonProperty("time")
  private final String time;

  @JsonProperty("error")
  private String message;

  @JsonProperty("method")
  private String method;

  @JsonProperty("status")
  private int status;

  @JsonProperty("host")
  private String remoteHost;

  @JsonProperty("url")
  private String url;

  @JsonProperty("headers")
  private Map<String, String> headers;

  @JsonProperty("cookies")
  private Map<String, String> cookies;

  public ErrorModel() {
    super();
    time = Utils.getCurrentDateTime();
  }

  /**
   * Vytvoří novou instanci.
   */
  public static ErrorModel create(HttpServletRequest httpRequest) {
    return create(httpRequest, null, null);
  }

  /**
   * Vytvoří novou instanci.
   */
  public static ErrorModel create(HttpServletRequest httpRequest, String message) {
    return create(httpRequest, null, message);
  }

  /**
   * Vytvoří novou instanci.
   */
  public static ErrorModel create(HttpServletRequest httpRequest, HttpStatus customStatus, String message) {
    Object requestStatus = httpRequest.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
    HttpStatus status = null;

    if (requestStatus != null) {
      status = HttpStatus.resolve((Integer) requestStatus);
    }
    if (customStatus != null) {
      status = customStatus;
    }
    if (status == null) {
      status = HttpStatus.OK;
    }
    String absoluteUrl = (String) httpRequest.getAttribute(LoggingAccessFilter.ATTR_ABSOLUTE_URL);
    ErrorModel model = new ErrorModel();
    model.setStatus(status.value());
    model.setUrl((absoluteUrl != null) ? absoluteUrl : Utils.getAbsoluteUrl(httpRequest));
    model.setMethod(httpRequest.getMethod());
    model.setRemoteHost(httpRequest.getRemoteHost());
    model.setMessage(message);

    Map<String, String> headersMap = new LinkedHashMap<>();
    Enumeration<String> headerNames = httpRequest.getHeaderNames();

    while (headerNames.hasMoreElements()) {
      String headerName = headerNames.nextElement();
      String header = httpRequest.getHeader(headerName);
      headersMap.put(headerName, header);
    }
    model.setHeaders(headersMap);

    Map<String, String> cookiesMap = new LinkedHashMap<>();
    Cookie[] cookies = httpRequest.getCookies();

    if (cookies != null) {
      for (Cookie cookie : cookies) {
        cookiesMap.put(cookie.getName(), cookie.getValue());
      }
    }
    model.setCookies(cookiesMap);

    return model;
  }

  public HttpStatus getHttpStatus() {
    return HttpStatus.resolve(status);
  }

  public int getStatus() {
    return status;
  }

  public void setStatus(int status) {
    this.status = status;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getMethod() {
    return method;
  }

  public void setMethod(String method) {
    this.method = method;
  }

  public String getRemoteHost() {
    return remoteHost;
  }

  public void setRemoteHost(String remoteHost) {
    this.remoteHost = remoteHost;
  }

  public Map<String, String> getHeaders() {
    return headers;
  }

  public void setHeaders(Map<String, String> headers) {
    this.headers = headers;
  }

  public Map<String, String> getCookies() {
    return cookies;
  }

  public void setCookies(Map<String, String> cookies) {
    this.cookies = cookies;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public String getTime() {
    return time;
  }
}
