package oc.mimic.filefetcher.controller.model.user;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Model pro změnu opránění uživatele do kategorii.
 *
 * @author mimic
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public final class ChangeCategoriesModelForm {

  @JsonProperty("login")
  private String login;

  @JsonProperty("append")
  private boolean append = true;

  @JsonProperty("categories")
  private List<String> categories;

  public String getLogin() {
    return login;
  }

  public void setLogin(String login) {
    this.login = login;
  }

  public List<String> getCategories() {
    return categories;
  }

  public void setCategories(List<String> categories) {
    this.categories = categories;
  }

  public boolean isAppend() {
    return append;
  }

  public void setAppend(boolean append) {
    this.append = append;
  }
}
