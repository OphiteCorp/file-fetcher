package oc.mimic.filefetcher.controller.model.loader;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Model pro loader.
 *
 * @author mimic
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LoaderForm {

  /**
   * JSON konfigurace pro přegenerování všech GUID do nových a unikátních.
   */
  public static final String CONF_REGENERATE_GUIDS = "regen-guids";

  /**
   * JSON konfigurace pro přegenerování všech pořadí kategorii dle importu.
   */
  public static final String CONF_REGENERATE_ORDERS_CATEGORY = "regen-orders-category";

  /**
   * JSON konfigurace pro přegenerování všech pořadí souborů dle importu.
   */
  public static final String CONF_REGENERATE_ORDERS_FILE = "regen-orders-file";

  @JsonProperty("config")
  private final Map<String, Object> config = new LinkedHashMap<>();

  @JsonProperty("vars")
  private final Map<String, Object> variables = new LinkedHashMap<>();

  @JsonProperty("users")
  private final List<LoaderUserForm> users = new ArrayList<>();

  @JsonProperty("data")
  private final List<LoaderCategoryForm> data = new ArrayList<>();

  @JsonProperty("files_without_category")
  private final List<LoaderFetchFileForm> filesWithoutCategory = new ArrayList<>();

  public List<LoaderUserForm> getUsers() {
    return users;
  }

  public List<LoaderCategoryForm> getData() {
    return data;
  }

  public Map<String, Object> getVariables() {
    return variables;
  }

  public Map<String, Object> getConfig() {
    return config;
  }

  public List<LoaderFetchFileForm> getFilesWithoutCategory() {
    return filesWithoutCategory;
  }
}
