package oc.mimic.filefetcher.controller.model.loader;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import oc.mimic.filefetcher.controller.model.category.CreateCategoryModelForm;
import oc.mimic.filefetcher.utils.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Model pro loader, který obsahuje data v kategorii včetně souborů.
 *
 * @author mimic
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class LoaderCategoryForm extends CreateCategoryModelForm implements ILoaderFormWrapper {

  @JsonIgnore
  private final String id;

  @JsonProperty("users")
  private final List<String> users = new ArrayList<>(0); // většinou nebudou žádní uživatele

  @JsonProperty("subcategories")
  private final List<LoaderCategoryForm> subcategories = new ArrayList<>();

  @JsonProperty("files")
  private final List<LoaderFetchFileForm> files = new ArrayList<>();

  public LoaderCategoryForm() {
    super();
    id = Utils.generateUuid();
  }

  @Override
  public String getId() {
    return id;
  }

  public List<LoaderFetchFileForm> getFiles() {
    return files;
  }

  public List<LoaderCategoryForm> getSubcategories() {
    return subcategories;
  }

  public List<String> getUsers() {
    return users;
  }
}
