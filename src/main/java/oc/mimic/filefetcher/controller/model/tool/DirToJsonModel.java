package oc.mimic.filefetcher.controller.model.tool;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Model s výstupním JSON pro adresář.
 *
 * @author mimic
 */
@JsonInclude
public class DirToJsonModel {

  @JsonProperty("source")
  private String source;

  @JsonProperty("author")
  private String author;

  public String getSource() {
    return source;
  }

  public void setSource(String source) {
    this.source = source;
  }

  public String getAuthor() {
    return author;
  }

  public void setAuthor(String author) {
    this.author = author;
  }
}
