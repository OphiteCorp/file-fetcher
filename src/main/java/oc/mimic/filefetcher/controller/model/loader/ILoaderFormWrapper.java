package oc.mimic.filefetcher.controller.model.loader;

/**
 * Rozhraní pro získání ID objektu.
 *
 * @author mimic
 */
public interface ILoaderFormWrapper {

  /**
   * Získá unikátní ID.
   */
  String getId();
}
