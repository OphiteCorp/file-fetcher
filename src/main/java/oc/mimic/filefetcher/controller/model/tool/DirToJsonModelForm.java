package oc.mimic.filefetcher.controller.model.tool;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Model pro vygenerování JSON z adresáře pro import.
 *
 * @author mimic
 */
@JsonInclude
public class DirToJsonModelForm {

  @JsonProperty("author")
  private String author;

  @JsonProperty("path")
  private String path;

  @JsonProperty("path_prefix")
  private String pathPrefix;

  public String getAuthor() {
    return author;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }

  public String getPathPrefix() {
    return pathPrefix;
  }

  public void setPathPrefix(String pathPrefix) {
    this.pathPrefix = pathPrefix;
  }
}
