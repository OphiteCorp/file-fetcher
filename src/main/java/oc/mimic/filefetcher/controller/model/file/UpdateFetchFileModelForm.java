package oc.mimic.filefetcher.controller.model.file;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Model pro aktualizaci souboru.
 *
 * @author mimic
 */
@JsonInclude
public final class UpdateFetchFileModelForm extends CreateFetchFileModelForm {

  // musí být pro GSON builder
  public UpdateFetchFileModelForm() {
  }

  public UpdateFetchFileModelForm(IFetchFileModel model) {
    setGuid(model.getGuid());
    setName(model.getName());
    setAuthor(model.getAuthor());
    setDescription(model.getDescription());
    setOrder(model.getOrder());
    setSource(model.getSource());
    setGrade(model.getGrade());
    setHidden(model.isHidden());
    setCategory(model.getCategory());
  }
}
