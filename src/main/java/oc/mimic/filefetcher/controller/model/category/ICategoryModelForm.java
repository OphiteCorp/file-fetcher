package oc.mimic.filefetcher.controller.model.category;

/**
 * Rozhrani modelu pro vytvoreni a editaci kategorie.
 *
 * @author mimic
 */
public interface ICategoryModelForm {

  String getName();

  String getDescription();

  String getParentGuid();

  Integer getOrder();

  String getGuid();

  String getPreviewFile();

  String getSortColumn();
}
