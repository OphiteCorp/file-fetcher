package oc.mimic.filefetcher.controller.model.loader;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import oc.mimic.filefetcher.controller.model.user.CreateUserModelForm;
import oc.mimic.filefetcher.utils.Utils;

/**
 * Model pro loader s informacemi o uživateli.
 *
 * @author mimic
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class LoaderUserForm extends CreateUserModelForm implements ILoaderFormWrapper {

  @JsonIgnore
  private final String id;

  public LoaderUserForm() {
    super();
    id = Utils.generateUuid();
  }

  @Override
  public String getId() {
    return id;
  }
}
