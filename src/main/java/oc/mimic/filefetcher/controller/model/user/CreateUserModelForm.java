package oc.mimic.filefetcher.controller.model.user;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Model pro vytvoření uživatele.
 *
 * @author mimic
 */
@JsonInclude
public class CreateUserModelForm {

  @JsonProperty("login")
  private String login;

  @JsonProperty("password")
  private String password;

  public String getLogin() {
    return login;
  }

  public void setLogin(String login) {
    this.login = login;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }
}
