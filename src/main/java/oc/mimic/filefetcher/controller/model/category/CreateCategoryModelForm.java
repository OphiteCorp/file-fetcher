package oc.mimic.filefetcher.controller.model.category;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Model pro vytvoření kategorie.
 *
 * @author mimic
 */
@JsonInclude
public class CreateCategoryModelForm implements ICategoryModelForm {

  @JsonProperty("guid")
  private String guid;

  @JsonProperty("name")
  private String name;

  @JsonProperty("desc")
  private String description;

  @JsonProperty("order")
  private Integer order;

  @JsonProperty("sort_column")
  private String sortColumn;

  @JsonProperty("parent")
  private String parentGuid;

  @JsonProperty("preview_file")
  private String previewFile;

  @Override
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  @Override
  public String getParentGuid() {
    return parentGuid;
  }

  public void setParentGuid(String parentGuid) {
    this.parentGuid = parentGuid;
  }

  @Override
  public Integer getOrder() {
    return order;
  }

  public void setOrder(Integer order) {
    this.order = order;
  }

  @Override
  public String getGuid() {
    return guid;
  }

  public void setGuid(String guid) {
    this.guid = guid;
  }

  @Override
  public String getPreviewFile() {
    return previewFile;
  }

  public void setPreviewFile(String previewFile) {
    this.previewFile = previewFile;
  }

  @Override
  public String getSortColumn() {
    return sortColumn;
  }

  public void setSortColumn(String sortColumn) {
    this.sortColumn = sortColumn;
  }
}
