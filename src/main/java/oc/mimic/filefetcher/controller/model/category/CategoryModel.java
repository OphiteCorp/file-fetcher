package oc.mimic.filefetcher.controller.model.category;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import oc.mimic.filefetcher.controller.model.file.FetchFileModel;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Model s informacemi o kategorii, které jsou veřejně dostupné.
 *
 * @author mimic
 */
@JsonInclude
public final class CategoryModel {

  @JsonProperty("categories")
  private final List<InnerCategory> categories = new ArrayList<>();

  @JsonProperty("files")
  private final List<FetchFileModel> files = new ArrayList<>();

  @JsonProperty("paths")
  private final Map<String, InnerCategoryPath> paths = new LinkedHashMap<>();

  public List<InnerCategory> getCategories() {
    return categories;
  }

  public Map<String, InnerCategoryPath> getPaths() {
    return paths;
  }

  public List<FetchFileModel> getFiles() {
    return files;
  }

  @JsonInclude(JsonInclude.Include.NON_NULL)
  public static final class InnerCategoryPath {

    @JsonProperty("name")
    private String name;

    @JsonProperty("desc")
    private String description;

    public InnerCategoryPath(String name, String description) {
      this.name = name;
      this.description = description;
    }

    public String getName() {
      return name;
    }

    public void setName(String name) {
      this.name = name;
    }

    public String getDescription() {
      return description;
    }

    public void setDescription(String description) {
      this.description = description;
    }
  }

  @JsonInclude(JsonInclude.Include.NON_NULL)
  public static final class InnerCategory {

    @JsonProperty("guid")
    private String guid;

    @JsonProperty("name")
    private String name;

    @JsonProperty("desc")
    private String description;

    @JsonProperty("created")
    private Long created;

    @JsonProperty("newly_added")
    private boolean newCategory;

    @JsonProperty("newly_added_left")
    private long newCategoryLeft;

    @JsonProperty("protected")
    private boolean isProtected;

    @JsonProperty("access_allowed")
    private boolean accessAllowed;

    @JsonProperty("files_count")
    private Long filesCount;

    @JsonProperty("sub_categories_count")
    private Long subCategoriesCount;

    @JsonProperty("total_files_count")
    private Long totalFilesCount;

    @JsonProperty("total_category_size")
    private Long totalCategorySize;

    @JsonProperty("preview_file")
    private InnerPreviewFileData previewFile;

    public String getGuid() {
      return guid;
    }

    public void setGuid(String guid) {
      this.guid = guid;
    }

    public String getName() {
      return name;
    }

    public void setName(String name) {
      this.name = name;
    }

    public String getDescription() {
      return description;
    }

    public void setDescription(String description) {
      this.description = description;
    }

    public Long getCreated() {
      return created;
    }

    public void setCreated(Long created) {
      this.created = created;
    }

    public Long getFilesCount() {
      return filesCount;
    }

    public void setFilesCount(Long filesCount) {
      this.filesCount = filesCount;
    }

    public Long getSubCategoriesCount() {
      return subCategoriesCount;
    }

    public void setSubCategoriesCount(Long subCategoriesCount) {
      this.subCategoriesCount = subCategoriesCount;
    }

    public InnerPreviewFileData getPreviewFile() {
      return previewFile;
    }

    public void setPreviewFile(InnerPreviewFileData previewFile) {
      this.previewFile = previewFile;
    }

    public boolean isNewCategory() {
      return newCategory;
    }

    public void setNewCategory(boolean newCategory) {
      this.newCategory = newCategory;
    }

    public Long getTotalFilesCount() {
      return totalFilesCount;
    }

    public void setTotalFilesCount(Long totalFilesCount) {
      this.totalFilesCount = totalFilesCount;
    }

    public Long getTotalCategorySize() {
      return totalCategorySize;
    }

    public void setTotalCategorySize(Long totalCategorySize) {
      this.totalCategorySize = totalCategorySize;
    }

    public boolean isProtected() {
      return isProtected;
    }

    public void setProtected(boolean aProtected) {
      isProtected = aProtected;
    }

    public boolean isAccessAllowed() {
      return accessAllowed;
    }

    public void setAccessAllowed(boolean accessAllowed) {
      this.accessAllowed = accessAllowed;
    }

    public long getNewCategoryLeft() {
      return newCategoryLeft;
    }

    public void setNewCategoryLeft(long newCategoryLeft) {
      this.newCategoryLeft = newCategoryLeft;
    }
  }

  @JsonInclude(JsonInclude.Include.NON_NULL)
  public static final class InnerPreviewFileData {

    private String guid;
    private long checksum;
    private int width;
    private int height;

    public String getGuid() {
      return guid;
    }

    public void setGuid(String guid) {
      this.guid = guid;
    }

    public long getChecksum() {
      return checksum;
    }

    public void setChecksum(long checksum) {
      this.checksum = checksum;
    }

    public int getWidth() {
      return width;
    }

    public void setWidth(int width) {
      this.width = width;
    }

    public int getHeight() {
      return height;
    }

    public void setHeight(int height) {
      this.height = height;
    }
  }
}
