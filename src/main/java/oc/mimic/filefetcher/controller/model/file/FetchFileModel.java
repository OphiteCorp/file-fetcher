package oc.mimic.filefetcher.controller.model.file;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import oc.mimic.filefetcher.db.service.data.IFileExtraData;

/**
 * Model s informacemi o souboru, které jsou veřejně dostupné.
 *
 * @author mimic
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public final class FetchFileModel {

  @JsonProperty("guid")
  private String guid;

  @JsonProperty("name")
  private String name;

  @JsonProperty("desc")
  private String description;

  @JsonProperty("author")
  private String author;

  @JsonProperty("created_record")
  private Long createdRecord;

  @JsonProperty("created_file")
  private Long createdFile;

  @JsonProperty("ext")
  private String extension;

  @JsonProperty("mime_type")
  private String mimeType;

  @JsonProperty("size")
  private Long size;

  @JsonProperty("checksum")
  private long checksum;

  @JsonProperty("grade")
  private String grade;

  @JsonProperty("hidden")
  private boolean hidden;

  @JsonProperty("exists")
  private boolean exists;

  @JsonProperty("newly_added")
  private boolean newFile;

  @JsonProperty("newly_added_left")
  private long newFileLeft;

  @JsonProperty("extra_type")
  private String extraType;

  @JsonProperty("extra_data")
  private IFileExtraData extraData;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getGuid() {
    return guid;
  }

  public void setGuid(String guid) {
    this.guid = guid;
  }

  public Long getSize() {
    return size;
  }

  public void setSize(Long size) {
    this.size = size;
  }

  public String getAuthor() {
    return author;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  public String getExtension() {
    return extension;
  }

  public void setExtension(String extension) {
    this.extension = extension;
  }

  public long getChecksum() {
    return checksum;
  }

  public void setChecksum(long checksum) {
    this.checksum = checksum;
  }

  public boolean isExists() {
    return exists;
  }

  public void setExists(boolean exists) {
    this.exists = exists;
  }

  public String getGrade() {
    return grade;
  }

  public void setGrade(String grade) {
    this.grade = grade;
  }

  public boolean isNewFile() {
    return newFile;
  }

  public void setNewFile(boolean newFile) {
    this.newFile = newFile;
  }

  public boolean isHidden() {
    return hidden;
  }

  public void setHidden(boolean hidden) {
    this.hidden = hidden;
  }

  public String getMimeType() {
    return mimeType;
  }

  public void setMimeType(String mimeType) {
    this.mimeType = mimeType;
  }

  public String getExtraType() {
    return extraType;
  }

  public void setExtraType(String extraType) {
    this.extraType = extraType;
  }

  public IFileExtraData getExtraData() {
    return extraData;
  }

  public void setExtraData(IFileExtraData extraData) {
    this.extraData = extraData;
  }

  public long getNewFileLeft() {
    return newFileLeft;
  }

  public void setNewFileLeft(long newFileLeft) {
    this.newFileLeft = newFileLeft;
  }

  public Long getCreatedRecord() {
    return createdRecord;
  }

  public void setCreatedRecord(Long createdRecord) {
    this.createdRecord = createdRecord;
  }

  public Long getCreatedFile() {
    return createdFile;
  }

  public void setCreatedFile(Long createdFile) {
    this.createdFile = createdFile;
  }
}
