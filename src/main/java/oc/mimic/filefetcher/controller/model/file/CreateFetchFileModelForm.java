package oc.mimic.filefetcher.controller.model.file;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Model pro vytvoření souboru.
 *
 * @author mimic
 */
@JsonInclude
public class CreateFetchFileModelForm implements IFetchFileModel {

  @JsonProperty("guid")
  private String guid;

  @JsonProperty("name")
  private String name;

  @JsonProperty("author")
  private String author;

  @JsonProperty("desc")
  private String description;

  @JsonProperty("order")
  private Integer order;

  @JsonProperty("category")
  private String category;

  @JsonProperty("source")
  private String source;

  @JsonProperty("grade")
  private String grade;

  @JsonProperty("hidden")
  private boolean hidden;

  @Override
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  @Override
  public Integer getOrder() {
    return order;
  }

  public void setOrder(Integer order) {
    this.order = order;
  }

  @Override
  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  @Override
  public String getSource() {
    return source;
  }

  public void setSource(String source) {
    this.source = source;
  }

  @Override
  public String getGuid() {
    return guid;
  }

  public void setGuid(String guid) {
    this.guid = guid;
  }

  @Override
  public String getAuthor() {
    return author;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  @Override
  public String getGrade() {
    return grade;
  }

  public void setGrade(String grade) {
    this.grade = grade;
  }

  @Override
  public boolean isHidden() {
    return hidden;
  }

  public void setHidden(boolean hidden) {
    this.hidden = hidden;
  }
}
