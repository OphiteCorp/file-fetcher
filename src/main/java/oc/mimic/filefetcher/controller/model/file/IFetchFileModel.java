package oc.mimic.filefetcher.controller.model.file;

/**
 * Rozhrani modelu pro vytvoreni a editaci souboru.
 *
 * @author mimic
 */
public interface IFetchFileModel {

  String getName();

  String getDescription();

  Integer getOrder();

  String getCategory();

  String getSource();

  String getGuid();

  String getAuthor();

  String getGrade();

  boolean isHidden();
}
