package oc.mimic.filefetcher.controller.model.category;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Model pro přiřazení souborů do kategorie.
 *
 * @author mimic
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public final class AssignFilesToCategoryModelForm {

  @JsonProperty("category")
  private String categoryGuid;

  @JsonProperty("files")
  private List<String> files;

  @JsonProperty("without_category")
  private Boolean withoutCategory;

  public String getCategoryGuid() {
    return categoryGuid;
  }

  public void setCategoryGuid(String categoryGuid) {
    this.categoryGuid = categoryGuid;
  }

  public List<String> getFiles() {
    return files;
  }

  public void setFiles(List<String> files) {
    this.files = files;
  }

  public Boolean getWithoutCategory() {
    return withoutCategory;
  }

  public void setWithoutCategory(Boolean withoutCategory) {
    this.withoutCategory = withoutCategory;
  }
}
