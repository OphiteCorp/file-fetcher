package oc.mimic.filefetcher.controller.model.loader;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import oc.mimic.filefetcher.controller.model.file.CreateFetchFileModelForm;
import oc.mimic.filefetcher.utils.Utils;

/**
 * Model pro loader s informacemi o souboru.
 *
 * @author mimic
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class LoaderFetchFileForm extends CreateFetchFileModelForm implements ILoaderFormWrapper {

  @JsonIgnore
  private final String id;

  public LoaderFetchFileForm() {
    super();
    id = Utils.generateUuid();
  }

  @Override
  public String getId() {
    return id;
  }
}
