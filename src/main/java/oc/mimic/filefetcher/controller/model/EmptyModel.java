package oc.mimic.filefetcher.controller.model;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Prázdný model.
 *
 * @author mimic
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public final class EmptyModel {

}
