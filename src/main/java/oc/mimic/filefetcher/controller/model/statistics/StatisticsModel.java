package oc.mimic.filefetcher.controller.model.statistics;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Model statistiky.
 *
 * @author mimic
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public final class StatisticsModel {

  @JsonProperty("total_categories")
  private long totalCategories;

  @JsonProperty("total_files")
  private long totalFiles;

  @JsonProperty("total_images")
  private long totalImages;

  @JsonProperty("total_videos")
  private long totalVideos;

  @JsonProperty("total_files_size")
  private long totalFilesSize;

  @JsonProperty("total_hidden_files")
  private long totalHiddenFiles;

  @JsonProperty("total_users")
  private long totalUsers;

  @JsonProperty("uptime")
  private long uptime;

  public long getTotalCategories() {
    return totalCategories;
  }

  public void setTotalCategories(long totalCategories) {
    this.totalCategories = totalCategories;
  }

  public long getTotalFiles() {
    return totalFiles;
  }

  public void setTotalFiles(long totalFiles) {
    this.totalFiles = totalFiles;
  }

  public long getTotalHiddenFiles() {
    return totalHiddenFiles;
  }

  public void setTotalHiddenFiles(long totalHiddenFiles) {
    this.totalHiddenFiles = totalHiddenFiles;
  }

  public long getTotalUsers() {
    return totalUsers;
  }

  public void setTotalUsers(long totalUsers) {
    this.totalUsers = totalUsers;
  }

  public long getTotalFilesSize() {
    return totalFilesSize;
  }

  public void setTotalFilesSize(long totalFilesSize) {
    this.totalFilesSize = totalFilesSize;
  }

  public long getUptime() {
    return uptime;
  }

  public void setUptime(long uptime) {
    this.uptime = uptime;
  }

  public long getTotalImages() {
    return totalImages;
  }

  public void setTotalImages(long totalImages) {
    this.totalImages = totalImages;
  }

  public long getTotalVideos() {
    return totalVideos;
  }

  public void setTotalVideos(long totalVideos) {
    this.totalVideos = totalVideos;
  }
}
