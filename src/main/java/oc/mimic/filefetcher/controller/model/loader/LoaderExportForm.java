package oc.mimic.filefetcher.controller.model.loader;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Model pro export dat z loaderu.
 *
 * @author mimic
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LoaderExportForm {

  /**
   * JSON konfigurace pro odebrání všech řazení pro kategorie.
   */
  public static final String CONF_REMOVE_ORDERS_CATEGORY = "remove-orders-category";

  /**
   * JSON konfigurace pro odebrání všech řazení pro soubory.
   */
  public static final String CONF_REMOVE_ORDERS_FILE = "remove-orders-file";

  /**
   * JSON konfigurace pro odebrání všech GUID (jak pro kategorie, tak soubory).
   */
  public static final String CONF_REMOVE_GUIDS = "remove-guids";

  /**
   * JSON konfigurace pro odebrání všech uživatelských přístupů do kategorii.
   */
  public static final String CONF_REMOVE_USER_ACCESS = "remove-user-access";

  @JsonProperty("config")
  private final Map<String, Object> config = new LinkedHashMap<>();

  public Map<String, Object> getConfig() {
    return config;
  }
}
