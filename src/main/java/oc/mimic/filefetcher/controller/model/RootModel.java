package oc.mimic.filefetcher.controller.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Drží základní informace pro root stránku.
 *
 * @author mimic
 */
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public final class RootModel {

  @JsonProperty("message")
  private String message;

  @JsonProperty("version")
  private String version;

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public String getVersion() {
    return version;
  }

  public void setVersion(String version) {
    this.version = version;
  }
}
