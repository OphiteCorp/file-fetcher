package oc.mimic.filefetcher.controller;

import oc.mimic.filefetcher.config.ApplicationConfig;
import oc.mimic.filefetcher.controller.mapper.ModelMapper;
import oc.mimic.filefetcher.controller.model.user.ChangeCategoriesModelForm;
import oc.mimic.filefetcher.controller.model.user.ChangePasswordModelForm;
import oc.mimic.filefetcher.controller.model.user.CreateUserModelForm;
import oc.mimic.filefetcher.db.entity.Category;
import oc.mimic.filefetcher.db.entity.User;
import oc.mimic.filefetcher.db.service.UserService;
import oc.mimic.filefetcher.db.service.data.CredentialData;
import oc.mimic.filefetcher.exception.FileFetcherException;
import oc.mimic.filefetcher.utils.Json;
import oc.mimic.filefetcher.utils.Validator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * Controller pro správu uživatelů.
 *
 * @author mimic
 */
@RestController
public class UserController extends AbstractController {

  private static final Logger LOG = LoggerFactory.getLogger(UserController.class);
  private static final String USER = "/user";

  @Autowired
  private UserService userService;

  @Autowired
  private ApplicationConfig applicationConfig;

  /**
   * Vytvoří nového uživatele.
   */
  @PostMapping(USER + "/create")
  public ResponseEntity<List<CreateUserModelForm>> create(@RequestBody List<CreateUserModelForm> formList) {
    List<CreateUserModelForm> modelList = new ArrayList<>();

    for (CreateUserModelForm form : formList) {
      checkValidModelForm(form.getLogin(), form.getPassword());

      User createdUser = userService.create(form);
      if (createdUser != null) {
        modelList.add(ModelMapper.mapToCreateUserModelForm(createdUser));
      }
    }
    return new ResponseEntity<>(modelList, HttpStatus.OK);
  }

  /**
   * Změní heslo uživatele. Tuto metodu volá stránka.
   */
  @GetMapping(USER + "/change-password-token")
  public ResponseEntity<Boolean> changePassword( //
          @RequestParam(value = "token") String tokenParam, //
          @RequestParam(value = "newPassword") String newPasswordParam) {

    CredentialData credential = new CredentialData(tokenParam, applicationConfig);

    ChangePasswordModelForm form = new ChangePasswordModelForm();
    form.setLogin(credential.getLogin());
    form.setPassword(newPasswordParam);

    checkValidModelForm(form.getLogin(), form.getPassword());
    boolean changed = false;

    if (userService.isCredentialValid(credential)) {
      changed = userService.changePassword(form.getLogin(), form.getPassword());
      LOG.info("The password for user '{}' has been changed.", form.getLogin());
    } else {
      LOG.warn("The user's password could not be changed. User '{}' probably does not exist.", form.getLogin());
    }
    return new ResponseEntity<>(changed, HttpStatus.OK);
  }

  /**
   * Změní heslo uživatele. Pro testovací potřeby.
   */
  @PostMapping(USER + "/change-password")
  public ResponseEntity<Map<String, Boolean>> changePassword(@RequestBody List<ChangePasswordModelForm> formList) {
    Map<String, Boolean> modelMap = new LinkedHashMap<>();

    for (ChangePasswordModelForm form : formList) {
      checkValidModelForm(form.getLogin(), form.getPassword());

      User user = userService.getByLogin(form.getLogin());
      boolean changed = false;

      if (user != null) {
        changed = userService.changePassword(form.getLogin(), form.getPassword());
      }
      modelMap.put(form.getLogin(), changed);

      if (user != null && changed) {
        LOG.info("The password for user '{}' has been changed.", form.getLogin());
      } else {
        LOG.warn("The user's password could not be changed. User '{}' does not exist.", form.getLogin());
      }
    }
    return new ResponseEntity<>(modelMap, HttpStatus.OK);
  }

  /**
   * Změní kategorie uživatele.
   */
  @PostMapping(USER + "/change-categories")
  public ResponseEntity<Map<String, Map<String, String>>> changeCategories(
          @RequestBody List<ChangeCategoriesModelForm> formList) {

    Map<String, Map<String, String>> modelMap = new LinkedHashMap<>();

    for (ChangeCategoriesModelForm form : formList) {
      checkValidModelForm(form.getLogin());

      User user = userService.getByLogin(form.getLogin());
      if (user != null) {
        // přiřadí uživatele do kategorie
        List<Category> categories = userService.assignCategoriesToUser(form);
        Map<String, String> innerMap = new LinkedHashMap<>();

        for (Category category : categories) {
          innerMap.put(category.getGuid(), category.getName());
        }
        modelMap.put(form.getLogin(), innerMap);
        LOG.info("Category assignment to user '{}' was modified.", form.getLogin());
      } else {
        LOG.warn("The category assignment to user '{}' could not be modified because the user does not exist.",
                form.getLogin());
      }
    }
    return new ResponseEntity<>(modelMap, HttpStatus.OK);
  }

  /**
   * Smaže uživatele.
   */
  @PostMapping(USER + "/delete/{login}")
  public ResponseEntity<Map<String, Boolean>> delete(@PathVariable("login") String login) {
    User user = userService.getByLogin(login);
    boolean deleted = false;

    if (user != null) {
      deleted = userService.delete(login);
    }
    if (user != null && deleted) {
      LOG.info("User with login '{}' was deleted.", login);
    } else {
      LOG.warn("Could not delete user. User '{}' does not exist.", login);
    }
    Map<String, Boolean> modelMap = new HashMap<>();
    modelMap.put(login, deleted);
    return new ResponseEntity<>(modelMap, HttpStatus.OK);
  }

  /**
   * Smaže všechny uživatele.
   */
  @PostMapping(USER + "/clean")
  public ResponseEntity<List<String>> clean() {
    List<User> deletedUsers = userService.deleteAll();
    List<String> deletedLogins = new ArrayList<>(deletedUsers.size());

    for (User user : deletedUsers) {
      deletedLogins.add(user.getLogin());
    }
    LOG.info("Deleted ({}) users:\n{}", deletedLogins.size(), Json.toPrettyJson(deletedLogins));
    return new ResponseEntity<>(deletedLogins, HttpStatus.OK);
  }

  // === PRIVATE ==============================================================
  // ==========================================================================

  private static void checkValidModelForm(String login, String password) {
    if (Validator.isNullOrBlank(login)) {
      LOG.warn("User login is required.");
      throw new FileFetcherException();
    }
    if (Validator.isNullOrBlank(password)) {
      LOG.warn("User password is required.");
      throw new FileFetcherException();
    }
  }

  private static void checkValidModelForm(String login) {
    if (Validator.isNullOrBlank(login)) {
      LOG.warn("User login is required.");
      throw new FileFetcherException();
    }
  }
}
