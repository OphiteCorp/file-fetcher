package oc.mimic.filefetcher;

import oc.mimic.filefetcher.component.SpringDebug;
import oc.mimic.filefetcher.config.ApplicationConfig;
import oc.mimic.filefetcher.db.DatabaseInitializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jmx.JmxAutoConfiguration;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

import java.text.MessageFormat;

/**
 * Hlavní třída aplikace s metodou main().
 *
 * @author mimic
 */
@SpringBootApplication(exclude = { JmxAutoConfiguration.class })
public class Application {

  private static final Logger LOG = LoggerFactory.getLogger(Application.class);

  /**
   * Verze aplikace.
   * Tuto verzi upravit i v gradle skriptu.
   */
  public static final String VERSION = "1.2.0";

  @Autowired
  private DatabaseInitializer databaseInitializer;

  @Autowired
  private SpringDebug springDebug;

  @Autowired
  private ApplicationConfig applicationConfig;

  public static void main(String[] args) {
    new SpringApplication(Application.class).run(args);
  }

  @EventListener(ApplicationReadyEvent.class)
  public void onStartup() {
    databaseInitializer.createDatabase();
    springDebug.printRequestMappings();

    LOG.info(MessageFormat
            .format("The FileFetcher v{0} web service is online and running on port {1,number,#}. URL: http://{2}:{1,number,#}{3}",
                    VERSION, applicationConfig.getServerPort(), applicationConfig.getServerAddress(),
                    applicationConfig.getContextPath()));
  }
}
