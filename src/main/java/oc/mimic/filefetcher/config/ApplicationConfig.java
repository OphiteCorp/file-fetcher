package oc.mimic.filefetcher.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.util.ArrayList;
import java.util.List;

/**
 * Konfigurace aplikace.
 *
 * @author mimic
 */
@Configuration
@PropertySource("classpath:application.yaml")
public class ApplicationConfig {

  @Value("${server.servlet.context-path}")
  private String contextPath;

  @Value("${app.controller.error.detail-log}")
  private boolean controllerErrorDetailLog;

  @Value("#{'${app.file.image.supported-types}'.trim().replaceAll(\"\\s*(?=,)|(?<=,)\\s*\", \"\").split(',')}")
  private final List<String> supportedImageTypes = new ArrayList<>();

  @Value("${app.token-pattern.origin}")
  private String tokenOrigin;

  @Value("${app.token-pattern.mixed}")
  private String tokenMixed;

  @Value("${app.cat-hide-categories-without-permission}")
  private boolean hideCategoriesWithoutPermission;

  @Value("${app.file.image.max-resize-width}")
  private int maxResizeWidth;

  @Value("${app.file.image.max-resize-height}")
  private int maxResizeHeight;

  @Value("${app.file.image.storage.enabled}")
  private boolean storageEnabled;

  @Value("${app.file.image.storage.directory}")
  private String storageDirectory;

  @Value("${app.file.image.calculate-checksum}")
  private boolean imageCalculateChecksum;

  @Value("#{'${app.file.video.supported-types}'.trim().replaceAll(\"\\s*(?=,)|(?<=,)\\s*\", \"\").split(',')}")
  private final List<String> supportedVideoTypes = new ArrayList<>();

  @Value("${app.file.video.cache-directory}")
  private String videoCacheDirectory;

  @Value("${app.file.video.preview-file-ext}")
  private String videoPreviewFileExt;

  @Value("${app.file.video.calculate-checksum}")
  private boolean videoCalculateChecksum;

  @Value("${app.db.file}")
  private String databaseFile;

  @Value("${server.address}")
  private String serverAddress;

  @Value("${server.port}")
  private int serverPort;

  @Value("${app.mark-as-new-in-days}")
  private int markAsNewInDays;

  public boolean isControllerErrorDetailLog() {
    return controllerErrorDetailLog;
  }

  public List<String> getSupportedImageTypes() {
    return supportedImageTypes;
  }

  public List<String> getSupportedVideoTypes() {
    return supportedVideoTypes;
  }

  public int getMaxResizeWidth() {
    return maxResizeWidth;
  }

  public int getMaxResizeHeight() {
    return maxResizeHeight;
  }

  public String getDatabaseFile() {
    return databaseFile;
  }

  public boolean isStorageEnabled() {
    return storageEnabled;
  }

  public String getStorageDirectory() {
    return storageDirectory;
  }

  public int getServerPort() {
    return serverPort;
  }

  public int getMarkAsNewInDays() {
    return markAsNewInDays;
  }

  public String getVideoCacheDirectory() {
    return videoCacheDirectory;
  }

  public String getVideoPreviewFileExt() {
    return videoPreviewFileExt;
  }

  public String getContextPath() {
    return contextPath;
  }

  public String getServerAddress() {
    return serverAddress;
  }

  public boolean isVideoCalculateChecksum() {
    return videoCalculateChecksum;
  }

  public boolean isImageCalculateChecksum() {
    return imageCalculateChecksum;
  }

  public String getTokenOrigin() {
    return tokenOrigin;
  }

  public String getTokenMixed() {
    return tokenMixed;
  }

  public boolean isHideCategoriesWithoutPermission() {
    return hideCategoriesWithoutPermission;
  }
}
