package oc.mimic.filefetcher.exception;

/**
 * Základní typ výjimky pro tuto aplikaci.
 *
 * @author mimic
 */
public class FileFetcherException extends RuntimeException {

  private static final long serialVersionUID = -3036916220141451715L;

  public FileFetcherException() {
  }

  public FileFetcherException(Throwable cause) {
    super(cause);
  }

  public FileFetcherException(String message) {
    super(message);
  }

  public FileFetcherException(String message, Throwable cause) {
    super(message, cause);
  }
}
