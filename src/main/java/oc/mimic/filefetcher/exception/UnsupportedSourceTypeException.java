package oc.mimic.filefetcher.exception;

/**
 * Vývjimka, kdy zdrojový soubor naní podporován.
 *
 * @author mimic
 */
public final class UnsupportedSourceTypeException extends FileFetcherException {

  private static final long serialVersionUID = 7535300931627195690L;

  public UnsupportedSourceTypeException(String message) {
    super(message);
  }
}
